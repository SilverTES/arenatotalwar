﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    public class Spark : Node
    {
        Color _color;

        Sprite _sprite = new Sprite();

        public Spark(Color color)
        {
            _color = color;

            _sprite.Add(Game1._animation_spark);

            _z = -100000; // Over all Node Childs

            SetSize(16, 16);
            SetPivot(8, 8);

            _sprite.Start("Spark", 1, 0);
        }

        public override Node Update(GameTime gameTime)
        {
            UpdateRect();

            _sprite.Update();

            if (_sprite.OffPlay) KillMe();

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX()-1, AbsY() - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX()+1, AbsY() - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX(), AbsY()-1 - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX(), AbsY()+1 - _animate.Value(), Color.Black);

            _sprite.Draw(batch, AbsX - _oX, AbsY - _oY,_color);


            return base.Render(batch);
        }
    }
}
