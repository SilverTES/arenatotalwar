﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using Microsoft.Xna.Framework.Media;

namespace ArenaTotalWar
{
    public enum NbPlayers
    {
        _1 = 1,
        _2,
        _3,
        _4,
    }

    public class PlayerATW : Player
    {

        // Bank of point in the level !
        static int _bankPoint;
        static NbPlayers _nbPlayer = NbPlayers._1;

        bool _isDead = false;

        public PlayerATW(int id, string name): base(id, name)
        {

        }

        public static void SetNbPlayers(NbPlayers nbPlayer) { _nbPlayer = nbPlayer; }
        public static NbPlayers GetNbPlayers() { return _nbPlayer; }

        public void SetDead(bool isDead) { _isDead = isDead; }
        public bool IsDead() { return _isDead; }

        public static int GetBankPoint() { return _bankPoint; }
        public static void AddBankPoint(int point)
        {
            _bankPoint += point;
            if (_bankPoint < 0) _bankPoint = 0;
        }
        public static void SetBankPoint(int point = 0) { _bankPoint = point; }
    }


    public partial class Game1 : Game
    {
        #region Attributes

        // Game
        public static Mode _mode = Mode.RETRO;
        public static bool _isQuit = false;
        public static NaviState _naviState = new NaviState(); // Game Screen State
        public static string _pathGamePadSetup = "controllers_Setup.xml";

        //public static List<Node> _nodeLevels = new List<Node>();

        static Shake _shake = new Shake();

        // Sound
        //public static float _volume = 1;

        //public static NodeGui _nodeGui;

        // Players 
        static public int PLAYER1 = 0;
        static public int PLAYER2 = 1;
        static public int PLAYER3 = 2;
        static public int PLAYER4 = 3;
        public const int MAX_PLAYER = 4;

        // Shorter Alias
        static public PlayerATW Player1; 
        static public PlayerATW Player2;
        static public PlayerATW Player3;
        static public PlayerATW Player4;

        static public PlayerATW[] _players = new PlayerATW[MAX_PLAYER];
        static public Hero[] _heros = new Hero[MAX_PLAYER];
        public static Controller[] _controllers = new Controller[MAX_PLAYER];


        public static Window _window = new Window();
        SpriteBatch _batch;
        public static FrameCounter _frameCounter = new FrameCounter();
        public static MouseState _mouseState;

        public static Input.Mouse _mouse = new Input.Mouse();

        static public int _relMouseX;
        static public int _relMouseY;
        static public int _screenW = 1280;
        static public int _screenH = 720;

        #region SpriteFont
        static public SpriteFont _font_Main;
        static public SpriteFont _font_Big;
        static public SpriteFont _font_Mini;
        #endregion

        #region Texture2D
        static public Texture2D _tex_TitleScreen;

        static public Texture2D _tex_Atlas00;
        static public Texture2D _tex_sokoban;
        static public Texture2D _tex_herosAtlas;
        static public Texture2D _tex_blood;
        static public Texture2D _tex_sword2;

        static public Texture2D _tex_shadow;
        static public Texture2D _tex_explosion;
        static public Texture2D _tex_fireExplosion;
        static public Texture2D _tex_playerHud;
        static public Texture2D _tex_enemyAtlas;
        static public Texture2D _tex_tree;
        static public Texture2D _tex_coin;
        static public Texture2D _tex_coin3;
        static public Texture2D _tex_exclamation;
        static public Texture2D _tex_itemAtlas;

        static public Texture2D _tex_fog;
        static public Texture2D _tex_fow;
        static public Texture2D _tex_smoke;
        static public Texture2D _tex_warning;

        static public Texture2D _tex_skinGui;
        static public Texture2D _tex_squareLock;
        static public Texture2D _tex_basicButton;

        static public Texture2D _tex_heroItems;
        static public Texture2D _tex_gameOver;
        #endregion

        #region SoundEffect
        public static SoundEffect _sound_bubble;
        public static SoundEffect _sound_explosion;
        public static SoundEffect _sound_poseBomb;
        public static SoundEffect _sound_coin;

        public static SoundEffect _sound_zombieMoan;
        public static SoundEffect _sound_zombieKillYou;
        public static SoundEffect _sound_zombieTalking;
        public static SoundEffect _sound_zombieDead;
        public static SoundEffect _sound_monsterRoar;

        public static SoundEffect _sound_footStep;
        public static SoundEffect _sound_killBlock;
        public static SoundEffect _sound_itemLife;

        public static SoundEffect _sound_alarm;
        public static SoundEffect _sound_click;
        public static SoundEffect _sound_key;
        public static SoundEffect _sound_notification;

        public static SoundEffect _sound_laserBlast;
        public static SoundEffect _sound_atom;
        public static SoundEffect _sound_slime;

        public static SoundEffect _sound_lose;
        public static SoundEffect _sound_win;

        public static SoundEffect _sound_electric1;
        public static SoundEffect _sound_electric2;
        public static SoundEffect _sound_sword;
        public static SoundEffect _sound_swordHit;

        public static SoundEffect _sound_hurtMale;
        public static SoundEffect _sound_hurtFemale;
        public static SoundEffect _sound_punch;
        public static SoundEffect _sound_hitWood;
        public static SoundEffect _sound_hitIronWood;
        public static SoundEffect _sound_hitGlass;
        #endregion

        #region Song
        static public Song _song_start;
        static public Song _song_intro;
        static public Song _song_wave;
        static public Song _song_epic;
        #endregion

        public static MessageQueue _messageQueue = new MessageQueue();

        #region Animation
        public static Animation _animation_explosion;
        public static Animation _animation_fireExplosion;
        public static Animation _animation_coin;
        public static Animation _animation_coin2;
        public static Animation _animation_lifeItem;
        public static Animation _animation_powerItem;
        public static Animation _animation_AddBombItem;
        public static Animation _animation_smoke;
        public static Animation _animation_squareLock;
        public static Animation _animation_sword2;

        public static Animation _animation_bulletFire;
        public static Animation _animation_bulletFire2;
        public static Animation _animation_bulletBlue;

        public static Animation _animation_spark;
        #endregion

        #region Sprite
        public static Sprite _sprite_HeroA = new Sprite();
        public static Sprite _sprite_HeroB = new Sprite();
        public static Sprite _sprite_HeroC = new Sprite();
        public static Sprite _sprite_HeroD = new Sprite();

        public static Sprite _sprite_zombieA = new Sprite();

        public static Sprite _sprite_omega = new Sprite();
        #endregion



        public static SkinGui _skinGui_00;
        public static SkinGui _skinGui_01;
        public static SkinGui _skinGui_02;
        public static SkinGui _skinGui_03;

        public static Style _style_main;

        #endregion

        protected override void LoadContent()
        {
            #region Content SpriteFont
            _font_Main = Content.Load<SpriteFont>("SpriteFont/mainFont");
            _font_Big = Content.Load<SpriteFont>("SpriteFont/bigFont");
            _font_Mini = Content.Load<SpriteFont>("SpriteFont/miniFont");
            #endregion

            #region Content Image
            _tex_TitleScreen = Content.Load<Texture2D>("Image/title_screen");

            _tex_Atlas00 = Content.Load<Texture2D>("Image/atlas00");
            _tex_shadow = Content.Load<Texture2D>("Image/shadow");
            _tex_sokoban = Content.Load<Texture2D>("Image/sokoban_tilesheet");
            _tex_herosAtlas = Content.Load<Texture2D>("Image/hero");
            _tex_blood = Content.Load<Texture2D>("Image/blood_64");
            _tex_sword2 = Content.Load<Texture2D>("Image/sword2");

            _tex_explosion = Content.Load<Texture2D>("Image/explosion_side");
            _tex_fireExplosion = Content.Load<Texture2D>("Image/fire_bomb_explosion");
            _tex_playerHud = Content.Load<Texture2D>("Image/player_hud");
            _tex_enemyAtlas = Content.Load<Texture2D>("Image/Zombie_V");
            _tex_tree = Content.Load<Texture2D>("Image/tree");
            _tex_coin = Content.Load<Texture2D>("Image/coin2");
            _tex_coin3 = Content.Load<Texture2D>("Image/coin3");
            _tex_exclamation = Content.Load<Texture2D>("Image/exclamation");
            _tex_itemAtlas = Content.Load<Texture2D>("Image/item");

            _tex_fog = Content.Load<Texture2D>("Image/fog");
            _tex_fow = Content.Load<Texture2D>("Image/fow");
            _tex_smoke = Content.Load<Texture2D>("Image/smoke");
            _tex_warning = Content.Load<Texture2D>("Image/warning");

            _tex_skinGui = Content.Load<Texture2D>("Image/skinGUI00");

            _tex_squareLock = Content.Load<Texture2D>("Image/square_lock");
            _tex_basicButton = Content.Load<Texture2D>("Image/basic_button");

            _tex_heroItems = Content.Load<Texture2D>("Image/hero_items");
            _tex_gameOver = Content.Load<Texture2D>("Image/game_over");
            #endregion

            #region Content Sound
            _sound_bubble = Content.Load<SoundEffect>("Sound/bubble1");
            _sound_explosion = Content.Load<SoundEffect>("Sound/Explosion");
            _sound_poseBomb = Content.Load<SoundEffect>("Sound/build");
            _sound_coin = Content.Load<SoundEffect>("Sound/coin");
            _sound_zombieMoan = Content.Load<SoundEffect>("Sound/Zombie_Moan");
            _sound_zombieKillYou = Content.Load<SoundEffect>("Sound/Zombie_Kill_You");
            _sound_zombieTalking = Content.Load<SoundEffect>("Sound/Zombie_Talking");
            _sound_zombieDead = Content.Load<SoundEffect>("Sound/Zombie_Dead");
            _sound_monsterRoar = Content.Load<SoundEffect>("Sound/Monster_Roar");
            _sound_footStep = Content.Load<SoundEffect>("Sound/footstep_2");
            _sound_killBlock = Content.Load<SoundEffect>("Sound/towerkill");
            _sound_itemLife = Content.Load<SoundEffect>("Sound/star");

            _sound_alarm = Content.Load<SoundEffect>("Sound/alarm");
            _sound_click = Content.Load<SoundEffect>("Sound/clock");
            _sound_key = Content.Load<SoundEffect>("Sound/key2");
            _sound_notification = Content.Load<SoundEffect>("Sound/alert_sfx3");

            _sound_laserBlast = Content.Load<SoundEffect>("Sound/LaserBlast");
            _sound_atom = Content.Load<SoundEffect>("Sound/atom");
            _sound_slime = Content.Load<SoundEffect>("Sound/slime_click");

            _sound_lose = Content.Load<SoundEffect>("Sound/lose");
            _sound_win = Content.Load<SoundEffect>("Sound/success_complete");

            _sound_electric1 = Content.Load<SoundEffect>("Sound/electric_door_opening_1");
            _sound_electric2 = Content.Load<SoundEffect>("Sound/electric_door_opening_2");
            _sound_sword = Content.Load<SoundEffect>("Sound/sword");
            _sound_swordHit = Content.Load<SoundEffect>("Sound/sword-hit");

            _sound_hurtMale = Content.Load<SoundEffect>("Sound/hurt_male");
            _sound_hurtFemale = Content.Load<SoundEffect>("Sound/hurt_female");

            _sound_punch = Content.Load<SoundEffect>("Sound/punch");
            _sound_hitWood = Content.Load<SoundEffect>("Sound/wood_hit");
            _sound_hitIronWood = Content.Load<SoundEffect>("Sound/iron_wood_slam");
            _sound_hitGlass = Content.Load<SoundEffect>("Sound/smashing_glass");


            #endregion

            #region Content Song
            _song_start = Content.Load<Song>("Song/ambiant_horror");
            _song_intro = Content.Load<Song>("Song/Doubt");
            _song_wave = Content.Load<Song>("Song/nightmare");
            _song_epic = Content.Load<Song>("Song/Action_Crave");
            #endregion
        }
        protected override void UnloadContent()
        {

        }

        void LoadSprite()
        {
            // Offset XY Atlas !
            int oX = 0;
            int oY = 0;

            // Size of final frame on screen !
            Rectangle rect = new Rectangle(0, 0, 64, 64);


            // Animation explosion side
            _animation_explosion = new Animation(_tex_explosion, "Explosion").SetLoop(Loops.NONE);
            for (int i = 0; i < 14; i++) _animation_explosion.Add(new Frame(new Rectangle(i*64, 0, 64, 64), 1, 32, 48, rect));
            for (int i = 0; i < 14; i++) _animation_explosion.Add(new Frame(new Rectangle(i * 64, 64, 64, 64), 1, 32, 48, rect));

            // Animation fire explosion
            _animation_fireExplosion = new Animation(_tex_fireExplosion, "Explosion").SetLoop(Loops.NONE);
            rect = new Rectangle(0, 0, 40, 60);
            for (int i = 0; i < 12; i++) _animation_fireExplosion.Add(new Frame(new Rectangle(i * 40, 0, 40, 60), 1, 20, 50, rect));
            for (int i = 0; i < 12; i++) _animation_fireExplosion.Add(new Frame(new Rectangle(i * 40, 60, 40, 60), 1, 20, 50, rect));

            rect = new Rectangle(0, 0, 16, 16);
            _animation_smoke = new Animation(_tex_smoke, "Idle").SetLoop(Loops.NONE)
                .Add(new Frame(new Rectangle(0, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(16, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(32, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(48, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(0, 16, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(16, 16, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(32, 16, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(48, 16, 16, 16), 4, 8, 14, rect));


            // Sword Attack animation
            _animation_sword2 = new Animation(_tex_sword2, "Attack").SetLoop(Loops.NONE);
            for (int j = 0; j < 2; ++j)
            {
                for (int i = 0; i < 4; ++i)
                {
                    _animation_sword2.Add(new Frame(new Rectangle(i*101, j*83, 101, 83),2, 20, 70, new Rectangle(0,0,50,40)));
                }
            }

            // Bullet Fire animation
            _animation_bulletFire = new Animation(_tex_sokoban, "ShootFire").SetLoop(Loops.REPEAT);
            for (int i = 0; i < 5; ++i)
            {
                _animation_bulletFire.Add(new Frame(new Rectangle(i * 28, 256, 28, 16), 1, 20, 8));
            }
            // Bullet Fire2 animation
            _animation_bulletFire2 = new Animation(_tex_sokoban, "ShootFire2").SetLoop(Loops.REPEAT);
            for (int i = 0; i < 5; ++i)
            {
                _animation_bulletFire2.Add(new Frame(new Rectangle(i * 30, 272, 30, 9), 1, 24, 5));
            }
            // Bullet Blue animation
            _animation_bulletBlue = new Animation(_tex_sokoban, "ShootBlue").SetLoop(Loops.REPEAT);
            for (int i = 0; i < 5; ++i)
            {
                _animation_bulletBlue.Add(new Frame(new Rectangle(i * 15, 288, 15, 10), 1, 8, 5));
            }

            // Spark animation
            _animation_spark = new Animation(_tex_sokoban, "Spark").SetLoop(Loops.NONE);
            for (int i = 0; i < 8; ++i)
            {
                _animation_spark.Add(new Frame(new Rectangle(i * 32, 304, 32, 32), 1, 8, 5, new Rectangle(0, 0, 16, 16)));
            }

            // Size of final frame on screen !
            rect = new Rectangle(0, 0, 32, 32);

            // Create Animations of ZombieA Sprite
            oX = 288;
            oY = 256;
            new Animation(_tex_enemyAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(32 + oX, 0 + oY, 32, 32), 4, 16, 24, rect))
                .AppendTo(_sprite_zombieA);
            new Animation(_tex_enemyAtlas, "Down").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 0 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 0 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 0 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_zombieA);
            new Animation(_tex_enemyAtlas, "Up").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 96 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 96 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 96 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_zombieA);
            new Animation(_tex_enemyAtlas, "Left").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 32 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 32 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 32 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_zombieA);
            new Animation(_tex_enemyAtlas, "Right").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 64 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 64 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 64 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_zombieA);

            // Create Animations of enemy Omega Sprite
            oX = 0;
            oY = 384;
            //// Size of final frame on screen !
            //rect = new Rectangle(0, 0, 64, 64);

            new Animation(_tex_enemyAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(32 + oX, 0 + oY, 32, 32), 4, 16, 24, rect))
                .AppendTo(_sprite_omega);
            new Animation(_tex_enemyAtlas, "Down").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 0 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 0 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 0 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_omega);
            new Animation(_tex_enemyAtlas, "Up").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 96 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 96 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 96 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_omega);
            new Animation(_tex_enemyAtlas, "Left").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 32 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 32 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 32 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_omega);
            new Animation(_tex_enemyAtlas, "Right").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 64 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(32 + oX, 64 + oY, 32, 32), 6, 16, 24, rect))
                .Add(new Frame(new Rectangle(64 + oX, 64 + oY, 32, 32), 6, 16, 24, rect))
                .AppendTo(_sprite_omega);



            // Size of final frame on screen !
            rect = new Rectangle(0, 0, 32, 32);

            // Create Animations of Coin Sprite
            _animation_coin = new Animation(_tex_sokoban, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(640, 320, 64, 64), 8, 32, 48, rect))
                .Add(new Frame(new Rectangle(576, 320, 64, 64), 8, 32, 48, rect));

            // Create Animations of LifeItem Sprite
            _animation_lifeItem = new Animation(_tex_itemAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(128, 256, 32, 32), 8, 16, 28, rect))
                .Add(new Frame(new Rectangle(160, 256, 32, 32), 8, 16, 28, rect));

            // Create Animations of PowerItem Sprite
            _animation_powerItem = new Animation(_tex_itemAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(224, 64, 32, 32), 8, 16, 28, rect))
                .Add(new Frame(new Rectangle(256, 64, 32, 32), 8, 16, 28, rect));

            // Create Animations of PowerItem Sprite
            _animation_AddBombItem = new Animation(_tex_itemAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(320, 0, 32, 32), 8, 16, 28, rect))
                .Add(new Frame(new Rectangle(288, 0, 32, 32), 8, 16, 28, rect));

            // Create Animations of Coin2 Sprite
            _animation_coin2 = new Animation(_tex_coin3, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(0, 0, 64, 64), 4, 32, 48, rect))
                .Add(new Frame(new Rectangle(64, 0, 64, 64), 4, 32, 48, rect))
                .Add(new Frame(new Rectangle(128, 0, 64, 64), 4, 32, 48, rect))
                .Add(new Frame(new Rectangle(192, 0, 64, 64), 4, 32, 48, rect))
                .Add(new Frame(new Rectangle(256, 0, 64, 64), 4, 32, 48, rect))
                .Add(new Frame(new Rectangle(320, 0, 64, 64), 4, 32, 48, rect));


            // Create Animations of Hero Sprite
            oX = 0;
            oY = 0;
            new Animation(_tex_herosAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .AppendTo(_sprite_HeroA);

            new Animation(_tex_herosAtlas, "Down").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroA);
            new Animation(_tex_herosAtlas, "Up").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroA);

            new Animation(_tex_herosAtlas, "Right").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroA);
            new Animation(_tex_herosAtlas, "Left").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroA);

            // Create Animations of Hero Sprite
            oX = 144;
            oY = 0;
            new Animation(_tex_herosAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .AppendTo(_sprite_HeroB);

            new Animation(_tex_herosAtlas, "Down").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroB);
            new Animation(_tex_herosAtlas, "Up").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroB);

            new Animation(_tex_herosAtlas, "Right").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroB);
            new Animation(_tex_herosAtlas, "Left").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroB);

            // Create Animations of Hero Sprite
            oX = 432;
            oY = 0;
            new Animation(_tex_herosAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .AppendTo(_sprite_HeroC);

            new Animation(_tex_herosAtlas, "Down").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroC);
            new Animation(_tex_herosAtlas, "Up").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroC);

            new Animation(_tex_herosAtlas, "Right").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroC);
            new Animation(_tex_herosAtlas, "Left").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroC);

            // Create Animations of Hero Sprite
            oX = 144;
            oY = 192;
            new Animation(_tex_herosAtlas, "Idle").SetLoop(Loops.REPEAT)
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 8, 24, 44, rect))
                .AppendTo(_sprite_HeroD);

            new Animation(_tex_herosAtlas, "Down").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 0 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroD);
            new Animation(_tex_herosAtlas, "Up").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 144 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroD);

            new Animation(_tex_herosAtlas, "Right").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 96 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroD);
            new Animation(_tex_herosAtlas, "Left").SetLoop(Loops.PINGPONG)
                .Add(new Frame(new Rectangle(0 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(48 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .Add(new Frame(new Rectangle(96 + oX, 48 + oY, 48, 48), 4, 24, 44, rect))
                .AppendTo(_sprite_HeroD);

            // Load SkinGui texture !

            oX = 24;
            oY = 24;

            _skinGui_00 = new SkinGui(_tex_skinGui)
                .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8,8)})
                .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8,8)})
                .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8,8)})
                .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8,8)})
                .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8)})
                .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8)});

            oX = 24;
            oY = 0;

           _skinGui_01 = new SkinGui(_tex_skinGui)
                .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8,8)})
                .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8,8)})
                .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8,8)})
                .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8,8)})
                .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8)})
                .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8)});

            oX = 0;
            oY = 0;

            _skinGui_02 = new SkinGui(_tex_skinGui)
                 .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                 .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8) });

            oX = 48;
            oY = 24;

            _skinGui_03 = new SkinGui(_tex_skinGui)
                 .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                 .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8) });
        }

        void CreateLevel()
        {
            //_nodeLevels.Add(new Level.Level0());

        }

        void CreatePlayer()
        {

            Player1 = _players[PLAYER1] = new PlayerATW(PLAYER1, "Mugen");
            Player2 = _players[PLAYER2] = new PlayerATW(PLAYER2, "Silver");
            Player3 = _players[PLAYER3] = new PlayerATW(PLAYER3, "Zero");
            Player4 = _players[PLAYER4] = new PlayerATW(PLAYER4, "Rei");

            _controllers[PLAYER1] = new Controller()
                .AsMainController(_players[PLAYER1]);

            new Controller()
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.Up, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.Down, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.Left, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.Right, 0, 0, 0, 0, -1))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.RightAlt, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.Space, 0, 0, 0, 0, -1))

                .SetButton(new Button((int)SNES.BUTTONS.START, (int)Keys.Enter, 0, 0, 0, 0, -1))
                .AppendTo(_players[PLAYER1]);

            _controllers[PLAYER2] = new Controller()
                .AsMainController(_players[PLAYER2]);

            new Controller()
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.Z, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.S, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.Q, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.D, 0, 0, 0, 0, -1))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.D1, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.D2, 0, 0, 0, 0, -1))
                .AppendTo(_players[PLAYER2]);

            _controllers[PLAYER3] = new Controller()
                .AsMainController(_players[PLAYER3]);

            new Controller()
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.O, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.L, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.K, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.M, 0, 0, 0, 0, -1))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.NumPad4, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.Space, 0, 0, 0, 0, -1))
                .AppendTo(_players[PLAYER3]);

            _controllers[PLAYER4] = new Controller()
                .AsMainController(_players[PLAYER4])
                .SetButton(new Button((int)SNES.BUTTONS.UP, (int)Keys.T, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.DOWN, (int)Keys.G, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.LEFT, (int)Keys.F, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.RIGHT, (int)Keys.H, 0, 0, 0, 0, -1))

                .SetButton(new Button((int)SNES.BUTTONS.A, (int)Keys.NumPad6, 0, 0, 0, 0, -1))
                .SetButton(new Button((int)SNES.BUTTONS.B, (int)Keys.Space, 0, 0, 0, 0, -1));

        }


        public static void Shake(float intensity, float step)
        {
            _shake.SetIntensity(intensity, step);
            Game1._sound_atom.Play(.2f, .001f, 0);
        }

        public static void AddPlayerInMap(Level level, string name, int playerIndex, Map2D<Tile> map2D, int mapX, int mapY, int sex = Hero.MALE)
        {
            // If hero not exist or player dead then recreat and insert Hero in _root["play"]
            //if (null == _heros[playerIndex] || _heros[playerIndex].IsDead()) 
            {
                _heros[playerIndex] = (Hero)new Hero(map2D, _players[playerIndex], sex)//, _controllers[playerIndex])
                    .SetName(name)
                    .AppendTo(level)
                    //.AppendTo(nodeLevel)
                    .Init();

                _heros[playerIndex].GoTo(mapX, mapY);

                RectangleF limit = new RectangleF(16, 32, level.GetArenaSizeW()-16, level.GetArenaSizeH());

                _heros[playerIndex].SetLimit(limit);
            }
        }

        public static void PopItem(Map2D<Tile> map2D, Node parent, int mapX, int mapY, int prob = 100)
        {
            if (null != map2D.Get(mapX, mapY))
                if (map2D.Get(mapX, mapY)._type == UID.Get<Portal>()) return; // If type Portal then can't pop item !

            if (prob < 50) prob = 50;

            if (Misc.Rng.Next(0, prob) > 20) // Pop something or not
            {

                int rng = Misc.Rng.Next(0, prob*2); // What to pop

                if (rng < 4)
                {
                    ItemPower.Add(map2D, parent, mapX, mapY);
                }
                else if (rng >= 4 && rng < 8)
                {
                    ItemLife.Add(map2D, parent, mapX, mapY);
                }
                else if (rng >= 8 && rng < 12)
                {
                    ItemAddBomb.Add(map2D, parent, mapX, mapY);
                }
                else
                {
                    ItemCoin.Add(map2D, parent, mapX, mapY);
                }

            }
        }

        public static bool IsNotMapObject(int type) // Useful for check object type in map before insert a new object, if void then insert !
        {
            //return true;
            return
                type != UID.Get<Tower>() && type != UID.Get<Tower.Case>() &&
                type != UID.Get<MapBlock>() &&
                type != UID.Get<MapDecor>() &&
                type != UID.Get<ItemCoin>() &&
                type != UID.Get<ItemLife>() &&
                type != UID.Get<ItemPower>() &&
                type != UID.Get<ItemAddBomb>() &&
                type != UID.Get<Bomb>() &&
                type != UID.Get<Block>()
            ;
        }


        public static void PopPlayers(Level level, Map2D<Tile> map2D, int portalX , int portalY, bool p1 = true, bool p2 = false, bool p3 = false, bool p4 = false)
        {
            // Reset heros dead status to true before add new heros !
            for (int i = 0; i < MAX_PLAYER; i++)
            {
                if (null != _heros[i])
                {
                    _heros[i].SetIsDead(true);
                }
            }

            if (p1) AddPlayerInMap(level, "Mugen", PLAYER1, map2D, portalX, portalY + 1, Hero.MALE);

            if (p2) AddPlayerInMap(level, "Silver", PLAYER2, map2D, portalX, portalY + 1, Hero.FEMALE);
            if (p3) AddPlayerInMap(level, "Zero", PLAYER3, map2D, portalX, portalY + 1, Hero.MALE);
            if (p4) AddPlayerInMap(level, "Rei", PLAYER4, map2D, portalX, portalY + 1, Hero.FEMALE);

            if (p2) _heros[PLAYER2].SetSprite(_sprite_HeroB.Clone());
            if (p3) _heros[PLAYER3].SetSprite(_sprite_HeroC.Clone());
            if (p4) _heros[PLAYER4].SetSprite(_sprite_HeroD.Clone());
        }

        public static void UpdateNaviGateButton(NaviGate naviGate, bool crossNavi = false) // Utilisé pour la navigation dans un Node avec un NaviGate !
        {
            if (crossNavi)
            {
                naviGate.Update(UID.Get<Gui.Base>());

                naviGate.UpdateSubmitButton(Game1.Player1.GetButton((int)SNES.BUTTONS.A) != 0);

                if (Input.Button.OnPress("up", Game1.Player1.GetButton((int)SNES.BUTTONS.UP) != 0)) naviGate.ToDirection(Position.UP);
                if (Input.Button.OnPress("down", Game1.Player1.GetButton((int)SNES.BUTTONS.DOWN) != 0)) naviGate.ToDirection(Position.DOWN);
                if (Input.Button.OnPress("left", Game1.Player1.GetButton((int)SNES.BUTTONS.LEFT) != 0)) naviGate.ToDirection(Position.LEFT);
                if (Input.Button.OnPress("right", Game1.Player1.GetButton((int)SNES.BUTTONS.RIGHT) != 0)) naviGate.ToDirection(Position.RIGHT);

                // back button
                Input.Button.OnPress("back", Game1.Player1.GetButton((int)SNES.BUTTONS.B) != 0);

            }
            else
            {
                naviGate.Update(UID.Get<Gui.Base>());

                naviGate.UpdateSubmitButton(Game1.Player1.GetButton((int)SNES.BUTTONS.A) != 0);

                if (Input.Button.OnPress("up", Game1.Player1.GetButton((int)SNES.BUTTONS.UP) != 0)) naviGate.ToPrevNaviNode(UID.Get<Gui.Base>());
                if (Input.Button.OnPress("down", Game1.Player1.GetButton((int)SNES.BUTTONS.DOWN) != 0)) naviGate.ToNextNaviNode(UID.Get<Gui.Base>());

                // back button
                Input.Button.OnPress("back", Game1.Player1.GetButton((int)SNES.BUTTONS.B) != 0);
            }


        }

    }
}
