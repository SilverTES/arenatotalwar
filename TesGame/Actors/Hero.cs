﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;


namespace ArenaTotalWar
{
    public class Hero : MapActor
    {
        public const int MALE = 1;
        public const int FEMALE = -1;

        public int Sex { get; private set; }

        public enum State
        {
            NONE, 
            //IS_HIT,
            NORMAL, // Normal Mode
            SWORD_ATTACK, // Sword Mode
            SELECT, // Select Item
            BUILD   // Build Mode
        }

        #region Attributes

        public State _state = State.NORMAL;
        
        public HeroUX _heroUX;

        

        Sprite _sprite;

        public Sprite _spriteSword;
        public bool _isAttackSword = false; // statuc : if sword animation play !
        public bool _isAttackSwordOn = false; // status : if the sword can make damage !
        Rectangle _rectSwordAttack;

        //Addon.Draggable _drag;

        PlayerATW _player;

        BarInfo _barInfo;

        float _zMax = 2.0f;
        const float _speedZ = .5f;
        float _pZ = 0;
        float _vZ = -_speedZ;

        // Abs Rect Limiter
        public RectangleF _limitRect;

        //int _score = 400;
        int _energy = 400;
        int _maxEnergy = 400;

        bool _isTurbo = false; // Turbo : Boost speed movement but cost energy !!
        float _normalSpeed = 2.0f;
        float _turboSpeed = 3.0f;
        public int _tickTurbo = 0;
        public int _timerTurbo = 20;

        
        int _wavePower = 1;
        int _waveTimer = 4;
        int _maxWavePower = 6;

        int _nbActiveBomb = 0; 
        int _maxActiveBomb = 1; // Nb bomb Hero can pose

        float _transparency = 1f;
        bool _isSelected = false; 

        bool _isDead = true;
        public bool _poseBomb = false; // 

        int _tickStep = 0;
        int _timerStep = 8;

        public bool IS_B_UP = false;
        public bool IS_B_DOWN = false;
        public bool IS_B_LEFT = false;
        public bool IS_B_RIGHT = false;
        public bool IS_B_L = false;
        public bool IS_B_R = false;
        public bool IS_B_A = false;
        public bool IS_B_B = false;
        public bool IS_B_X = false;
        public bool IS_B_Y = false;

        public bool IS_PUSH_B_UP = false;
        public bool IS_PUSH_B_DOWN = false;
        public bool IS_PUSH_B_LEFT = false;
        public bool IS_PUSH_B_RIGHT = false;
        public bool IS_PUSH_B_L = false;
        public bool IS_PUSH_B_R = false;
        public bool IS_PUSH_B_A = false;
        public bool IS_PUSH_B_B = false;
        public bool IS_PUSH_B_X = false;
        public bool IS_PUSH_B_Y = false;

        public bool ON_B_UP = false;
        public bool ON_B_DOWN = false;
        public bool ON_B_LEFT = false;
        public bool ON_B_RIGHT = false;
        public bool ON_B_L = false;
        public bool ON_B_R = false;
        public bool ON_B_A = false;
        public bool ON_B_B = false;
        public bool ON_B_X = false;
        public bool ON_B_Y = false;

        // Is Hit 

        //int _nbMaxSwordHit = 4; // Number max object can sword hit 
        bool _isHit = false;
        int _tickHit = 0;
        int _tempoHit = 20;
        Vector2 _hitDirection = new Vector2();
        float _hitDirectionSpeed = 1f;

        Shake _shake;

        #endregion 

        public Hero(Map2D<Tile> map2D, PlayerATW player, int sex = MALE) : base (map2D)
        {

            _shake = new Shake();

            _player = player;
            Sex = sex;
            //player.SetController(controller);

            // Set Type 
            _type = UID.Get<Hero>();

            // Create all Addons !
            //_drag = CreateAddon<Addon.Draggable>();


            // Set Size and Pivot of Hero
            //SetSize(Level.TileSizeW, Level.TileSizeH);
            //SetPivot(Level.TileSizeW/2, Level.TileSizeH/2);
            SetSize(32, 32);
            SetPivot(16, 16);

            SetCollideZone(1, new RectangleF());
            _rectSwordAttack = new Rectangle(0,0,30,30);


            _speed = _normalSpeed;

            // Create new Sprite
            _sprite = Game1._sprite_HeroA.Clone();
            //_sprite = Game1._zombieA.Clone();
            _spriteSword = new Sprite();
            _spriteSword.Add(Game1._animation_sword2);

            _spriteSword.Start("Attack", 1, 0);

            // Create Collide Point of Hero
            //AddPoint((int)Position.NW, new CollidePoint(-10, -12));
            //AddPoint((int)Position.NE, new CollidePoint(10, -12));
            //AddPoint((int)Position.SW, new CollidePoint(-10, 4));
            //AddPoint((int)Position.SE, new CollidePoint(10, 4));


            // Create Collide Point of Hero
            AddPoint((int)CPoint.UL, new CollidePoint(-6, -12));
            AddPoint((int)CPoint.UR, new CollidePoint(+6, -12));

            AddPoint((int)CPoint.DL, new CollidePoint(-6, +6));
            AddPoint((int)CPoint.DR, new CollidePoint(+6, +6));

            AddPoint((int)CPoint.LU, new CollidePoint(-12, -8));
            AddPoint((int)CPoint.LD, new CollidePoint(-12, +2));

            AddPoint((int)CPoint.RU, new CollidePoint(+12, -8));
            AddPoint((int)CPoint.RD, new CollidePoint(+12, +2));


            _barInfo = new BarInfo(this, Color.YellowGreen, Color.Firebrick, Color.GhostWhite * .2f, _maxEnergy, 24, 4, 1);
            _barInfo.SetY(-16);
            _barInfo.SetPlayShow(true, true);
            _barInfo.AppendTo(this);
            _barInfo.AddWarning(new BarInfo.Warning(_maxEnergy / 6, _maxEnergy / 3, 80));
            _barInfo.AddWarning(new BarInfo.Warning(0, _maxEnergy / 6, 40));

        }
        public void SetSprite(Sprite spriteHero)
        {
            _sprite = spriteHero;
            _sprite.Start("Idle", 1, 0);
        }
        public Hero SetLimit(RectangleF limitRect)
        {
            _limitRect = limitRect;
            return this;
        }

        public Hero Shake(float intensity = 1, float step = .05f)
        {
            _shake.SetIntensity(intensity, step);
            return this;
        }

        public Hero PlaySoundHurt(int sex)
        {
            if (sex == MALE)
                Game1._sound_hurtMale.Play(.6f, .01f, 0);
            else
                Game1._sound_hurtFemale.Play(.4f, .01f, 0);

            return this;
        }

        public Hero HitMe(Vector2 hitDirection, float hitDirectionSpeed = 1f, int tempoHit = 20)
        {
            if (!_isHit)
            {
                _hitDirection = hitDirection;
                _hitDirectionSpeed = hitDirectionSpeed;
                _tempoHit = tempoHit;
                _tickHit = 0;

                //Shake(intensity, step);
                _isHit = true;
            }

            return this;
        }
        public bool IsHit() { return _isHit; }

        public Hero SetIsDead(bool isDead) { _isDead = isDead; return this; }
        public bool IsDead() { return _isDead; }

        public override Node Init()
        {
            //_drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH - 60));
            //_drag.SetLimitRect(true);

            //_limitRect = new RectangleF(0, 0, Game1._screenW, Game1._screenH - 60);
            //_limitRect = _parent.AbsRectF();

            _sprite.Start("Idle", 1, 0);

            _heroUX = new HeroUX(this, _map2D._tileW, _map2D._tileH);
            _heroUX.AppendTo(_parent);

            _isDead = false;

            return this;
        }

        public override Node Update(GameTime gameTime)
        {
            // Update Addons
            //_drag.Update(Game1._mouse);
            _sprite.Update();
            _spriteSword.Update();

            if (_spriteSword.OffPlay)
                _isAttackSword = false;

            //UpdateCollideZone(0, Gfx.AddRect(_rect,new Rectangle(8,8,-16,-16)));
            UpdateCollideZone(0, _rect);


            if (_showDirection == Position.UP) { _rectSwordAttack.X = -15; _rectSwordAttack.Y = -64; }
            if (_showDirection == Position.DOWN) { _rectSwordAttack.X = -15; _rectSwordAttack.Y = 4; }
            if (_showDirection == Position.LEFT) { _rectSwordAttack.X = -44; _rectSwordAttack.Y = -28; }
            if (_showDirection == Position.RIGHT) { _rectSwordAttack.X = +12; _rectSwordAttack.Y = -28; }

            _isAttackSwordOn = _spriteSword.CurFrame == 3 && _isAttackSword && _spriteSword.OnChangeFrame;

            UpdateCollideZone(1, Gfx.TranslateRect(_rectSwordAttack, XY), _isAttackSwordOn);

            // Y to Z order :
            _z = -(int)_y;

            // Limit movement
            if (_x < _limitRect.X) _x = _limitRect.X;
            if (_x > _limitRect.Width) _x = _limitRect.Width;
            if (_y < _limitRect.Y) _y = _limitRect.Y;
            if (_y > _limitRect.Height) _y = _limitRect.Height;

            if (_isHit)
            {
                //_state = State.IS_HIT;

                _tickHit++;

                if (_tickHit > _tempoHit)
                {
                    _tickHit = 0;

                    _isHit = false;
                    _state = State.NORMAL;
                    //_hitDirection = new Vector2();
                }
            }

            // Manage Button Status
            IS_B_UP = _player.GetButton((int)SNES.BUTTONS.UP) != 0;
            IS_B_DOWN = _player.GetButton((int)SNES.BUTTONS.DOWN) != 0;
            IS_B_LEFT = _player.GetButton((int)SNES.BUTTONS.LEFT) != 0;
            IS_B_RIGHT = _player.GetButton((int)SNES.BUTTONS.RIGHT) != 0;

            IS_B_L = _player.GetButton((int)SNES.BUTTONS.L) != 0;
            IS_B_R = _player.GetButton((int)SNES.BUTTONS.R) != 0;

            IS_B_A = _player.GetButton((int)SNES.BUTTONS.A) != 0 && !_isSelected; // If hero is selected then can't control A !
            IS_B_B = _player.GetButton((int)SNES.BUTTONS.B) != 0;
            IS_B_X = _player.GetButton((int)SNES.BUTTONS.X) != 0;
            IS_B_Y = _player.GetButton((int)SNES.BUTTONS.Y) != 0;

            // Manage Button Trigger
            ON_B_UP = false; if (!IS_B_UP) IS_PUSH_B_UP = false; if (IS_B_UP && !IS_PUSH_B_UP) { IS_PUSH_B_UP = true; ON_B_UP = true; }
            ON_B_DOWN = false; if (!IS_B_DOWN) IS_PUSH_B_DOWN = false; if (IS_B_DOWN && !IS_PUSH_B_DOWN) { IS_PUSH_B_DOWN = true; ON_B_DOWN = true; }
            ON_B_LEFT = false; if (!IS_B_LEFT) IS_PUSH_B_LEFT = false; if (IS_B_LEFT && !IS_PUSH_B_LEFT) { IS_PUSH_B_LEFT = true; ON_B_LEFT = true; }
            ON_B_RIGHT = false; if (!IS_B_RIGHT) IS_PUSH_B_RIGHT = false; if (IS_B_RIGHT && !IS_PUSH_B_RIGHT) { IS_PUSH_B_RIGHT = true; ON_B_RIGHT = true; }

            ON_B_L = false; if (!IS_B_L) IS_PUSH_B_L = false; if (IS_B_L && !IS_PUSH_B_L) { IS_PUSH_B_L = true; ON_B_L = true; }
            ON_B_R = false; if (!IS_B_R) IS_PUSH_B_R = false; if (IS_B_R && !IS_PUSH_B_R) { IS_PUSH_B_R = true; ON_B_R = true; }

            ON_B_A = false; if (!IS_B_A) IS_PUSH_B_A = false; if (IS_B_A && !IS_PUSH_B_A) { IS_PUSH_B_A = true; ON_B_A = true; }
            ON_B_B = false; if (!IS_B_B) IS_PUSH_B_B = false; if (IS_B_B && !IS_PUSH_B_B) { IS_PUSH_B_B = true; ON_B_B = true; }
            ON_B_X = false; if (!IS_B_X) IS_PUSH_B_X = false; if (IS_B_X && !IS_PUSH_B_X) { IS_PUSH_B_X = true; ON_B_X = true; }
            ON_B_Y = false; if (!IS_B_Y) IS_PUSH_B_Y = false; if (IS_B_Y && !IS_PUSH_B_Y) { IS_PUSH_B_Y = true; ON_B_Y = true; }

            if (!_isHit)
                _state = State.NORMAL;

            if (IS_B_R)
            {
                _state = State.BUILD;
            }

            if (IS_B_L)
            {
                _state = State.SELECT;
            }

            if (_spriteSword.IsPlay)  // Change State Mode to Sword Attack !
                _state = State.SWORD_ATTACK;


            // Hero Builder Mode !
            if (_state == State.BUILD)
            {

                if (IS_B_UP) MoveU(_speed, false);
                if (IS_B_DOWN) MoveD(_speed, false);
                if (IS_B_LEFT) MoveL(_speed, false);
                if (IS_B_RIGHT) MoveR(_speed, false);

            }

            // Hero Builder Mode !
            if (_state == State.SWORD_ATTACK)
            {

                if (IS_B_UP) MoveU(_speed / 2, false);
                if (IS_B_DOWN) MoveD(_speed / 2, false);
                if (IS_B_LEFT) MoveL(_speed / 2, false);
                if (IS_B_RIGHT) MoveR(_speed / 2, false);

            }

            //if (_state == State.IS_HIT)
            if (_isHit)
            {
                // Update MapActor !
                UpdateMapActor(gameTime);

                // Check if collide other object then stop move !
                if (_listCollideZone.Count > 0)
                {
                    if (_hitDirection.Y < 0 && (_collideP[(int)Position.NW]._isCollide || _collideP[(int)Position.NE]._isCollide)) _hitDirection.Y = 0;
                    if (_hitDirection.Y > 0 && (_collideP[(int)Position.SW]._isCollide || _collideP[(int)Position.SE]._isCollide)) _hitDirection.Y = 0;
                    if (_hitDirection.X < 0 && (_collideP[(int)Position.NW]._isCollide || _collideP[(int)Position.SW]._isCollide)) _hitDirection.X = 0;
                    if (_hitDirection.X > 0 && (_collideP[(int)Position.NE]._isCollide || _collideP[(int)Position.SE]._isCollide)) _hitDirection.X = 0;
                }

                Move(_hitDirection.X, _hitDirection.Y, _hitDirectionSpeed);

                _hitDirection = _hitDirection / 1.2f;

                if (_tickHit % 4 == 0) // Draw smoke when enemy is hit
                    new Smoke(_x, _y + 8, 16)
                        .AppendTo(_parent);


            }


            // Hero Normal Mode !
            if (_state == State.NORMAL)
            {
                if (IS_B_UP)     MoveU(_speed);
                if (IS_B_DOWN)   MoveD(_speed);
                if (IS_B_LEFT)   MoveL(_speed);
                if (IS_B_RIGHT)  MoveR(_speed);

                //if (B_A) // Command Pose bomb
                //    PoseBomb(_mapPosition.X, _mapPosition.Y);
                //else
                //    _poseBomb = false;


                if (IS_B_B && GetEnergy() > 10 && _isMove) // Command Turbo
                {
                    _isTurbo = true;
                    _speed = _turboSpeed;


                    // Malus when run
                    ++_tickTurbo;
                    if (_tickTurbo > _timerTurbo)
                    {
                        _tickTurbo = 0;
                        AddEnergy(-1);
                    }
                }
                else
                {
                    _isTurbo = false;
                    _speed = _normalSpeed;
                }

            }

            // trigger : when change direction
            if (_onChangeDirection)
            {
                // Set Animation by direction 
                if (_showDirection == Position.UP) _sprite.SetAnimation("Up");
                if (_showDirection == Position.DOWN) _sprite.SetAnimation("Down");
                if (_showDirection == Position.LEFT) _sprite.SetAnimation("Left");
                if (_showDirection == Position.RIGHT) _sprite.SetAnimation("Right");
            }

            // trigger : when stop move then set animation to 'Idle'
            if (_offMove)
            {
                //_sprite.SetAnimation("Idle");
            }

            if (_isMove)
            {
                _pZ += _vZ;

                if (_pZ < -_zMax) _vZ = _speedZ;
                if (_pZ >= 0) _vZ = -_speedZ;

                if (_isTurbo)
                {
                    _tickStep++;
                    if (_tickStep > _timerStep)
                    {
                        _tickStep = 0;
                        Game1._sound_footStep.Play(0.4f, 0.01f, 0);

                        new Smoke(_x, _y, 16)
                            .AppendTo(_parent);
                    }
                }

            }

            // Resume and Pause Animation if _isMove or not !
            if (_isMove)
                _sprite.Resume();
            else
            {
                _sprite.PlayAt(0);
                _sprite.Pause();
            }


            UpdateRect();

            // Update MapActor !
            UpdateMapActor(gameTime);

            // If hero is in Portal Zone agro then stop agro Hero !
            Collide.Zone collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Portal>(), Portal.ZONE_AGRO);

            _isSelected = false;
            _transparency = 1f;

            //if (null != collideZone) // Enemy in Portal Agro Zone
            //{
            //    Portal portal = collideZone._node.This<Portal>();
            //    // if hero is near portal
            //    if (Geo.IsNear(XY, portal.XY, 64))
            //    {
            //        _alpha = .6f;
            //        _isSelected = true;
            //    }
            //}


            // Sword Attack Collision Check 
            var listCollideZone = Collision2D.ListCollideZoneByNodeType
            (
                GetCollideZone(1),
                new int[]
                {
                    UID.Get<MapDecor>(),
                    UID.Get<Block>(),
                    UID.Get<MapBlock>(),
                    UID.Get<ItemCoin>(),
                    UID.Get<Enemy>(),
                    UID.Get<EnemyOmega>(),
                    UID.Get<Tower>()
                },
                new int[]
                {
                    MapDecor.ZONE_BODY,
                    Block.ZONE_BODY,
                    MapBlock.ZONE_BODY,
                    0,
                    Enemy.ZONE_BODY,
                    EnemyOmega.ZONE_BODY,
                    0
                }

            );

            if (listCollideZone.Count > 0)
            {
                bool hitMetal = false;
                bool hitOrganic = false;
                bool hitWood = false;
                bool hitIronWood = false;
                bool hitGlass = false;

                for (int i=0; i<listCollideZone.Count; ++i)
                {
                    //if (i > _nbMaxSwordHit) break;

                    Node collider = listCollideZone[i]._node;

                    Vector2 swordPos = XY + new Vector2(_rectSwordAttack.X + _rectSwordAttack.Width/2, _rectSwordAttack.Y + _rectSwordAttack.Height/2);
                    Vector2 impactPos = Geo.GetCenter(swordPos, collider.XY);

                    if (collider._type == UID.Get<Block>() ||
                        collider._type == UID.Get<MapDecor>() ||
                        collider._type == UID.Get<Tower>() ||
                        collider._type == UID.Get<MapBlock>())
                    {
                        // Hero recoil
                        float strength = collider.This<MapObject>()._strength;
                        HitMe(Geo.GetVector(collider.XY, XY, .2f), strength, (int)strength/2);
                        Shake(strength / 5, .1f);

                        // Particles effect
                        new Smoke(impactPos.X, impactPos.Y, 8, false)
                            .AppendTo(_parent);

                        new Spark(Color.White).SetPosition(impactPos)
                            .AppendTo(_parent);


                        if (collider._type == UID.Get<Tower>())
                        {
                            hitMetal = true;
                            collider.This<Tower>().Shake(3, 1f);
                        }

                        if (collider._type == UID.Get<MapBlock>())
                            hitMetal = true;

                        if (collider._type == UID.Get<Block>())
                        {
                            hitGlass = true;
                            collider.This<Block>().AddEnergy(-80);
                            collider.This<Block>().Shake(3, .1f);
                        }

                        if (collider._type == UID.Get<MapDecor>())
                        {
                            //hitWood = true;
                            //hitIronWood = true;
                            collider.This<MapDecor>().Shake(strength / 10, .1f);
                            collider.This<MapDecor>().HitDamage(-80);

                            //new Smoke(impactPos.X, impactPos.Y, 16, false, 32, 16)
                            //    .AppendTo(_parent);
                        }


                        
                    }

                    if (collider._type == UID.Get<ItemCoin>()) collider.This<ItemCoin>().GetMe(this);

                    if (collider._type == UID.Get<Enemy>())
                    {
                        Enemy enemy = collider.This<Enemy>();

                        // Hero recoil
                        float strength = collider.This<MapObject>()._strength;
                        HitMe(Geo.GetVector(collider.XY, XY, .2f), strength, (int)strength / 2);
                        Shake(strength / 5, .1f);

                        enemy.AddEnergy(-50);

                        enemy.HitMe(Geo.GetVector(XY, enemy.XY, .2f), 40, 30, 3); // Move to opposite Hero position

                        new PopInfo("-50", Color.Red)
                            .SetPosition(enemy._x, enemy._y - 32)
                            .AppendTo(_parent);

                        new Smoke(impactPos.X, impactPos.Y, 8, false)
                            .AppendTo(_parent);

                        new Spark(Color.White).SetPosition(impactPos)
                            .AppendTo(_parent);

                        hitOrganic = true;

                    }

                    if (collider._type == UID.Get<EnemyOmega>())
                    {
                        EnemyOmega enemy = collider.This<EnemyOmega>();

                        // Hero recoil
                        float strength = collider.This<MapObject>()._strength;
                        HitMe(Geo.GetVector(collider.XY, XY, .2f), strength, (int)strength / 2);
                        Shake(strength / 5, .1f);

                        enemy.AddEnergy(-50);

                        enemy.HitMe(Geo.GetVector(XY, enemy.XY, .2f), 40, 30, 3); // Move to opposite Hero position

                        new PopInfo("-50", Color.Red)
                            .SetPosition(enemy._x, enemy._y - 32)
                            .AppendTo(_parent);

                        new Smoke(impactPos.X, impactPos.Y, 8, false)
                            .AppendTo(_parent);

                        new Spark(Color.White).SetPosition(impactPos)
                            .AppendTo(_parent);

                        hitOrganic = true;

                    }


                }

                if (hitMetal) Game1._sound_swordHit.Play(.1f, .01f, 0);
                if (hitOrganic) Game1._sound_punch.Play(.1f, .01f, 0);
                if (hitWood) Game1._sound_hitWood.Play(.4f, 1f, 0); 
                if (hitIronWood) Game1._sound_hitIronWood.Play(.4f, 1f, 0); 
                if (hitGlass) Game1._sound_hitGlass.Play(.1f, .1f, 0); 
            }


            if (HasMessage())
            {
                Data data = (Data)_message._data;

                if (data._type == Data.Type.ADD_SCORE)
                    PlayerATW.AddBankPoint(data._value);
                    //_score += data._value;

                if (data._type == Data.Type.ADD_ENERGY)
                {
                    Console.WriteLine("Add Energy!");
                    AddEnergy(data._value);
                }

                if (data._type == Data.Type.ADD_POWER)
                {
                    Console.WriteLine("Add PowerBomb!");
                    AddBombPower(data._value);
                }

                if (data._type == Data.Type.ADD_MAX_ACTIVE_BOMB)
                {
                    Console.WriteLine("Add nbActiveBomb!");
                    SetMaxActiveBomb(data._value);
                }

                EndMessage();
            }

            if (_energy <= 0)
                _isDead = true;
            else
                _isDead = false;


            if (_isDead)
            {
                Console.WriteLine("Hero Dead ="+_name);
                _player.SetDead(true);

                _heroUX.KillMe();

                KillMe();
            }

            _barInfo.SetValue(_energy);

            UpdateChilds(gameTime);

            //_caseSelector.Update();

            //_caseSelector.SetMapPosition(_mapPosition.X + _builder._x, _mapPosition.Y + _builder._y);

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {
            if (InRectView())
            {
                // Direction Lock
                if (IS_B_R)
                    batch.Draw(Game1._tex_sokoban, new Vector2(AbsX - 16, AbsY - 8), new Rectangle(32, 0, 32, 16), Color.DeepSkyBlue);

                // if hero is selected
                if (_isSelected)
                    batch.Draw(Game1._tex_sokoban, new Vector2(AbsX - 16, AbsY - 8), new Rectangle(32, 0, 32, 16), Color.OrangeRed);


                // Blinking effect
                Color color = Color.White;
                if (_isHit)
                {
                    if (_tickHit % 4 == 0)
                        color = Color.DarkOrange;

                    if (_tickHit % 10 == 0)
                        color = Color.Red;
                }


                batch.Draw(Game1._tex_shadow,new Rectangle((int)(AbsX-_oX)+4, AbsY-6, 32-8, 16-4), Color.Black*.2f ); // Shadow


                if (_showDirection == Position.UP && _isAttackSword)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, -1.5708f, 10, -28);

                _sprite.Draw(batch, AbsX + _shake.GetVector2().X, AbsY + (int)_pZ + _shake.GetVector2().Y, color * _transparency); // Sprite

                if (_showDirection == Position.RIGHT && _isAttackSword)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 0, 8, -4);

                if (_showDirection == Position.LEFT && _isAttackSword)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 0, -40, -4, SpriteEffects.FlipHorizontally);

                if (_showDirection == Position.DOWN && _isAttackSword)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 1.5708f, -8, 0);



                //RenderDebugMapActor(batch);

                // Debug
                //batch.DrawCircle(AbsX(), AbsY(), 2, 8, Color.GhostWhite, 2);

                //Color color = Color.Purple;

                //if (AUTO_UP ||
                //    AUTO_DOWN ||
                //    AUTO_LEFT ||
                //    AUTO_RIGHT)
                //    color = Color.PowderBlue;

                //batch.DrawRectangle(AbsRectF(), color * .8f);
                //Draw.TopCenterString(batch, Game1._font_Main, (int)_rect.X + "," + (int)_rect.Y, AbsX(), AbsY() - 60, Color.Crimson);
                //Draw.TopCenterString(batch, Game1._font_Main, AbsX() + "," + AbsY(), AbsX(), AbsY() - 60, Color.Crimson);

                //if (GetCollideZone(0)._isCollide)
                //batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua, 1);

                //if (GetCollideZone(1)._isCollidable)
                //batch.DrawRectangle(Gfx.TranslateRect(GetCollideZone(1)._rect, _parent.AbsVectorXY()), Color.Red, 1);

                //Draw.CenterStringX(batch, Game1._mainFont, _nbActiveBomb.ToString(), AbsX(), AbsY() - _oY + 16, Color.LightGoldenrodYellow);

                //Draw.TopCenterString(batch, Game1._font_Main, _isDiagonalMove.ToString(), AbsX(), AbsY() + 48, Color.Crimson);

                RenderChilds(batch);

                //_barInfo.Render(batch);

            }


            return this;
        }

        // Status Getter 
        public int GetMaxActiveBomb() { return _maxActiveBomb; }
        public Hero SetMaxActiveBomb(int nb)
        {
            _maxActiveBomb += nb;
            return this;
        }
        public int GetActiveBomb() { return _nbActiveBomb; }
        public Hero SetNbActiveBomb(int nb)
        {
            _nbActiveBomb += nb;
            if (_nbActiveBomb <= 0) _nbActiveBomb = 0;
            return this;
        }
        public int GetBombPower() { return _wavePower; }
        public Hero SetBombPower(int power)
        {
            if (_wavePower <= 0 || _wavePower >= _maxWavePower) return this;
            _wavePower = power;
            return this;
        }
        public Hero AddBombPower(int point)
        {
            _wavePower += point;

            if (_wavePower <= 0) _wavePower = 0;
            if (_wavePower >= _maxWavePower) _wavePower = _maxWavePower;

            return this;
        }
        //public int GetScore() { return _score; }
        //public Hero AddScore(int score)
        //{
        //    _score += score;
        //    if (_score < 0) _score = 0;
        //    return this;
        //}
        public int GetEnergy()
        {
            if (_energy <= 0) _energy = 0;
            return _energy;
        }
        public int GetMaxEnergy() { return _maxEnergy; }
        public Hero SetEnergy(int energy)
        {
            if (_energy <= 0 || _energy >= _maxEnergy) return this;
            _energy = energy;
            return this;
        }
        public Hero AddEnergy(int point)
        {
            _energy += point;

            if (_energy <= 0) _energy = 0;
            if (_energy >= _maxEnergy) _energy = _maxEnergy;

            return this;
        }

        public void PoseBomb(int mapX, int mapY)
        {
            if (!_poseBomb)
            {
                _poseBomb = true;

                if (_nbActiveBomb < _maxActiveBomb && PlayerATW.GetBankPoint() >= 10) // Don't pose bomb is _maxActiveBomb reach !
                {

                    if (Bomb.Add(_map2D, _parent, mapX, mapY, 120, _wavePower, _waveTimer, this))
                    {
                        new PopInfo("-10", Color.Red)
                            .SetPosition(_x, _y - 32)
                            .AppendTo(_parent);

                        PlayerATW.AddBankPoint(-10);

                        Game1._sound_poseBomb.Play(0.1f, 0.4f, 0);
                    }

                }
            }

        }


    }
}
