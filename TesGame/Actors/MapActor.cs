﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;


namespace ArenaTotalWar
{
    public enum CPoint
    {
        UL, UR, // Collide TOP
        DL, DR, // Collide BOTTOM
        LU, LD, // Collide LEFT
        RU, RD, // Collide RIGHT
    }

    public class MapActor : MapObject
    {
        // Use for List Collision Point Detection
        public class CollidePoint
        {
            public float _x;
            public float _y;
            public int _position;
            public bool _isCollide = false;

            public CollidePoint(float x, float y)
            {
                _x = x;
                _y = y;
            }
        }

        #region Attributes

        // Collision

        public List<Collide.Zone> _listCollideZone = new List<Collide.Zone>();

        // Map Actor Movement
        public float _speed = 2f;

        public bool _isMove = false;
        public bool _isDiagonalMove = false;
        public bool _lastMove = false;
        public bool _onMove = false;
        public bool _offMove = false;


        // Follow path 
        public List<Point> _path = new List<Point>();
        public bool _isGoal = false;
        public bool _onGoal = false;
        public Vector2 _moveVector = new Vector2();
        public float _goalX;
        public float _goalY;

        public bool _isFollow = false;

        // Map Actor Direction
        public bool _onChangeDirection = false;
        public Position _direction = Position.DOWN;
        public Position _lastDirection = Position.DOWN;
        public Position _showDirection = Position.DOWN;

        // contrainte mouvement
        public bool MOVE_UP = true;
        public bool MOVE_DOWN = true;
        public bool MOVE_LEFT = true;
        public bool MOVE_RIGHT = true;

        // Mouvement de décalage automatique si passage sur le côté libre !
        public bool AUTO_MOVE = true;  // Allow auto move or not
        public bool AUTO_UP = false;
        public bool AUTO_DOWN = false;
        public bool AUTO_LEFT = false;
        public bool AUTO_RIGHT = false;

        // Active Direction !
        public bool D_UP = false;
        public bool D_DOWN = false;
        public bool D_LEFT = false;
        public bool D_RIGHT = false;

        #endregion 

        // Collide points
        public Dictionary<int, CollidePoint> _collideP = new Dictionary<int, CollidePoint>();

        public MapActor(Map2D<Tile> map2D) : base(map2D)
        {
            _type = UID.Get<MapActor>();

            SetCollideZone(0, _rect);
        }

        public void AddPoint(int position, CollidePoint collideP)
        {
            _collideP[position] = collideP;
        }
        public CollidePoint GetPoint(int position)
        {
            if (_collideP.ContainsKey(position))
                return _collideP[position];

            return null;
        }

        public void UpdateMapActor(GameTime gameTime)
        {

            MOVE_UP = true;
            MOVE_DOWN = true;
            MOVE_LEFT = true;
            MOVE_RIGHT = true;

            AUTO_MOVE = true;
            AUTO_UP = false;
            AUTO_DOWN = false;
            AUTO_LEFT = false;
            AUTO_RIGHT = false;

            // -------------------------------------------------------------- Check Collisions !

            UpdateRect();

            if (_collideP.Count > 0)
            {
                // Reset all collide point to false !
                for (int i = _collideP.Keys.First(); i < _collideP.Count; i++)
                {
                    _collideP[i]._isCollide = false;
                }



                // ----------------------------------------------------------- Collision with TileMap
                for (int i = _collideP.Keys.First(); i < _collideP.Count; i++)
                {
                    int tileW = _map2D._tileW;
                    int tileH = _map2D._tileH;

                    float pX = _x + _collideP[i]._x;
                    float pY = _y + _collideP[i]._y;

                    int mapX = (int)(pX / tileW);
                    int mapY = (int)(pY / tileH);

                    Tile tile = _map2D.Get(mapX, mapY);

                    if (null != tile)
                        if (tile._isCollidable && tile._passLevel > 1)
                        {
                            if (i == (int)CPoint.UL || i == (int)CPoint.UR) { MOVE_UP = false; _collideP[i]._isCollide = true; }
                            if (i == (int)CPoint.DL || i == (int)CPoint.DR) { MOVE_DOWN = false; _collideP[i]._isCollide = true; }

                            if (i == (int)CPoint.LU || i == (int)CPoint.LD) { MOVE_LEFT = false; _collideP[i]._isCollide = true; }
                            if (i == (int)CPoint.RU || i == (int)CPoint.RD) { MOVE_RIGHT = false; _collideP[i]._isCollide = true; }
                        }

                }

                // ----------------------------------------------------------- Collision with MapBlock

                _listCollideZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(0), new int[] { UID.Get<MapBlock>(), UID.Get<Bomb>(), UID.Get<Block>(), UID.Get<MapDecor>(), UID.Get<Tower>() }, 0);
                //_listCollideZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(0), new int[] { UID.Get<Block>() /*, UID.Get<Bomb>(), UID.Get<Block>(), UID.Get<MapDecor>(), UID.Get<Tower>()*/ }, 0);


                // Check if the tile where Actor is not collidable before check collision with other objects !

                Tile tileActor = _map2D.Get(_mapPosition.X, _mapPosition.Y);

                if (null != tileActor)
                    if (_listCollideZone.Count > 0 && !tileActor._isCollidable)
                    {

                        for (int index = 0; index < _listCollideZone.Count; index++)
                        {
                            Node collider = _listCollideZone[index]._node;

                            //Console.Write("< Collide by MapBlock : {0} > ", collider._index);

                            for (int i = _collideP.Keys.First(); i < _collideP.Count; i++)
                            {

                                float absX = _x + _collideP[i]._x;
                                float absY = _y + _collideP[i]._y;

                                if (Collision2D.PointInRect(new Vector2(absX, absY), collider.GetCollideZone(0)._rect))
                                {
                                    if ((i == (int)CPoint.UL || i == (int)CPoint.UR)) { MOVE_UP = false; _collideP[i]._isCollide = true; }
                                    if ((i == (int)CPoint.DL || i == (int)CPoint.DR)) { MOVE_DOWN = false; _collideP[i]._isCollide = true; }

                                    if ((i == (int)CPoint.LU || i == (int)CPoint.LD)) { MOVE_LEFT = false; _collideP[i]._isCollide = true; }
                                    if ((i == (int)CPoint.RU || i == (int)CPoint.RD)) { MOVE_RIGHT = false; _collideP[i]._isCollide = true; }
                                }
                            }

                        }
                    }

                // Test Auto Move !!
                if (AUTO_MOVE)
                {
                    for (int i = _collideP.Keys.First(); i < _collideP.Count; i++)
                    {
                        if (!MOVE_UP && !_collideP[i]._isCollide && D_UP && !(D_LEFT || D_RIGHT))
                        {
                            //Console.Write("< AUTO UP > "+ i);
                            if (i == (int)CPoint.UL && MOVE_LEFT) AUTO_LEFT = true;
                            if (i == (int)CPoint.UR && MOVE_RIGHT) AUTO_RIGHT = true;
                        }
                        if (!MOVE_DOWN && !_collideP[i]._isCollide && D_DOWN && !(D_LEFT || D_RIGHT))
                        {
                            //Console.Write("< AUTO DOWN >"+i);
                            if (i == (int)CPoint.DL && MOVE_LEFT) AUTO_LEFT = true;
                            if (i == (int)CPoint.DR && MOVE_RIGHT) AUTO_RIGHT = true;
                        }
                        if (!MOVE_LEFT && !_collideP[i]._isCollide && D_LEFT && !(D_UP || D_DOWN))
                        {
                            //Console.Write("< AUTO LEFT > " + i);
                            if (i == (int)CPoint.LU && MOVE_UP) AUTO_UP = true;
                            if (i == (int)CPoint.LD && MOVE_DOWN) AUTO_DOWN = true;
                        }
                        if (!MOVE_RIGHT && !_collideP[i]._isCollide && D_RIGHT && !(D_UP || D_DOWN))
                        {
                            //Console.Write("< AUTO RIGHT >" + i);
                            if (i == (int)CPoint.RU && MOVE_UP) AUTO_UP = true;
                            if (i == (int)CPoint.RD && MOVE_DOWN) AUTO_DOWN = true;
                        }
                    }

                }

            }

            // Manage Actor direction show !
            if ((_lastDirection == Position.UP && !(D_LEFT || D_RIGHT)) ||
                (_lastDirection == Position.DOWN && !(D_LEFT || D_RIGHT)) ||
                (_lastDirection == Position.LEFT && !(D_UP || D_DOWN)) ||
                (_lastDirection == Position.RIGHT && !(D_UP || D_DOWN)))
                _showDirection = _direction;

            // Check if diagonal Move !

            if ((D_UP && (D_LEFT || D_RIGHT)) ||
                (D_DOWN && (D_LEFT || D_RIGHT)) ||
                (D_LEFT && (D_UP || D_DOWN)) ||
                (D_RIGHT && (D_UP || D_DOWN)))
                _isDiagonalMove = true;
            else
                _isDiagonalMove = false;

            // Reset Button Direction status
            D_UP = false;
            D_DOWN = false;
            D_LEFT = false;
            D_RIGHT = false;

            _onChangeDirection = false;

            if (_lastMove != _isMove) // Pattern Actor Move on / off 
            {
                _lastMove = _isMove;
                // reset Move status
                _onMove = false;
                _offMove = false;

                if (_isMove)
                {
                    _onMove = true;
                }
                else
                {
                    _offMove = true;
                }
            }

            if (_onMove || (_lastDirection != _direction))
            {
                _lastDirection = _direction;
                _onChangeDirection = true;
            }

            _isMove = false;

            base.Update(gameTime);

        }
        public void RenderDebugMapActor(SpriteBatch batch)
        {
            int tileW = _map2D._tileW;
            int tileH = _map2D._tileH;

            int X = AbsX / tileW;
            int Y = AbsY / tileH;
            //batch.DrawRectangle(new Rectangle(X * tileW, Y * tileH, tileW, tileH), Color.LightSlateGray, 1); // Center

            // Draw Path
            if (_path.Count > 0)
            {
                Point prevPoint = new Point(_path[0].X, _path[0].Y);
                int index = 0;
                foreach (Point point in _path)
                {
                    Vector2 grafVecPrev = new Vector2(prevPoint.X * tileW + tileW / 2, prevPoint.Y * tileH + tileH / 2);
                    Vector2 grafVec = new Vector2(point.X * tileW + tileW / 2, point.Y * tileH + tileH / 2);

                    Draw.Line(batch, grafVecPrev + _parent.AbsXY, grafVec + _parent.AbsXY, Color.RoyalBlue * 0.2f, 1);
                    //batch.DrawCircle(grafVec, 4, 8, Color.RoyalBlue, 4);

                    batch.DrawString(Game1._font_Main, "" + index, new Vector2(grafVec.X - 4, grafVec.Y - 12) + _parent.AbsXY, Color.Firebrick);

                    prevPoint = point;
                    index++;
                }
            }

            // Debug : Draw _collideP
            foreach (KeyValuePair<int, CollidePoint> point in _collideP)
            {
                int size = 1;

                if (null != point.Value)
                {

                    float pX = AbsX + point.Value._x;
                    float pY = AbsY + point.Value._y;

                    int mapX = (int)(pX / tileW);
                    int mapY = (int)(pY / tileH);

                    Tile tN = _map2D.Get(mapX, mapY - 1);
                    Tile tS = _map2D.Get(mapX, mapY + 1);
                    Tile tW = _map2D.Get(mapX - 1, mapY);
                    Tile tE = _map2D.Get(mapX + 1, mapY);

                    //if (point.Key == (int)Position.NW)
                    //    batch.DrawRectangle(new Rectangle((mapX - 1) * tileW, mapY * tileH, tileW, tileH), Color.MonoGameOrange, 3); // W

                    //if (point.Key == (int)Position.NE)
                    //    batch.DrawRectangle(new Rectangle((mapX + 1) * tileW, mapY * tileH, tileW, tileH), Color.MonoGameOrange, 3); // E

                    //if (point.Key == (int)Position.NW)
                    //    batch.DrawRectangle(new Rectangle(mapX * tileW, (mapY - 1) * tileH, tileW, tileH), Color.MonoGameOrange, 3); // N

                    //if (point.Key == (int)Position.SE)
                    //    batch.DrawRectangle(new Rectangle(mapX * tileW, (mapY + 1) * tileH, tileW, tileH), Color.MonoGameOrange, 3); // S

                    //batch.DrawRectangle(new Rectangle(mapX * tileW, (mapY - 1) * tileH, tileW, tileH), Color.MonoGameOrange, 3); // N
                    //batch.DrawRectangle(new Rectangle(mapX * tileW, (mapY + 1) * tileH, tileW, tileH), Color.MonoGameOrange, 3); // S
                    //batch.DrawRectangle(new Rectangle((mapX - 1) * tileW, mapY * tileH, tileW, tileH), Color.MonoGameOrange, 3); // W
                    //batch.DrawRectangle(new Rectangle((mapX + 1) * tileW, mapY * tileH, tileW, tileH), Color.MonoGameOrange, 3); // E

                    Color color = Color.GreenYellow;

                    if (point.Value._isCollide)
                        color = Color.Red;

                    //Draw.RectFill(batch, new Rectangle((int)(pX - size / 2), (int)(pY - size / 2), size, size), color);

                    Draw.FillSquare(batch, new Vector2(pX - size / 2, pY - size / 2), size, color);

                    //batch.DrawCircle(point.Value._abs.X - 2, point.Value._abs.Y, 2, 4, Color.Green);




                }
            }

            //var listCollideZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(0), UID.Get<MapBlock>(), 0);

            //batch.DrawString(Game1._mainFont, " : " + listCollideZone.Count, new Vector2(AbsX() - 24, AbsY() - 42), Color.MonoGameOrange);

        }

        public void StartFollowPath(List<Point> path)
        {
            _path = path;
            _onGoal = true;
            _isGoal = false;

            ResumeFollowPath();

            if (_path.Count > 2) _path.RemoveAt(0); // Remove first point of path !
        }
        public void PauseFollowPath()
        {
            _isFollow = false;
        }
        public void ResumeFollowPath()
        {
            _isFollow = true;
        }
        public Point GetPathPoint(int index)
        {
            return new Point((int)_path[index].X / _map2D._tileW, (int)(_path[index].Y / _map2D._tileH));
        }
        public Point GetFirstPathPoint()
        {
            if (_path.Count > 0)
                return new Point((int)_path[0].X / _map2D._tileW, (int)(_path[0].Y / _map2D._tileH));

            return default(Point);
        }
        public Point GetLastPathPoint()
        {
            if (_path.Count > 0)
            {
                int index = _path.Count - 1;
                return new Point((int)_path[index].X / _map2D._tileW, (int)(_path[index].Y / _map2D._tileH));
            }
            return default(Point);
        }
        public void FollowPath(float distMin = 1)
        {
            if (_isFollow && _path.Count > 0)
            {
                _isMove = true;

                if (_onGoal)
                {
                    _onGoal = false;
                    _goalX = _path[0].X * _map2D._tileW + _map2D._tileW / 2;
                    _goalY = _path[0].Y * _map2D._tileH + _map2D._tileH / 2;
                }

                _moveVector = Geo.GetVector(new Vector2(_x, _y), new Vector2(_goalX, _goalY), _speed);

                // _showDirection Management
                if (Math.Abs(_moveVector.X) < Math.Abs(_moveVector.Y)) // Up or Down
                {
                    if (_moveVector.Y < 0)
                        _showDirection = Position.UP;
                    else
                        _showDirection = Position.DOWN;
                }
                else // Left or Right // priority for horizontal
                {
                    if (_moveVector.X < 0)
                        _showDirection = Position.LEFT;
                    else
                        _showDirection = Position.RIGHT;
                }

                _x += _moveVector.X;
                _y += _moveVector.Y;

                if (Math.Abs(_x - _goalX) <= distMin &&
                    Math.Abs(_y - _goalY) <= distMin)
                {
                    _isGoal = true;
                    _onGoal = true;
                    _path.RemoveAt(0); // Remove first point of path !
                }
            }


        }
        public bool IsReachGoal() { return _path.Count == 0; }
        public void Move(float vx, float vy, float speed)
        {
            _isMove = true; // Set Move Status 

            if (_isDiagonalMove)
            {
                vx = vx / 1.414f;
                vy = vy / 1.414f;
            }

            _x += vx * speed;
            _y += vy * speed;
        }
        public void MoveU(float speed, bool setDirection = true)
        {
            if (setDirection)
                _direction = Position.UP;

            D_UP = true;

            if (AUTO_LEFT) Move(-1, 0, speed);
            if (AUTO_RIGHT) Move(1, 0, speed);
            if (!AUTO_UP && MOVE_UP) Move(0, -1, speed);
        }
        public void MoveD(float speed, bool setDirection = true)
        {
            if (setDirection)
                _direction = Position.DOWN;

            D_DOWN = true;

            if (AUTO_LEFT) Move(-1, 0, speed);
            if (AUTO_RIGHT) Move(1, 0, speed);
            if (!AUTO_DOWN && MOVE_DOWN) Move(0, 1, speed);
        }
        public void MoveL(float speed, bool setDirection = true)
        {
            if (setDirection)
                _direction = Position.LEFT;

            D_LEFT = true;

            if (AUTO_UP) Move(0, -1, speed);
            if (AUTO_DOWN) Move(0, 1, speed);
            if (!AUTO_LEFT && MOVE_LEFT) Move(-1, 0, speed);
        }
        public void MoveR(float speed, bool setDirection = true)
        {
            if (setDirection)
                _direction = Position.RIGHT;

            D_RIGHT = true;

            if (AUTO_UP) Move(0, -1, speed);
            if (AUTO_DOWN) Move(0, 1, speed);
            if (!AUTO_RIGHT && MOVE_RIGHT) Move(1, 0, speed);
        }
    }
}
