﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;


namespace ArenaTotalWar
{
    public class EnemyOmega : MapActor
    {
        public enum State
        {
            NORMAL,
            IS_HIT,
            IS_ATTACK
        }


        #region Attributes

        public const int ZONE_AGRO = 1;
        public const int ZONE_BODY = 2;

        //Target _targetPortal;

        public State _state = State.NORMAL;

        Shake _shake;
        Sprite _sprite;

        //Addon.Draggable _drag;

        float _zMax = 2.0f;
        const float _speedZ = .5f;
        float _pZ = 0;
        float _vZ = -_speedZ;
        float _transparency = 1f;

        // Abs Rect Limiter
        public RectangleF _limitRect;

        BarInfo _barInfo;

        int _energy = 300;
        int _maxEnergy = 300;

        //Target _targetMap;

        // When enemy is detected by portal
        bool _isSelected = false;
        float _alphaWarning;

        bool _isAgro = false;
        bool _onAgro = false;
        int _tickAgro = 0;
        int _tempoAgro = 240; // Tempo to find another path
        int _agroRange = 48; // Attack Zone

        bool _isDead = false;
        bool _isHit = false;
        int _tickHit = 0;
        int _tempoHit = 20;
        //bool _poseBomb = false;

        // Player Index to Kill
        Point _start = new Point();
        Point _end = new Point();
        //bool _isFollow = true;

        bool _isPursuit = false;
        int _tempoPursuit; // Tempo when enemy pursuit hero !

        MapObject _target; // target to follow !
        MapObject _targetPortal; // target to follow !

        Vector2 _hitDirection = new Vector2();
        float _hitDirectionSpeed = 1f;

        #endregion 

        public EnemyOmega(Map2D<Tile> map2D, MapObject target) : base(map2D)
        {
            // Set Type 
            _type = UID.Get<EnemyOmega>();

            _strength = 12;

            // Create all Addons !
            //_drag = CreateAddon<Addon.Draggable>();

            _shake = new Shake();

            _target = target;
            _targetPortal = target;

            // Set Size and Pivot of Hero
            SetSize(32, 32);
            SetPivot(16, 16);

            _speed = 0.5f; // normal speed 
            //_speed = 1.0f; // attack speed

            // Create new Sprite
            //_sprite = Game1._spriteHero.Clone();
            _sprite = Game1._sprite_omega.Clone();

            // Create Collide Point of Hero
            AddPoint((int)Position.NW, new CollidePoint(-10, -12));
            AddPoint((int)Position.NE, new CollidePoint(10, -12));
            AddPoint((int)Position.SW, new CollidePoint(-10, 4));
            AddPoint((int)Position.SE, new CollidePoint(10, 4));

            SetCollideZone(1, _rect);
            SetCollideZone(ZONE_BODY, _rect); // Zone of Enemy Body

            _animate.Add("popup", Easing.BackEaseInOut, new Tweening(0, 16, 8));
            _animate.Start("popup");

            //_barInfo = new BarInfo(this, Color.Green, Color.Firebrick, Color.GhostWhite, _maxEnergy, 24, 2);
            _barInfo = new BarInfo(this, Color.Yellow, Color.Firebrick, Color.GhostWhite * .2f, _maxEnergy, 24, 4, 1);
            _barInfo.SetY(-16);
            _barInfo.AppendTo(this);
            _barInfo.AddWarning(new BarInfo.Warning(_maxEnergy / 6, _maxEnergy / 3, 80));
            _barInfo.AddWarning(new BarInfo.Warning(0, _maxEnergy / 6, 40));
        }

        public override Node Init()
        {
            //_drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH - 60));
            //_drag.SetLimitRect(true);
            _limitRect = _parent.AbsRectF;

            _sprite.Start("Idle", 1, 0);

            // Debug : Start follow when created !
            //_start = GetMapPosition();
            //_end = Game1._heros[0].GetMapPosition();
            //StartFollowPath(new Astar<Tile>(_map2D.GetMap(), _start, _end, Find.Diagonal)._path);

            // Active FollowPath at Init !
            _tickAgro = 220;
            ResumeFollowPath();

            return this;
        }

        public static EnemyOmega Add(Node parent, Map2D<Tile> map2D, int mapX, int mapY, MapObject target)
        {
            //Game1._sound_zombieTalking.Play(1.0f, 0.2f, 0);

            EnemyOmega enemy = (EnemyOmega)new EnemyOmega(map2D, target)
                .AppendTo(parent)
                .This<EnemyOmega>().GoTo(mapX, mapY)
                .Init();

            return enemy;
        }

        public EnemyOmega HitMe(Vector2 hitDirection, float hitDirectionSpeed = 1f, int tempoHit = 20, float intensity = 1, float step = .05f)
        {

            _hitDirection = hitDirection;
            _hitDirectionSpeed = hitDirectionSpeed;
            _tempoHit = tempoHit;
            _tickHit = 0;
            _shake.SetIntensity(intensity, step);

            _isHit = true;

            return this;
        }

        public EnemyOmega SetTarget(MapObject target)
        {
            _target = target;

            return this;
        }

        public override Node Update(GameTime gameTime)
        {

            // Update Addons
            //_drag.Update(Game1._mouse);
            _sprite.Update();

            UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(4, 4, -8, -8)), true, 0); // collideLevel = 0 , detect collide with everything !

            UpdateCollideZone(ZONE_BODY, Gfx.AddRect(_rect, new Rectangle(4, -8, -8, 4)), true, 0); // 

            // Optimization check Agro only when _tickAgro > _timerAgro !!
            //_isAgro = false;
            _onAgro = false;
            _tickAgro++;
            if (_tickAgro > _tempoAgro)
            {
                _tickAgro = 0;
                _isAgro = true;
                _onAgro = true;
            }

            UpdateCollideZone(ZONE_AGRO, Gfx.AddRect(_rect, new Rectangle(-_agroRange, -_agroRange, _agroRange * 2, _agroRange * 2)));

            // Y to Z order :
            _z = -(int)_y;

            // Limit movement
            if (_x < 0) _x = 0;
            if (_x > _limitRect.Width) _x = _limitRect.Width;
            if (_y < 0) _y = 0;
            if (_y > _limitRect.Height) _y = _limitRect.Height;


            if (_isHit)
            {
                _state = State.IS_HIT;

                _tickHit++;

                if (_tickHit > _tempoHit)
                {
                    _tickHit = 0;

                    _state = State.NORMAL;
                    _isHit = false;
                    //_hitDirection = new Vector2();
                }
            }


            // Enemy State NORMAL
            if (_state == State.NORMAL)
            {
                // trigger : when change direction
                if (_onChangeDirection)
                {
                    // Set Animation by direction 
                    if (_showDirection == Position.UP) _sprite.SetAnimation("Up");
                    if (_showDirection == Position.DOWN) _sprite.SetAnimation("Down");
                    if (_showDirection == Position.LEFT) _sprite.SetAnimation("Left");
                    if (_showDirection == Position.RIGHT) _sprite.SetAnimation("Right");
                }

                // trigger : when stop move then set animation to 'Idle'
                if (_offMove)
                {
                    _sprite.SetAnimation("Idle");
                }

                if (_isMove)
                {
                    _pZ += _vZ;

                    if (_pZ < -_zMax) _vZ = _speedZ;
                    if (_pZ >= 0) _vZ = -_speedZ;

                }

                if (_isPursuit)
                    _speed = 1f;
                else
                    _speed = 0.5f + (float)(Misc.Rng.NextDouble() / 4);

                // Resume and Pause Animation if _isMove or not !
                //if (_isMove)
                //    _sprite.Resume();
                //else
                //    _sprite.Pause();

                //UpdateRect();

                // Update MapActor !
                UpdateMapActor(gameTime);

                // If enemy is in Portal Zone agro then stop agro Hero !
                Collide.Zone collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(ZONE_BODY), UID.Get<Portal>(), Portal.ZONE_AGRO);

                if (null != collideZone) // Enemy in Portal Agro Zone
                {

                    Portal portal = collideZone._node.This<Portal>();

                    portal.Warning();

                    _transparency = .6f;
                    _isSelected = true;
                    _alphaWarning = portal.GetAlphaWarning();


                    collideZone = null;
                }
                else
                {
                    _isSelected = false;

                    _transparency = 1f;
                    collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(ZONE_AGRO), UID.Get<Hero>(), 0);
                }

                if (null != collideZone)
                {
                    if (!_isPursuit)
                    {
                        Hero hero = collideZone._node.This<Hero>();

                        if (Misc.Rng.Next(0, 4) >= 2)
                            Game1._sound_zombieMoan.Play(0.2f, 0.05f, 0);
                        else
                            Game1._sound_zombieKillYou.Play(0.2f, 0.1f, 0);

                        new PopInfo("Agro", Color.Red)
                            .SetPosition(_x, _y - 32)
                            .AppendTo(_parent);

                        _animate.Start("popup");

                        // Create path to Hero
                        _start = _mapPosition;
                        _end = hero.This<Hero>()._mapPosition;

                        if (_map2D.IsInMap(_start) && _map2D.IsInMap(_end))
                        {
                            List<Point> path = new Astar<Tile>(_map2D.GetMap(), _start, _end, Find.Diagonal, 1)._path;

                            if (path.Count > 0) // Pursuit only if find path !
                            {

                                _isPursuit = true;
                                _tempoPursuit = 480;

                                SetTarget(hero.This<MapObject>());

                            }

                        }


                    }

                }



                if (_isPursuit)
                {
                    _tickAgro = _tempoAgro;

                    if (IsReachGoal()) // Reach the target
                    {
                        Console.WriteLine("Reach Hero Position");

                        _state = State.IS_ATTACK;
                    }

                    --_tempoPursuit;
                    if (_tempoPursuit < 0 || _path.Count == 0)
                    {
                        _tickAgro = 0;
                        _tempoPursuit = 0;

                        _isPursuit = false;

                        // ReTarget Portal
                        SetTarget(_targetPortal);
                    }
                }

                if (_onAgro)
                {
                    _start = _mapPosition;
                    _end = _target._mapPosition;

                    if (_map2D.IsInMap(_start) && _map2D.IsInMap(_end))
                    {
                        List<Point> path = new Astar<Tile>(_map2D.GetMap(), _start, _end, Find.Diagonal, 1)._path;

                        if (path.Count > 0) // if find path !
                        {
                            if (!_isPursuit)
                                StartFollowPath(path);
                            else
                                if (path.Count > 2)
                                StartFollowPath(path);
                        }

                    }
                }
                if (!_isPursuit)
                    FollowPath(4);
                else
                    FollowPath(8);

            }

            if (_state == State.IS_ATTACK)
            {
                Collide.Zone collideZone;

                collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(ZONE_AGRO), UID.Get<Hero>(), 0);
                if (null != collideZone)
                {
                    Node collider = collideZone._node;

                    if (collider._type == UID.Get<Hero>())
                    {
                        Hero hero = collider.This<Hero>();

                        // Stop Attack if out of enemy range
                        if (Geo.IsNear(XY, hero.XY, 24))
                            SetTarget(hero);
                        else
                        {
                            _state = State.NORMAL;
                            _tickAgro = _tempoAgro;
                            _isPursuit = false;
                        }

                    }

                }
                else
                {
                    // if Hero is out of zone then return to Normal State
                    _state = State.NORMAL;
                    _tickAgro = _tempoAgro;
                    _isPursuit = false;
                }




                collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Hero>(), 0);
                if (null != collideZone)
                {
                    Node collider = collideZone._node;

                    if (collider._type == UID.Get<Hero>())
                    {
                        Hero hero = collider.This<Hero>();

                        if (!hero.IsHit())
                        {

                            hero.HitMe(Geo.GetVector(XY, hero.XY, .2f), 30, 20); // Move to opposite enemy position
                            hero.Shake(3, .1f);
                            hero.PlaySoundHurt(hero.Sex);

                            hero.AddEnergy(-2);
                            new PopInfo("-2", Color.Red)
                                .SetPosition(collider._x, collider._y - 32)
                                .AppendTo(_parent);

                        }

                    }
                }
                else
                {
                    // if Hero is out of zone then return to Normal State
                    //_state = State.NORMAL;
                    //_tickAgro = _timerAgro;
                    //_isPursuit = false;

                    //FollowPath(8);
                }

            }

            if (_state == State.IS_HIT)
            {
                // Update MapActor !
                UpdateMapActor(gameTime);

                // Check if collide other object then stop move !
                if (_listCollideZone.Count > 0)
                {
                    if (_hitDirection.Y < 0 && (_collideP[(int)Position.NW]._isCollide || _collideP[(int)Position.NE]._isCollide)) _hitDirection.Y = 0;
                    if (_hitDirection.Y > 0 && (_collideP[(int)Position.SW]._isCollide || _collideP[(int)Position.SE]._isCollide)) _hitDirection.Y = 0;
                    if (_hitDirection.X < 0 && (_collideP[(int)Position.NW]._isCollide || _collideP[(int)Position.SW]._isCollide)) _hitDirection.X = 0;
                    if (_hitDirection.X > 0 && (_collideP[(int)Position.NE]._isCollide || _collideP[(int)Position.SE]._isCollide)) _hitDirection.X = 0;
                }

                Move(_hitDirection.X, _hitDirection.Y, _hitDirectionSpeed);

                _hitDirection = _hitDirection / 1.2f;

                if (_tickHit % 4 == 0) // Draw smoke when enemy is hit
                    new Smoke(_x, _y + 8, 16)
                        .AppendTo(_parent);


            }


            // Messages
            if (HasMessage())
            {
                Data data = (Data)_message._data;

                //Console.WriteLine(data._msg);

                //if (data._type == Data.Type.ADD_SCORE)
                //    _score += data._value;

                //AddEnergy(20);

                EndMessage();
            }

            if (_energy <= 0 && !_isDead)
            {
                _isDead = true;
            }

            if (_isDead)
            {
                //Console.WriteLine("Enemy Dead =" + _name);
                //_player.SetDead(true);

                Game1._sound_zombieDead.Play(.2f, .0005f, 0);

                Game1.PopItem(_map2D, _parent, _mapPosition.X, _mapPosition.Y, 50);

                // Dessine une tâche de sang qui disparait tout seul
                var blood = new Blood(Game1._tex_blood, _x, _y).AppendTo(_parent);

                new FireExplosion(_x, _y).AppendTo(_parent);

                KillMe();

            }


            _barInfo.SetValue(_energy);

            UpdateChilds(gameTime);

            _animate.NextFrame();

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {

            batch.Draw(Game1._tex_shadow, new Rectangle((int)(AbsX - _oX) + 4, AbsY - 2, 32 - 8, 16 - 4), Color.Black * .2f); // Shadow

            if (_isSelected)
                batch.Draw(Game1._tex_sokoban, new Vector2(AbsX - 16, AbsY - 8), new Rectangle(32, 0, 32, 16), Color.Red * _alphaWarning);

            // Blinking effect
            Color color = Color.White;
            if (_isHit)
            {
                if (_tickHit % 4 == 0)
                    color = Color.DarkOrange;

                if (_tickHit % 10 == 0)
                    color = Color.Red;
            }

            _sprite.Draw(batch, AbsX + _shake.GetVector2().X, AbsY + _pZ + _shake.GetVector2().Y, color * _transparency); // Sprite

            //RenderDebugMapActor(batch);

            // Debug
            //batch.DrawCircle(AbsX(), AbsY(), 2, 8, Color.GhostWhite, 2);

            //Color color = Color.Purple;

            //if (AUTO_UP ||
            //    AUTO_DOWN ||
            //    AUTO_LEFT ||
            //    AUTO_RIGHT)
            //    color = Color.PowderBlue;

            //batch.DrawRectangle(AbsRectF(), color * .8f);
            //Draw.CenterStringX(batch, Game1._mainFont, AbsX() + "," + AbsY() + ":" + _sprite._curFrame, AbsX(), AbsY() + 16, Color.Crimson);

            //if (GetCollideZone(0)._isCollide)
            //batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua, 1);
            //batch.DrawRectangle(GetCollideZone(1)._rect, Color.Aqua, 1);
            //batch.DrawRectangle(Gfx.TranslateRect(GetCollideZone(2)._rect, _parent.AbsVectorXY()), Color.Red, 1);

            //Draw.CenterStringX(batch, Game1._mainFont, _command._node._name + ":"+_score , AbsX(), AbsY() - 34, Color.LightGoldenrodYellow);

            // Debug Follow Target !
            //Draw.CenterStringXY(batch, Game1._font_Main, _path.Count.ToString() + " : " + _tickAgro + " : "+ _tempoPursuit, AbsX(), AbsY() - 64, Color.LightGoldenrodYellow);
            //batch.DrawRectangle(_target.AbsRectF(), Color.Red, 2);

            if (_state == State.IS_ATTACK)
                Draw.CenterBorderedStringXY(batch, Game1._font_Main, "Attack", AbsX, AbsY - 48, Color.Red, Color.Black);

            if (_isPursuit)
                batch.Draw(Game1._tex_exclamation, new Vector2(AbsX - 4, AbsY - _oY - (20 + _animate.Value())), Color.White);

            RenderChilds(batch);

            return this;
        }

        // Status Getter 
        public int GetEnergy()
        {
            if (_energy <= 0) _energy = 0;
            return _energy;
        }
        public EnemyOmega SetEnergy(int energy)
        {
            _energy = energy;
            return this;
        }
        public EnemyOmega AddEnergy(int point)
        {
            _energy += point;
            return this;
        }

    }
}
