﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;


namespace ArenaTotalWar
{
    public class Sword : Node
    {
        Node _owner;

        Sprite _spriteSword;
        bool _isAttackSword = false;

        public Sword(Node owner)
        {
            _owner = owner;

            _spriteSword = new Sprite();
            _spriteSword.Add(Game1._animation_sword2);

            SetSize(50, 40);
        }

        public bool IsAttack() { return _isAttackSword; }
        public void Attack()
        {
            _isAttackSword = true;
            _spriteSword.Start("Attack", 1, 0);
        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y;

            _spriteSword.Update();

            if (_spriteSword.OffPlay)
                _isAttackSword = false;

            return this;
        }

        public void Draw(SpriteBatch batch, float x, float y, Position showDirection)
        {
            if (_isAttackSword)
            {
                if (showDirection == Position.UP)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, -1.5708f, x, y);

                if (showDirection == Position.RIGHT)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 0,x, y);

                if (showDirection == Position.LEFT)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 0, x, y, SpriteEffects.FlipHorizontally);

                if (showDirection == Position.DOWN)
                    _spriteSword.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 1.5708f, x, y);
            }
        }

    }
}