﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace ArenaTotalWar
{
    public class HeroUX : Node
    {
        public enum Items
        {
            VOID,
            SELECT,
            GUN,
            BOMB,
            BLOCK,
            TOWER,
            AMMO,
            KATANA
        }

        public class Item
        {
            public Items _type = Items.VOID;
            public int _nb;
        }

        bool _showBuilder;
        bool _showSelecter;

        // Item Array Container 
        Item[,] _itemMap = new Item[3,3];

        // where to put the item
        public int _buildX = 0;
        public int _buildY = 0;

        // seleted item position 
        public int _selectX = 1;
        public int _selectY = 2;

        public float _sightX;
        public float _sightY;

        Hero _owner;

        float _tileW;
        float _tileH;

        RectangleF _rectBuilder; // Selected zone
        Rectangle _rectSelecter; // Buildable zone

        Addon.Loop _loop = new Addon.Loop();

        public bool _isBuildable = false; // Check if place is buildable : need to improve 

        public Point _focus = new Point();

        public HeroUX(Hero owner, float tileW, float tileH)
        {
            _owner = owner;
            _tileW = tileW;
            _tileH = tileH;

            SetZ(-10000);

            _loop.SetLoop(-1, -1, 1, .25f, Loops.PINGPONG);
            _loop.Start();

            // Create ItemMap
            for (int i=0; i<3; i++)
            {
                for (int j=0; j<3; j++)
                {
                    _itemMap[i, j] = new Item();
                }
            }


           SetItem(1, 1, new Item() { _nb = 10, _type = Items.SELECT });
           SetItem(0, 0, new Item() { _nb = 10, _type = Items.TOWER });
           SetItem(2, 0, new Item() { _nb = 10, _type = Items.AMMO });
           SetItem(1, 0, new Item() { _nb = 10, _type = Items.GUN });
           SetItem(1, 2, new Item() { _nb = 10, _type = Items.KATANA });
           SetItem(2, 1, new Item() { _nb = 10, _type = Items.BOMB });
           SetItem(2, 2, new Item() { _nb = 10, _type = Items.BLOCK });

        }
        public void SetItem(int selectX, int selectY, Item item)
        {
            if (selectX < 0 || selectX > 2) return;
            if (selectY < 0 || selectY > 2) return;

            _itemMap[selectX, selectY] = item;
        }
        public Item GetItem(int selectX, int selectY)
        {
            if (selectX < 0 || selectX > 2) return new Item();
            if (selectY < 0 || selectY > 2) return new Item();

            return _itemMap[selectX, selectY];
        }
        public Item GetCurrentItem()
        {
            return _itemMap[_selectX, _selectY];
        }
        public override Node Update(GameTime gameTime)
        {

            _loop.Update();

            _buildX = 0;
            _buildY = 0;

            if (_owner._showDirection == Position.UP) _buildY = -1;
            if (_owner._showDirection == Position.DOWN) _buildY = 1;
            if (_owner._showDirection == Position.LEFT) _buildX = -1;
            if (_owner._showDirection == Position.RIGHT) _buildX = 1;

            _focus.X = _owner._mapPosition.X + _buildX;
            _focus.Y = _owner._mapPosition.Y + _buildY;

            float mapFocusX = _focus.X * _tileW;
            float mapFocusY = _focus.Y * _tileH;

            // Check if place where focus is Buildable
            _isBuildable = false;
            Tile focusTile = _owner._map2D.Get(_focus.X, _focus.Y);
            if (null != focusTile)
            {
                //if (Game1.IsNotMapObject(focusTile._type))
                if (focusTile._type == Const.NoIndex)
                {
                    _isBuildable = true;
                }

                if (focusTile._subType == UID.Get<Portal>()) // If type is Portal then we can't build anything on the tile !
                    _isBuildable = false;

                //if (GetCurrentItem()._type == HeroUX.Items.TOWER)
                //{
                //    if (focusTile._subType == UID.Get<Tower.Case>())
                //        _isBuildable = true;
                //    else
                //        _isBuildable = false;
                //}

            }

            // Fire Gun
            if (_owner.ON_B_A && GetCurrentItem()._type == HeroUX.Items.GUN && PlayerATW.GetBankPoint() >= 2)
            {
                PlayerATW.AddBankPoint(-2);

                float vx = 0;
                float vy = 0;

                float speed = 8;

                if (_owner._showDirection == Position.UP) { vx = 0; vy = -speed;}
                if (_owner._showDirection == Position.DOWN) { vx = 0; vy = speed;}
                if (_owner._showDirection == Position.LEFT) { vx = -speed; vy = 0; }
                if (_owner._showDirection == Position.RIGHT) { vx = speed; vy = 0; }

                new Bullet(_sightX+5, _sightY+5, vx, vy, Bullet.BLUE)
                    .AppendTo(_parent);

                new Smoke(_sightX + 5, _sightY + 5, 8, false)
                    .AppendTo(_parent);

                Game1._sound_laserBlast.Play(.05f, 1f, 0);
            }

            // Attack Sword
            if (_owner.ON_B_A && GetCurrentItem()._type == HeroUX.Items.KATANA)
            {
                if (!_owner._spriteSword.IsPlay)
                {
                    _owner._isAttackSword = true;
                    _owner._spriteSword.Start("Attack", 1, 0);

                    Game1._sound_sword.Play(.2f, .1f, 0);

                    //System.Console.WriteLine("Attack Sword !");
                }
            }


            // Pose Bomb : 2 mode direct or directional
            if (GetCurrentItem()._type == Items.BOMB && _owner.ON_B_A)
            {
                //if (_owner.IS_B_A) // Command Pose bomb
                //{
                if (_owner._state == Hero.State.BUILD)
                    _owner.PoseBomb(_focus.X, _focus.Y);
                else
                    _owner.PoseBomb(_owner._mapPosition.X, _owner._mapPosition.Y);
                //}
            }
            else
                _owner._poseBomb = false;

            // When player press build button
            if (_owner._state == Hero.State.BUILD)
            {
                if (_owner._onChangeMapPosition &&
                   (GetCurrentItem()._type == Items.BLOCK ||
                    GetCurrentItem()._type == Items.SELECT ||
                    GetCurrentItem()._type == Items.TOWER ||
                    GetCurrentItem()._type == Items.AMMO ||
                    GetCurrentItem()._type == Items.BOMB))
                    Game1._sound_click.Play(.2f, .8f, 0);

                if (!_showBuilder) Game1._sound_click.Play(.2f, .5f, 0); // Play sound when begin build ! 

                _showBuilder = true;

                // Build Block !
                if (GetCurrentItem()._type == Items.BLOCK && _owner.ON_B_A && _isBuildable)
                {
                    if (PlayerATW.GetBankPoint() >= 40)
                    {
                        if (Block.Add(_owner._map2D, _parent, _focus.X, _focus.Y))
                        {
                            new PopInfo("-40", Color.Red)
                                .SetPosition(mapFocusX+_tileW/2, mapFocusY+_tileH/2 - 16)
                                .AppendTo(_parent);

                            PlayerATW.AddBankPoint(-40);

                            Game1._sound_killBlock.Play(.5f, .5f, 0);
                        }

                    }
                }

                // Build Tower !
                if (GetCurrentItem()._type == Items.TOWER && _owner.ON_B_A && _isBuildable)
                {
                    if (PlayerATW.GetBankPoint() >= 40)
                    {
                        if (Tower.Add(_owner._map2D, (Level)_parent, _focus.X, _focus.Y))
                        {
                            new PopInfo("-40", Color.Red)
                                .SetPosition(mapFocusX + _tileW / 2, mapFocusY + _tileH / 2 - 16)
                                .AppendTo(_parent);

                            PlayerATW.AddBankPoint(-40);

                            Game1._sound_electric2.Play(.4f, .05f, 0);
                        }

                    }
                }

                if (null != focusTile)
                    if (focusTile._type == UID.Get<Tower>() && GetCurrentItem()._type == HeroUX.Items.AMMO) // If type is Tower then we can reload ammo !
                    {
                        _isBuildable = true;

                        if (_owner.ON_B_A && PlayerATW.GetBankPoint() >= 20)
                        {
                            Tower tower = _parent.Index(focusTile._id).This<Tower>();

                            if (null != tower)
                            {
                                if (tower.AddAmmo(50))
                                {
                                    PlayerATW.AddBankPoint(-20);
                                    System.Console.WriteLine("Add Ammo !!");
                                }
                            }


                        }
                    }

            }
            else
            {
                _showBuilder = false;
            }

            // When player press select item button
            if (_owner._state == Hero.State.SELECT)
            {
                bool selectMove = false;

                _showSelecter = true;
                
                if (_owner.ON_B_UP && _selectY > 0)
                {
                    _selectY += -1;
                    selectMove = true;
                }

                if (_owner.ON_B_DOWN && _selectY < 2)
                {
                    _selectY += 1;
                    selectMove = true;
                }

                if (_owner.ON_B_LEFT && _selectX > 0)
                {
                    _selectX += -1;
                    selectMove = true;
                }

                if (_owner.ON_B_RIGHT && _selectX < 2)
                {
                    _selectX += 1;
                    selectMove = true;
                }


                if (selectMove)
                {
                    Game1._sound_click.Play(.5f, .5f, 0);
                }

            }
            else
            {
                _showSelecter = false;
            }

            _rectBuilder = new RectangleF(mapFocusX, mapFocusY, _tileW, _tileH);

            _rectSelecter = new Rectangle
            (
                (int)(- _tileW*2),
                (int)(- _tileH*2),
                (int)_tileW * 3,
                (int)_tileH * 3
            );

            // Set Sight Position
            _sightX = _owner._x - 5 + (_buildX * 24);
            _sightY = _owner._y - _owner._oY + (_buildY * 24);

            return this;
        }


        public override Node Render(SpriteBatch batch)
        {
            //if (InRectView())
            {
                Item item = GetCurrentItem();

                // Draw Builder
                if (_showBuilder && item._type != Items.GUN && item._type != Items.KATANA)
                {

                    // Draw Animated Arrow
                    float angle = 0;
                    if (_owner._showDirection == Position.UP) angle = 0;
                    if (_owner._showDirection == Position.DOWN) angle = 180;
                    if (_owner._showDirection == Position.LEFT) angle = 270;
                    if (_owner._showDirection == Position.RIGHT) angle = 90;
                    float rotation = MathHelper.ToRadians(angle);
                
                    batch.Draw(
                        Game1._tex_heroItems,
                        new Vector2(_sightX - _owner._x + _owner.AbsX+5, _sightY - _owner._y + _owner.AbsY + 6),
                        new Rectangle(16, 0, 13, 13), 
                        Color.White,
                        rotation + _loop._current/4,
                        new Vector2(6,6),
                        1,
                        SpriteEffects.None,
                        0);


                    // Draw Builder Cursor
                    RectangleF rect = Gfx.AddRect(Gfx.TranslateRect(_rectBuilder, AbsXY), -_loop._current, -_loop._current, _loop._current * 2, _loop._current * 2);

                    Draw.FillRectangle(batch, (Rectangle)rect, Color.BlueViolet * .2f);

                    if (_isBuildable)
                        batch.Draw(Game1._tex_sokoban, (Rectangle)rect, new Rectangle(0, 0, 32, 32), Color.LightGreen);
                    else
                        batch.Draw(Game1._tex_sokoban, (Rectangle)rect, new Rectangle(0, 0, 32, 32), Color.Red);

                }

                // Draw viewFinder !
                if (item._type == Items.GUN)
                {
                    batch.Draw(Game1._tex_heroItems, 
                        new Vector2(_sightX - _owner._x + _owner.AbsX, _sightY - _owner._y + _owner.AbsY), 
                        new Rectangle(0, 0, 11, 11), Color.White);
                }


                // Draw Score 
                //Draw.TopCenterBorderedString(batch,
                //    Game1._font_Main, PlayerATW.GetBankPoint().ToString(), _owner.AbsX(), _owner.AbsY() - 48, Color.Orange, Color.Black);

                // Draw Selecter
                if (_showSelecter)
                {
                    Draw.FillRectangle(batch, Gfx.TranslateRect(_rectSelecter,new Point( _owner.AbsX, _owner.AbsY)), Color.Blue * .4f);

                    for (int i=0; i<3; i++)
                    {
                        for (int j=0; j<3; j++)
                        {
                            RectangleF rectItem = new RectangleF(_owner.AbsX + _tileW * (i-2), _owner.AbsY + _tileH * (j-2), _tileW, _tileH);

                            Draw.Rectangle(batch, rectItem, Color.Black * .4f, 1);

                            if (GetItem(i, j)._type != Items.VOID)
                                Draw.CenterStringXY(
                                    batch,
                                    Game1._font_Mini, 
                                    GetItem(i,j)._type.ToString(), rectItem.X + _tileW / 2, rectItem.Y + _tileH / 2, Color.Red * .6f);
                        }
                    }

                    RectangleF rect = new RectangleF(_owner.AbsX + _tileW * (_selectX - 2) - _loop._current, _owner.AbsY + _tileH * (_selectY - 2) - _loop._current, _tileW + _loop._current * 2, _tileH + _loop._current * 2);

                    //batch.DrawRectangle(rect, Color.LightGreen, 1);

                    batch.Draw(Game1._tex_sokoban, (Rectangle)rect, new Rectangle(0, 192, 32, 32), Color.White);

                    Draw.CenterBorderedStringXY(batch,
                        Game1._font_Mini, item._type.ToString(), rect.X + _loop._current+ _tileW / 2, rect.Y + _loop._current+ _tileH / 2, Color.Yellow, Color.Black);



                }
            }

            return this;
        }
    }
}
