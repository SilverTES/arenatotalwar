﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Retro2D;
using System;
using System.Collections.Generic;
using static Retro2D.Node;

namespace ArenaTotalWar
{
    public class Data
    {
        public enum Type
        {
            NONE,
            ON_PRESS_BUTTON,
            SELECT_LEVEL,
            ADD_SCORE,
            GET_DAMAGE,
            ADD_ENERGY,
            ADD_POWER,
            ADD_MAX_ACTIVE_BOMB
        }

        public Type _type = Type.NONE;
        public string _msg;
        public int _value;

        public Data SetMsg(string msg)
        {
            _msg = msg;

            return this;
        }
    }


    public partial class Game1 : Game
    {

        public static RenderTarget2D fogTarget;

        public Game1()
        {
            _window.Setup(this, _mode, "Minimal", _screenW, _screenH, 2, 0, false, true, false);
            Content.RootDirectory = "Content";

        }
        protected override void Initialize()
        {

            base.Initialize();

            _window.Init(_font_Main);
            _window.SetScale(4);
            _batch = _window.Batch;

            fogTarget = new RenderTarget2D(GraphicsDevice, _screenW, _screenH);

            LoadSprite();

            CreatePlayer();

            _style_main = new Style
            {
                _font = _font_Main,
                _skinGui = _skinGui_03,
                _color = Style.ColorValue.MakeColor(Color.OrangeRed),
                _textBorder = true,
                _colorTextBorder = Style.ColorValue.MakeColor(Color.DarkRed),
                _focusBackgroundFillColor = Style.ColorValue.MakeColor(Color.Red * .4f),
                _focusBorderColor = Style.ColorValue.MakeColor(Color.Red),
                _border = new Style.Space(3)
            };

            //_nodeGui = new NodeGui()
            //{
            //    _style = _style_main,
            //    _window = _window,
            //    _showFocus = true,
            //    //_mouse = _mouse,
            //    _isLimitRect = false
            //};


            _root["Title"] = new Screen.Title()
                .SetSize(_screenW, _screenH)
                .Init()
                .AppendTo(_root);

            _root["Options"] = new Screen.Options(_pathGamePadSetup)
                .SetSize(_screenW, _screenH)
                .Init()
                .AppendTo(_root);

            _root["Level"] = new Screen.SelectLevel()
                .SetSize(_screenW, _screenH)
                .Init()
                .AppendTo(_root);

            _root["Play"] = new Screen.Play()
                .SetSize(_screenW, _screenH)
                .AppendTo(_root);

            Retro2D.Screen.Init(_naviState.CreateNaviState(_root["Title"])); // Screen where to Start 

            IsFixedTimeStep = false;

            //Addon.Type.ConsoleLogAll();

            //PopPlayers(_root["Play"].This<Screen.Play>().GetLayerMapObject());

            // Load GamePad Setup from file
            LoadGamePadSetupFromFile(_pathGamePadSetup);

            //Console.WriteLine(_controllers[PLAYER2].ToString());

            //CreateLevel();

            MediaPlayer.Play(Game1._song_intro); // start menu music !
            MediaPlayer.Volume = 0.2f;
            MediaPlayer.IsRepeating = true;

            

        }

        public static void SaveGamePadSetupToFile(string path)
        {
            List<Controller> controllers = new List<Controller>()
            {
                _controllers[PLAYER1],
                _controllers[PLAYER2],
                _controllers[PLAYER3],
                _controllers[PLAYER4]
            };

            FileIO.XmlSerialization.WriteToXmlFile(path, controllers);

            Console.WriteLine("GamePad File Setup Saved !");
        }

        public static void LoadGamePadSetupFromFile(string path)
        {
            List<Controller> controllers = FileIO.XmlSerialization.ReadFromXmlFile<List<Controller>>(path);

            _controllers[PLAYER1].Copy(controllers[PLAYER1]);
            _controllers[PLAYER2].Copy(controllers[PLAYER2]);
            _controllers[PLAYER3].Copy(controllers[PLAYER3]);
            _controllers[PLAYER4].Copy(controllers[PLAYER4]);

            Console.WriteLine("GamePad File Setup Loaded !");
        }


        protected override void Update(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();

            _window.GetMouse(ref _relMouseX, ref _relMouseY, ref _mouseState);

            _mouse.Update(_relMouseX, _relMouseY, (int)_mouseState.LeftButton);

            if (Input.Button.OnePress("Back", keyState.IsKeyDown(Keys.Back) || _isQuit))
            {
                _sound_click.Play(.5f, .05f, 0);
                if (Retro2D.Screen.CurScreen() == _root["Title"])
                    Exit();

                Retro2D.Screen.GoTo(_naviState.PopState(), new Transition.FadeInOut().Init());
            }

            // Debug RApid Quit
            if (keyState.IsKeyDown(Keys.Escape))
                Exit();

            if (Input.Button.OnePress("ScreenTitle", keyState.IsKeyDown(Keys.F1)))
                Retro2D.Screen.GoTo(_naviState.PushState(_root["Title"]), new Transition.FadeInOut().Init());

            if (Input.Button.OnePress("ScreenIntro", keyState.IsKeyDown(Keys.F2)))
                Retro2D.Screen.GoTo(_naviState.PushState(_root["Options"]), new Transition.FadeInOut().Init());

            if (Input.Button.OnePress("ScreenPlay", keyState.IsKeyDown(Keys.F3)))
                Retro2D.Screen.GoTo(_naviState.PushState(_root["Play"]), new Transition.FadeInOut().Init());

            //if (Input.Button.OnePress("Refresh", keyState.IsKeyDown(Keys.F5))) // Repop Players !
            //    PopPlayers(_root["Play"].This<Screen.Play>().GetLayerMapObject());

            // Change Skin
            if (keyState.IsKeyDown(Keys.LeftControl))
            {
                if (Input.Button.OnePress("skin00", keyState.IsKeyDown(Keys.D1))) _style_main._skinGui = _skinGui_00;
                if (Input.Button.OnePress("skin01", keyState.IsKeyDown(Keys.D2))) _style_main._skinGui = _skinGui_01;
                if (Input.Button.OnePress("skin02", keyState.IsKeyDown(Keys.D3))) _style_main._skinGui = _skinGui_02;
                if (Input.Button.OnePress("skin02", keyState.IsKeyDown(Keys.D4))) _style_main._skinGui = _skinGui_03;
            }

            if (Input.Button.OnePress("SwitchMode", keyState.IsKeyDown(Keys.F10)))
            {
                if (_mode == Mode.RETRO)
                    _mode = Mode.NORMAL;
                else
                    _mode = Mode.RETRO;

                _window.SetMode(_mode);
            }

            _window.UpdateStdWindowControl();

            _frameCounter.Update(gameTime);

            this.Window.Title = 
                "FPS :" + _frameCounter.Fps() + 
                " CurScreen() : "+ Retro2D.Screen.CurScreen() +
                " PrevScreen() : "+ Retro2D.Screen.PrevScreen();

            if (_shake.IsShake())
            {
                _window.SetOriginPosition((int)_shake.GetVector2().X, (int)_shake.GetVector2().Y);
            }


            Retro2D.Screen.Update(gameTime);

            _messageQueue.Dispatch();

            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {

            // Render 
            _window.SetRenderTarget(_window.NativeRenderTarget);

            _batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.LinearClamp);//, DepthStencilState.None, RasterizerState.CullCounterClockwise);

            Retro2D.Screen.Render(_batch);
            Retro2D.Draw.Sight(_batch, _relMouseX, _relMouseY, _screenW, _screenH, Color.MonoGameOrange * .2f, 1);
            _batch.Draw(Retro2D.Draw._mouseCursor, new Vector2(_relMouseX, _relMouseY), Color.Red);

            //if (!_root._naviState.StateEmpty())
            //int i = 0;
            //foreach (Node node in _naviState.GetStackNode())
            //{
            //    //(i == 0) ? "Current" : i.ToString()
            //    _batch.DrawString(_mainFont, i + " --- " + node._index, new Vector2(100, 10 + i * 10), Color.OrangeRed);
            //    i++;
            //}

            //int i = 0;
            //foreach (Keys key in Keyboard.GetState().GetPressedKeys())
            //{
            //    //(i == 0) ? "Current" : i.ToString()
            //    _batch.DrawString(_font_Main, i + " --- " + key + " : Value = " + (int)key, new Vector2(100, 10 + i * 10), Color.OrangeRed);
            //    i++;
            //}

            _batch.End();


            _window.SetRenderTarget(null);
            _batch.Begin(SpriteSortMode.Immediate, BlendState.NonPremultiplied, SamplerState.PointClamp);

            _window.Render(Color.White);

            _batch.End();

            base.Draw(gameTime);
        }
    }
}
