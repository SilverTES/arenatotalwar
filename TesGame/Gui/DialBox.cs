﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArenaTotalWar
{
    class DialBox : Gui.Base
    {
        bool _isShow = false;

        public DialBox(Input.Mouse mouse, int start = 0, int end = 480, int duration = 16) : base(mouse)
        {
            _naviGate = new NaviGate(this);

            Style._focusBorderColor._value = Color.Yellow;
            SetFocusable(false);

            _naviGate.SetNaviGate(false);
            _naviGate.SetNaviNodeFocusAt(0);

            _animate.Add("show", Easing.CircularEaseOut, new Tweening(end, start, duration));
            _animate.Add("hide", Easing.CircularEaseIn, new Tweening(start, end, duration));
            //_animate.Start("up");
            
        }

        public override Node Init()
        {
            return base.Init();
        }

        public void ToggleShow()
        {
            _isShow = !_isShow;

            if (_isShow)
                Show();
            else
                Hide();
        }

        public bool IsShow()
        {
            return _isShow;
        }

        public void Show()
        {
            _animate.Start("show");
            Game1._sound_key.Play(.5f, .5f, 0);
        }

        public void Hide()
        {
            _animate.Start("hide");
            Game1._sound_key.Play(.5f, .2f, 0);

        }

        public override Node Update(GameTime gameTime)
        {
            if (_animate.OnBegin("show"))
            {
                PostMessage("ON_SHOW");
            }
            if (_animate.OnBegin("hide"))
            {
                PostMessage("ON_HIDE");
                _naviGate.SetNaviGate(false); // Loose NaviGate 
            }

            if (_animate.OnEnd("show"))
            {
                PostMessage("OFF_SHOW");

                if (null != _parent) // if parent exist then move NaviGate from parent to this
                {
                    if (null != _parent._naviGate)
                        _parent._naviGate.MoveNaviGateTo(this, _parent);
                }
                else
                {
                    _naviGate.SetNaviGate(true);
                }

            }

            if (_animate.OnEnd("hide"))
            {
                PostMessage("OFF_HIDE");

                _naviGate.BackNaviGate();
                //Console.WriteLine("BackNaviGate :"+_navigate._prevNaviGateNode);
            }

            if (_animate.IsPlay())
                _y = _animate.Value();

            _animate.NextFrame();


            if (_naviGate.IsNaviGate)
            {
                Game1.UpdateNaviGateButton(_naviGate);

                if (Input.Button.OnPress("back"))
                {
                    //Console.WriteLine("dialbox back button pressed");
                    //_navigate.BackNaviGate();
                }

                if (_naviGate.OnSubmit())
                {
                    _naviGate.MoveNaviGateToFocusedChild();
                    Game1._sound_click.Play(.5f, .05f, 0);

                    //Console.WriteLine("Coucou");
                }

                if (_naviGate.OnChangeFocus && !Retro2D.Screen.IsTransition())
                {
                    Game1._sound_click.Play(.5f, .05f, 0);
                }
            }


            //UpdateChilds();

            if (HasMessage())
            {
                Data data = (Data)_message._data;

                if (data._msg == "OK")
                {
                    ToggleShow();
                }

                EndMessage();
            }

            return base.Update(gameTime);
        }

        //public override Node Render(SpriteBatch batch)
        //{
        //    base.Render(batch);

        //    RenderChilds(batch);

        //    return this;
        //}

    }
}
