﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System;

namespace ArenaTotalWar
{
    class MessageBox : Gui.Base
    {
        bool _isShow = false;
        //bool _isShowContent = false;
        RectangleF _rectOrigin = new RectangleF();
        float _xOrigin;
        float _yOrigin;

        public MessageBox(Input.Mouse mouse, int start = 0, int end = 10, int duration = 16) : base(mouse)
        {
            _naviGate = new NaviGate(this);
            
            SetFocusable(false);

            _naviGate.SetNaviGate(false);
            _naviGate.SetNaviNodeFocusAt(0);

            _animate.Add("show", Easing.CircularEaseOut, new Tweening(start, end, duration));
            _animate.Add("hide", Easing.CircularEaseIn, new Tweening(end, start, duration));
            //_animate.Start("up");

            //_rectOrigin = _rect;
            HideContent();

        }

        public override Node Init()
        {
            //GetStyle()._borderWidth = 2f;
            //GetStyle()._focusColor = Color.YellowGreen;

            float maxi = Math.Max(_rect.Width, _rect.Height) / 8;

            // ReSet tweening end by rect messagebox size !
            _animate.Of("show")._tweening._end = maxi;
            _animate.Of("hide")._tweening._start = maxi;

            SetPivot(Position.CENTER);

            _rectOrigin.Width = _rect.Width;
            _rectOrigin.Height = _rect.Height;

            //Console.WriteLine("_rectOrigin : {0} x {1}", _rectOrigin.Width, _rectOrigin.Height);

            _xOrigin = _x;
            _yOrigin = _y;

            _x = _xOrigin + _oX;
            _y = _yOrigin + _oY;

            //SetSize(0, 0);

            return base.Init();
        }

        public void ToggleShow()
        {
            _isShow = !_isShow;

            if (_isShow)
                Show();
            else
                Hide();
        }

        public bool IsShow()
        {
            return _isShow;
        }

        public void Show()
        {
            _animate.Start("show");
            Game1._sound_notification.Play(.5f, .5f, 0);
        }

        public void Hide()
        {
            _animate.Start("hide");
            //Game1._sound_bubble.Play(.5f, .2f, 0);

        }

        public override Node Update(GameTime gameTime)
        {
            int size = (int)_animate.Value();

            int w = size * 8;
            int h = size * 8;

            if (w >= _rectOrigin.Width) w = (int)_rectOrigin.Width;
            if (h >= _rectOrigin.Height) h = (int)_rectOrigin.Height;

            _rect.Width = w;
            _rect.Height = h;

            _x = _xOrigin + _rectOrigin.X - (w / 2);
            _y = _yOrigin + _rectOrigin.Y - (h / 2);


            SetAlpha(_animate.Value()/_animate.Of("show")._tweening._end);

            if (_animate.OnBegin("show"))
            {
                SetVisible(true);

                PostMessage("ON_SHOW");

            }
            if (_animate.OnBegin("hide"))
            {
                //_isShowContent = false;
                HideContent();

                PostMessage("ON_HIDE");
                _naviGate.SetNaviGate(false); // Loose NaviGate 
            }

            if (_animate.OnEnd("show"))
            {
                //_isShowContent = true;
                ShowContent();

                PostMessage("OFF_SHOW");

                if (null != _parent) // if parent exist then move NaviGate from parent to this
                {
                    if (null != _parent._naviGate)
                        _parent._naviGate.MoveNaviGateTo(this, _parent);
                }
                else
                {
                    _naviGate.SetNaviGate(true);
                }

            }

            if (_animate.OnEnd("hide"))
            {
                SetVisible(false);

                PostMessage("OFF_HIDE");

                _naviGate.BackNaviGate();
                //Console.WriteLine("BackNaviGate :"+_navigate._prevNaviGateNode);
            }

            //if (_animate.IsPlay())
            //    _y = 280 + _animate.Value();

            _animate.NextFrame();


            if (_naviGate.IsNaviGate)
            {
                Game1.UpdateNaviGateButton(_naviGate);

                if (Input.Button.OnPress("back"))
                {
                    //Console.WriteLine("dialbox back button pressed");
                    //_navigate.BackNaviGate();
                }

                if (_naviGate.OnSubmit())
                {
                    _naviGate.MoveNaviGateToFocusedChild();
                    Game1._sound_click.Play(.5f, .05f, 0);

                    //Console.WriteLine("Coucou");
                }

                if (_naviGate.OnChangeFocus && !Retro2D.Screen.IsTransition())
                {
                    Game1._sound_click.Play(.5f, .05f, 0);
                }
            }


            //UpdateChilds();

            if (HasMessage())
            {
                Data data = (Data)_message._data;

                if (data._msg == "OK")
                {
                    ToggleShow();
                }

                EndMessage();
            }

            return base.Update(gameTime);
        }

        //public override Node Render(SpriteBatch batch)
        //{

        //    base.Render(batch);

        //    //RenderChilds(batch);

        //    //StringBuilder debug = new StringBuilder().AppendFormat(" _x={0}, _y={1}, _rect.Width={2}, _rect.Height={3} ", _x, _y, _rect.Width, _rect.Height);
        //    //batch.DrawString(Game1._font_Main, debug.ToString(), new Vector2(10, 100), Color.MonoGameOrange);

        //    return this;
        //}

    }
}
