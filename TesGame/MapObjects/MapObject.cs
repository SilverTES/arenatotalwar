﻿using Microsoft.Xna.Framework;

using Retro2D;
using System;

namespace ArenaTotalWar
{
    public class MapObject : Node
    {
        // Use for pathFollow Target
        //public class Target
        //{
        //    public Point _mapPosition;
        //}

        #region Attributes

        //public Target _target = new Target();

        public float _strength = 0; // Strength of the object !

        // Map to collide 
        public Map2D<Tile> _map2D;

        public bool _onChangeMapPosition = false; // trigger : MapActor change map position !
        public Point _mapPosition = new Point();
        public Point _lastMapPosition = new Point();

        #endregion 

        protected MapObject(Map2D<Tile> map2D)
        {
            _map2D = map2D;
        }

        public static void Remove(Map2D<Tile> map2D, Node parent, int mapX, int mapY, bool killObject = false)
        {
            if (null != map2D.Get(mapX, mapY))
            {
                map2D.Get(mapX, mapY)._isCollidable = false;
                map2D.Get(mapX, mapY).InitPassLevel();
                map2D.Get(mapX, mapY)._type = Const.NoIndex;

                // If object exist then kill it !
                if (killObject)
                    if (null != parent.Index(map2D.Get(mapX, mapY)._id))
                    {
                        parent.Index(map2D.Get(mapX, mapY)._id).KillMe();
                        //Reset Index
                        map2D.Get(mapX, mapY)._id = Const.NoIndex;
                    }

            }
        }

        public override Node Update(GameTime gameTime)
        {
            // Set Actor map position
            if (!_lastMapPosition.Equals(_mapPosition))
            {
                _onChangeMapPosition = true;
                _lastMapPosition = _mapPosition;

                //Console.WriteLine(" --- ON Change Map Position");
            }
            else
                _onChangeMapPosition = false;

            _mapPosition.X = (int)(_x / _map2D._tileW);
            _mapPosition.Y = (int)(_y / _map2D._tileH);

            //_target._mapPosition = _mapPosition;

            return this;
        }

        public MapObject GoTo(int mapX, int mapY)
        {
            SetX(mapX * _map2D._tileW + _oX);
            SetY(mapY * _map2D._tileH + _oY);

            _mapPosition.X = (int)(_x / _map2D._tileW);
            _mapPosition.Y = (int)(_y / _map2D._tileH);

            return this;

        }
        //public Point GetMapPosition()
        //{
        //    return _mapPosition;
        //}

    }
}
