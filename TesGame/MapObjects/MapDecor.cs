﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace ArenaTotalWar
{
    class MapDecor : MapBlock
    {
        #region Attributes
        //public const int ZONE_BODY = 0;

        float _transparency = 1f;
        float _minAlpha = .2f;

        Shake _shake = new Shake();

        bool _isCut = false;
        bool _onCut = false;

        bool _isHit = false;

        int _lifeMax = 400;
        int _life;

        #endregion

        public MapDecor(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D, mapX, mapY)
        {
            _strength = 20;

            _life = _lifeMax;

            _type = UID.Get<MapDecor>();
            SetSize(32, 32);
            SetPivot(14, 28);

            //_z = -1000; // Over all Node Childs

            SetCollideZone(1, _rect); // For detect Hero behind go transparent !

            map2D.Get(mapX, mapY)._type = _type;
            map2D.Get(mapX, mapY)._isCollidable = true;
            map2D.Get(mapX, mapY).UpdatePassLevel(1);

        }
        public static new MapDecor Add(Map2D<Tile> map2D, Node parent, int mapX, int mapY) // Force Add 
        {
            Remove(map2D, parent, mapX, mapY, true);
            Remove(map2D, parent, mapX-1, mapY, true);
            Remove(map2D, parent, mapX+1, mapY, true);

            MapDecor mapDecor = new MapDecor(map2D, mapX, mapY);

            map2D.Get(mapX, mapY)._id = mapDecor.AppendTo(parent)._index;

            return mapDecor;
        }

        public MapDecor Shake(float intensity = 1, float step = .05f)
        {
            _shake.SetIntensity(intensity, step);
            return this;
        }
        public MapBlock HitDamage(int point)
        {
            if (!_isHit)
            {
                _isHit = true;
                AddLife(point/2);
                Game1._sound_hitIronWood.Play(.4f, 1f, 0);
            }
            return this;
        }

        public void Remove()
        {
            Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y, false);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int x = _mapPosition.X - 1 + i;
                    int y = _mapPosition.Y - 1 + j;

                    Game1.PopItem(_map2D, _parent, x, y);

                }
            }

            //Game1.PopItem(_map2D, _parent, _mapPosition.X - 1, _mapPosition.Y);
            //Game1.PopItem(_map2D, _parent, _mapPosition.X + 1, _mapPosition.Y);
            //Game1.PopItem(_map2D, _parent, _mapPosition.X, _mapPosition.Y - 1);
            //Game1.PopItem(_map2D, _parent, _mapPosition.X, _mapPosition.Y + 1);

            KillMe();

        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y - 64; // Explosion is over everything on the same line !

            UpdateRect();
            UpdateCollideZone(1, Gfx.AddRect(_rect, new Rectangle(-32, -80, 64, 48)));

            // Detect Hero behind !
            var listCollideZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(1), new int[] { UID.Get<Hero>(), UID.Get<Bomb>(), UID.Get<Tower>() }, 0);

            if (listCollideZone.Count > 0 && !_isCut)
            {
                _transparency -= 0.05f;
                if (_transparency <= _minAlpha) _transparency = _minAlpha;
            }
            else
            {
                _transparency += 0.05f;
                if (_transparency >= 1 ) _transparency = 1;
            }

            if (_life < _lifeMax / 2)
            {
                _isCut = true;
            }
            
            if (!_onCut && _isCut)
            {
                _onCut = true;

                new Smoke(_x, _y-64, 256, false, false, 64, 64)
                    .AppendTo(_parent);

                Game1._sound_hitWood.Play(.4f, .001f, 0);
                Game1._sound_killBlock.Play(.4f, .001f, 0);
            }

            if (_life <= 0)
            {
                new Smoke(_x, _y, 64, false, true, 24, 20)
                    .AppendTo(_parent);

                Game1._sound_hitWood.Play(.4f, .001f, 0);
                Game1._sound_killBlock.Play(.4f, .001f, 0);
                Remove();
            }

            if (_shake.IsShake()) _isHit = true; else _isHit = false;

            return base.Update(gameTime);
        }

        public MapDecor AddLife(int life)
        {
            _life += life;
            if (_life < 0) _life = 0;
            return this;
        }

        public override Node Render(SpriteBatch batch)
        {
            if (InRectView())
            {

                Rectangle rectShadow = new Rectangle((int)(AbsX - _oX) - 34, AbsY - 20, 100, 32 - 4);
                Rectangle rect = new Rectangle(0, 0, 128, 128);

                if (_isCut)
                {
                    rectShadow.X += 10;
                    rectShadow.Y += 2;
                    rectShadow.Width += -20;
                    rectShadow.Height += -4;

                    rect.X = 128; 
                }

                batch.Draw(Game1._tex_shadow, rectShadow , Color.Black * .2f); // Shadow
                batch.Draw(Game1._tex_tree, new Vector2(AbsX-56, AbsY-120) + _shake.GetVector2(), rect , Color.White * _transparency);

                //batch.DrawRectangle(Gfx.TranslateRect(GetCollideZone(ZONE_BODY)._rect, _parent.AbsVectorXY()), Color.Yellow, 1);

                //Draw.CenterBorderedStringXY(batch, Game1._font_Mini, _life.ToString(), AbsX(), AbsY() - 34, Color.LightGoldenrodYellow, Color.Black);

            }

            return this;
        }
    }
}
