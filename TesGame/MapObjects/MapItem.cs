﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    class MapItem : MapObject
    {
        #region Attributes

        //Addon.Draggable _drag;
        Collide.Zone _collideZone;

        bool _isHit = false;
        Node _collideNode = null;

        Sprite _sprite;

        #endregion 

        public MapItem(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D)
        {
            _type = UID.Get<MapItem>();

            SetSize(32, 32);
            SetPivot(16, 28);


            SetCollideZone(0,_rect);

            //_drag = CreateAddon<Addon.Draggable>();
            //_drag.SetDraggable(false);


            _sprite = new Sprite();

            _sprite.Add(Game1._animation_coin);
            _sprite.Start("Idle", 1, 0);

            _animate.Add("popup", Easing.BackEaseInOut, new Tweening(0, 24, 16));

            //_drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH));
            //_drag.SetLimitRect(true);

            map2D.Get(mapX, mapY)._type = _type;
            GoTo(mapX, mapY);

        }

        public void Remove()
        {
            //_map2D.Get(_mapPosition.X, _mapPosition.Y)._type = 0;
            //KillMe();
            Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y, true);
        }

        public override Node Init()
        {

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            

            _z = -(int)_y;

            //_drag.Update(Game1._mouse);
            _sprite.Update();

            UpdateRect();
            UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(12, 12, -24, -24)));

            _collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), new int[]{ UID.Get<MapActor>(), UID.Get<Hero>()}, 0);

            if (null != _collideZone && !_isHit)
            {
                _isHit = true;
                _collideNode = _collideZone._node;

                new PopInfo("+10", Color.Yellow)
                    .SetPosition(_x, _y - 32)
                    .AppendTo(_parent);

                Game1._sound_bubble.Play(0.2f, 1.0f, 0);

                Data msg = new Data()
                {
                    _type = Data.Type.ADD_SCORE,
                    _msg = "COUCOU TOUT LE MONDE !",
                    _value = 10
                };

                Game1._messageQueue.Post((int)msg._type, msg, _collideNode);

                _animate.Start("popup");
            }

            if (_isHit)
            {
                //_rect = Gfx.AddRect(_rect, new RectangleF(1, 1, -2, -2));
                //_oX--;
                //_oY--;

                if (_animate.OnEnd("popup"))
                {
                    Remove();
                }
            }

            _animate.NextFrame();

            base.Update(gameTime);

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            batch.Draw(Game1._tex_shadow, new Rectangle((int)(AbsX - _oX) + 8, AbsY - 6, 32 - 16, 16 - 12), Color.Black * .2f); // Shadow

            //batch.Draw(Game1._sokoban, AbsRect(), new Rectangle(256, 192, 64, 64), Color.White);

            _sprite.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 0, 0, -4 - _animate.Value()); // Sprite



            return base.Render(batch);
        }
    }
}