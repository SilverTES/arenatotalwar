﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    class Block : MapObject
    {
        #region Attributes

        public const int ZONE_BODY = 1;

        const float MAX_ENERGY = 200;
        float _energy = MAX_ENERGY;

        Shake _shake;

        #endregion 

        public Block(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D)
        {
            _strength = 10;

            _type = UID.Get<Block>();

            _shake = new Shake();

            SetSize(Level.TileSizeW, Level.TileSizeH);
            SetPivot(Level.TileSizeW/2, Level.TileSizeH-4);
            SetCollideZone(0, _rect);
            SetCollideZone(1, _rect);

            map2D.Get(mapX, mapY)._type = _type;
            map2D.Get(mapX, mapY)._isCollidable = false;
            map2D.Get(mapX, mapY).UpdatePassLevel(2);

            GoTo(mapX, mapY);
        }

        public static bool Add(Map2D<Tile> map2D, Node parent, int mapX, int mapY)
        {
            if (null != map2D.Get(mapX, mapY))
            {

                if (Game1.IsNotMapObject(map2D.Get(mapX, mapY)._type))
                {
                    map2D.Get(mapX, mapY)._id = new Block(map2D, mapX, mapY).AppendTo(parent)._index;

                    return true;
                }
            }
            return false;
        }
        public void Remove()
        {
            new PopInfo("Block", Color.GreenYellow)
                .SetPosition(_x, _y - 32)
                .AppendTo(_parent);

            Game1._sound_killBlock.Play(0.2f, 0.4f, 0);

            Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y);

            Game1.PopItem(_map2D, _parent, _mapPosition.X, _mapPosition.Y);

            KillMe();


        }

        public Block Shake(float intensity = 1, float step = .05f)
        {
            _shake.SetIntensity(intensity, step);
            return this;
        }
        

        public override Node Init()
        {

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y;

            UpdateRect();
            //UpdateCollideZone(0, _rect);
            UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(-2, -2, 4, 4)));
            UpdateCollideZone(ZONE_BODY, _rect);

            //var listCollideZone = Collision2D.ListCollideZoneByNodeType
            //(
            //    GetCollideZone(ZONE_BODY),
            //    UID.Get<Hero>(),
            //    1 // Sword Hero
            //);

            //if (listCollideZone.Count > 0)
            //{
            //    for (int i = 0; i < listCollideZone.Count; ++i)
            //    {
            //        Hero hero = listCollideZone[i]._node.This<Hero>();
                    
            //        if (hero._isAttackSwordOn)
            //        {
            //            AddEnergy(-80);
            //            Shake(3, .1f);
            //        }

            //    }
            //}

            if (_energy <= 0)
            {
                new Smoke(_x, _y-16, 64, true, true, 16, 16)
                    .AppendTo(_parent);

                Remove();
            }

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            if (InRectView())
            {
                int oY = 0;

                float damage = _energy / MAX_ENERGY;

                if (damage < .90f) oY = 64;
                if (damage < .60f) oY = 128;
                if (damage < .30f) oY = 192;

                batch.Draw(Game1._tex_sokoban, (Rectangle)Gfx.TranslateRect(AbsRect, _shake.GetVector2()), new Rectangle(512, 0 + oY, 64, 64), Color.White);

                //batch.DrawRectangle(AbsRect(), Color.YellowGreen *.4f, 2);

                //var colliderZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Hero>(), 0);
                //if (colliderZone != null)
                //    batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua * 0.2f, 1);

                //batch.DrawString(Game1._font_Main, ":" + damage, new Vector2(AbsX() - 16, AbsY() - 16), Color.Yellow);
                //batch.DrawString(Game1._font_Main, _x + "," + _y, new Vector2(AbsX() - 16, AbsY() - 16), Color.Yellow);

                //Draw.CenterBorderedStringXY(batch, Game1._font_Mini, _energy.ToString(), AbsX(), AbsY(), Color.LightGoldenrodYellow, Color.Black);

            }


            return this;
        }

        public void AddEnergy(float energy)
        {
            _energy += energy;

            if (_energy <= 0) _energy = 0;
        }
    }
}
