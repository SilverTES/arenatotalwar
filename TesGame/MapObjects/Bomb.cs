﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;
using System.Text;

namespace ArenaTotalWar
{
    class Bomb : MapBlock
    {
        #region Attributes

        Hero _owner; 

        //Addon.Draggable _drag;

        int _tick = 0;
        int _bombTimer = 80;
        int _wavePower;
        int _waveTimer;

        StringBuilder _sbTick = new StringBuilder();

        Node _collideNode = null; // Use  When Hero Push Bomb !
        Collide.Zone _collideZone = null;

        bool _isMove = false;
        bool _isPush = false;

        Position _direction = Position.M;
        float _speed = 2.4f;
        float _vx = 0;
        float _vy = 0;

        // Manage Bomb Move Collision 
        bool _isCollideMove = false; // Available position check if Bomb reach collideMove Position X & Y !
        float _collideMoveX = 0; // Position of the Bomb when collide someting collidable
        float _collideMoveY = 0;

        int _tickStep = 0;
        int _timerStep = 8;

        #endregion 

        public Bomb(Map2D<Tile> map2D, int mapX, int mapY, int bombTimer = 80, int wavePower = 1, int waveTimer = 1, Hero owner = null) : base(map2D, mapX, mapY)
        {
            _type = UID.Get<Bomb>();

            _tick = _bombTimer = bombTimer;
            _wavePower = wavePower;
            _waveTimer = waveTimer;

            _owner = owner;

            SetSize(32, 32);
            SetPivot(16, 28);
            SetCollideZone(0, _rect);
            SetCollideZone(1, _rect);

            //_drag = CreateAddon<Addon.Draggable>();
            //_drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH));
            //_drag.SetLimitRect(true);
            //_drag.SetDraggable(false);

            map2D.Get(mapX, mapY)._type = _type;
            map2D.Get(mapX, mapY)._isCollidable = true;
            map2D.Get(mapX, mapY).UpdatePassLevel(1); 
            GoTo(mapX, mapY);

            _owner.SetNbActiveBomb(1);
        }

        public static bool Add(Map2D<Tile> map2D, Node parent, int mapX, int mapY, int bombTimer = 80, int wavePower = 1, int waveTimer = 1, Hero owner = null)
        {
            if (null != map2D.Get(mapX, mapY))
            {
                if (Game1.IsNotMapObject(map2D.Get(mapX, mapY)._type))
                {
                    Bomb bomb = new Bomb(map2D, mapX, mapY, bombTimer, wavePower, waveTimer, owner);

                    map2D.Get(mapX, mapY)._id = bomb.AppendTo(parent)._index;

                    //bomb.UpdateRect();

                    return true;
                }
            }
            return false;
        }

        public void Explose()
        {
            Game1._sound_explosion.Play(0.1f, 0.2f, 0);

            if (!_isMove) // Remove from map2D only when Bomb don't Move
                Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y);

            _owner.SetNbActiveBomb(-1);

            // Create Explosion wave
            new ExplosionWave(_map2D, Position.RIGHT, _wavePower, _waveTimer).AppendTo(_parent)
                .This<ExplosionWave>().GoTo(_mapPosition.X, _mapPosition.Y);

            new ExplosionWave(_map2D, Position.LEFT, _wavePower, _waveTimer).AppendTo(_parent)
                .This<ExplosionWave>().GoTo(_mapPosition.X, _mapPosition.Y);

            new ExplosionWave(_map2D, Position.UP, _wavePower, _waveTimer).AppendTo(_parent)
                .This<ExplosionWave>().GoTo(_mapPosition.X, _mapPosition.Y);

            new ExplosionWave(_map2D, Position.DOWN, _wavePower, _waveTimer).AppendTo(_parent)
                .This<ExplosionWave>().GoTo(_mapPosition.X, _mapPosition.Y);

            //if (null != _map2D.Get(_mapPosition.X, _mapPosition.Y))
            //{
            //    _map2D.Get(_mapPosition.X, _mapPosition.Y)._type = 0;
            //    _map2D.Get(_mapPosition.X, _mapPosition.Y)._isCollidable = false;
            //    _map2D.Get(_mapPosition.X, _mapPosition.Y)._isPassable = true;
            //}

            KillMe();
        }
        void ReEnterInMap()
        {
            _x = _collideMoveX;
            _y = _collideMoveY;

            _isPush = false;
            _isMove = false;
            _direction = Position.M;
            _isCollideMove = false;

            _map2D.Get(_mapPosition.X, _mapPosition.Y)._type = _type;
            _map2D.Get(_mapPosition.X, _mapPosition.Y)._isCollidable = true;
            _map2D.Get(_mapPosition.X, _mapPosition.Y).UpdatePassLevel(1);
        }

        public override Node Init()
        {
            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y;
            //_drag.Update(Game1._mouse);

            UpdateRect();
            //UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(-5, -5, 10, 10)));
            UpdateCollideZone(0, _rect);

            UpdateCollideZone(1, _rect);

            _collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), new int[] { UID.Get<MapActor>(), UID.Get<Hero>(), UID.Get<Enemy>() }, 0);

            if (null != _collideZone)
            {
                _collideNode = _collideZone._node;

                if (_collideNode._type == UID.Get<Hero>() && !_isPush) // Pushable Bomb
                {
                    RectangleF rect = GetCollideZone(0)._rect;
                    // Check where is the hero 
                    if (_collideNode._x <= rect.X && _collideNode.This<Hero>().IS_B_RIGHT && !_map2D.Get(_mapPosition.X + 1, _mapPosition.Y)._isCollidable) // he is at Left
                    {
                        _isPush = true;
                        _isMove = true;
                        _direction = Position.RIGHT;
                        Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y);
                    }
                    if (_collideNode._x >= rect.X + rect.Width && _collideNode.This<Hero>().IS_B_LEFT && !_map2D.Get(_mapPosition.X - 1, _mapPosition.Y)._isCollidable) // he is at Right
                    {
                        _isPush = true;
                        _isMove = true;
                        _direction = Position.LEFT;
                        Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y);
                    }
                    if (_collideNode._y <= rect.Y && _collideNode.This<Hero>().IS_B_DOWN && !_map2D.Get(_mapPosition.X, _mapPosition.Y + 1)._isCollidable) // he is at Top
                    {
                        _isPush = true;
                        _isMove = true;
                        _direction = Position.DOWN;
                        Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y);
                    }
                    if (_collideNode._y >= rect.Y + rect.Height && _collideNode.This<Hero>().IS_B_UP && !_map2D.Get(_mapPosition.X, _mapPosition.Y - 1)._isCollidable) // he is at Bottom
                    {
                        _isPush = true;
                        _isMove = true;
                        _direction = Position.UP;
                        Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y);
                    }

                    if (_isPush)
                        Game1._sound_bubble.Play(0.2f, 0.4f, 0);
                }

                if (_collideNode._type == UID.Get<Enemy>() && _isMove) // Bomb Hit Enemy then Explose
                {
                    Explose();
                }


            }

            // Move when pushed !
            if (_isMove)
            {
                _tickStep++;
                if (_tickStep > _timerStep)
                {
                    _tickStep = 0;
                    Game1._sound_footStep.Play(0.4f, 0.01f, 0);

                    new Smoke(_x, _y-8, 16)
                        .AppendTo(_parent);
                }


                if (_direction == Position.UP) { _vx = 0; _vy = -1; }
                if (_direction == Position.DOWN) { _vx = 0; _vy = 1; }
                if (_direction == Position.LEFT) { _vx = -1; _vy = 0; }
                if (_direction == Position.RIGHT) { _vx = 1; _vy = 0; }

                _x += _vx * _speed;
                _y += _vy * _speed;

                if (_isCollideMove)
                {

                    if (_direction == Position.UP && _y <= _collideMoveY) ReEnterInMap();
                    if (_direction == Position.DOWN && _y >= _collideMoveY) ReEnterInMap();
                    if (_direction == Position.LEFT && _x <= _collideMoveX) ReEnterInMap();
                    if (_direction == Position.RIGHT && _x >= _collideMoveX) ReEnterInMap();

                }

                // Next bomb position move
                int nextX = _mapPosition.X + (int)_vx;
                int nextY = _mapPosition.Y + (int)_vy;

                // Limit reach Map 
                if (nextX <0 || nextX > _map2D._mapW - 1 ||
                    nextY <0 || nextY > _map2D._mapH - 1)
                {
                    _isCollideMove = true;

                    _collideMoveX = _mapPosition.X * _map2D._tileW + _oX;
                    _collideMoveY = _mapPosition.Y * _map2D._tileH + _oY;

                    //System.Console.Write("< Collide Limit Map >");
                }

                if (null != _map2D.Get(nextX, nextY)) // If object is not passable behind the bomb  then can't move bomb
                    if (_map2D.Get(nextX, nextY)._passLevel > 0)
                    {
                        if (!_isCollideMove)
                        {
                            _isCollideMove = true;

                            _collideMoveX = _mapPosition.X*_map2D._tileW+_oX; 
                            _collideMoveY = _mapPosition.Y*_map2D._tileH+_oY; 
                        }

                    }

                //if (_map2D.Get(_mapPosition.X, _mapPosition.Y)._isCollidable) // if reach an obstacle then explose !
                //    Explose();
            }



            _tick--;
            if (_tick <= 0)
            {
                Explose();
            }

            _sbTick.Clear().Append(_tick.ToString());

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            batch.Draw(Game1._tex_sokoban, AbsRect, new Rectangle(128, 192, 64, 64), Color.White);

            //batch.DrawRectangle(AbsRect(), Color.YellowGreen *.4f, 2);

            //var colliderZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Hero>(), 0);
            //if (colliderZone != null)
            //    batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua * 0.2f, 1);

            Draw.TopCenterString(batch, Game1._font_Main, _sbTick.ToString(), AbsX, AbsY - 16, Color.Yellow);

            return this;
        }
    }
}
