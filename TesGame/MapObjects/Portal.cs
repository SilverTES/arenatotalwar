﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;
using System.Collections.Generic;

namespace ArenaTotalWar
{
    class Portal : MapObject
    {
        #region Attributes

        public const int ZONE_BODY = 0;
        public const int ZONE_AGRO = 1;

        int _energy = 20;
        int _maxEnergy = 20;
        BarInfo _barInfo;

        List<Collide.Zone> _listCollideZone;

        #endregion 

        public bool IsDead()
        {
            return _energy == 0; 
        }

        public Portal(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D)
        {
            _type = UID.Get<Portal>();

            SetSize(32, 32);
            SetPivot(16, 28);
            SetCollideZone(ZONE_BODY, _rect);
            SetCollideZone(ZONE_AGRO, _rect);

            map2D.Get(mapX, mapY)._type = _type;
            map2D.Get(mapX, mapY)._isCollidable = false;
            map2D.Get(mapX, mapY).UpdatePassLevel(0); // need Strong passLevel for pathfinding !

            GoTo(mapX, mapY);

            _barInfo = new BarInfo(this, Color.Orange, Color.Red, Color.Black * .2f, _maxEnergy, 48, 4, 1);
            _barInfo.SetY(-44);
            _barInfo.SetPlayShow(true, true);
            _barInfo.AppendTo(this);
            //_barInfo.AddWarning(new BarInfo.Warning(_maxEnergy/6, _maxEnergy/3, 80));
            //_barInfo.AddWarning(new BarInfo.Warning(0,_maxEnergy / 6, 40));


        }

        public static Portal Add(Map2D<Tile> map2D, Level level, int mapX, int mapY)
        {
            if (null != map2D.Get(mapX, mapY))
            {
                //if (Game1.IsNotMapObject(map2D.Get(mapX, mapY)._type))
                {

                    for (int i = 0; i < 9; i++)
                    {
                        for (int j = 0; j < 9; j++)
                        {
                            int x = mapX - 4 + i;
                            int y = mapY - 4 + j;

                            level.AddPathCase(x, y);

                        }
                    }

                    Portal portal = new Portal(map2D, mapX, mapY);
                    portal.AppendTo(level);
                    map2D.Get(mapX, mapY)._id = portal._index;

                    MapBlock.Add(map2D, level, mapX, mapY - 1)?.SetVisible(false);
                    MapBlock.Add(map2D, level, mapX-1, mapY - 1)?.SetVisible(false);
                    MapBlock.Add(map2D, level, mapX+1, mapY - 1)?.SetVisible(false);
                    MapBlock.Add(map2D, level, mapX - 1, mapY)?.SetVisible(false);
                    MapBlock.Add(map2D, level, mapX + 1, mapY)?.SetVisible(false);

                    return portal;
                }
            }
            return null;
        }

        public Portal Warning()
        {
            _barInfo.SetWarning(true, 80, true, true);

            return this;
        }

        public float GetAlphaWarning() { return _barInfo.GetAlphaWarning(); }

        public override Node Init()
        {

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y - 2;

            UpdateRect();
            UpdateCollideZone(ZONE_BODY, Gfx.AddRect(_rect, new Rectangle(5, -5, -10, -15)));
            UpdateCollideZone(ZONE_AGRO, Gfx.AddRect(_rect, new Rectangle(-128, -128, 256, 256)));

            _listCollideZone = Collision2D.ListCollideZoneByNodeType
            (
                GetCollideZone(ZONE_BODY),
                new int[]
                {
                    UID.Get<Enemy>(),
                    UID.Get<EnemyOmega>(),
                    UID.Get<Hero>()
                },
                0
            );

            if (_listCollideZone.Count > 0) // Hit only one time !
            {

                for (int i = 0; i < _listCollideZone.Count; ++i)
                {
                    Node collider = _listCollideZone[i]._node;

                    //if (collider._type == UID.Get<Hero>())
                    //{
                    //    collider.This<Hero>().AddEnergy(-10);
                    //    new PopInfo("-10", Color.Red)
                    //        .SetPosition(_x, _y - 32)
                    //        .AppendTo(_parent);

                    //    KillMe();

                    //}

                    if (collider._type == UID.Get<Enemy>())
                    {
                        //collider.This<Enemy>().AddEnergy(-40);
                        //collider.This<Enemy>().HitMe();

                        new PopInfo("-1", Color.Red)
                            .SetPosition(_x, _y - 32)
                            .AppendTo(_parent);

                        collider.This<Enemy>().KillMe();

                        _energy += -1;

                        if (_energy <= 0)
                            _energy = 0;
                        else
                            Game1.Shake(16, .2f);

                    }
                    if (collider._type == UID.Get<EnemyOmega>())
                    {
                        //collider.This<Enemy>().AddEnergy(-40);
                        //collider.This<Enemy>().HitMe();

                        new PopInfo("-1", Color.Red)
                            .SetPosition(_x, _y - 32)
                            .AppendTo(_parent);

                        collider.This<EnemyOmega>().KillMe();

                        _energy += -1;

                        if (_energy <= 0)
                            _energy = 0;
                        else
                            Game1.Shake(16, .2f);

                    }


                    // if hero touch Portal then get energy recovery
                    if (collider._type == UID.Get<Hero>())
                    {
                        Hero hero = collider.This<Hero>();

                        ++hero._tickTurbo;
                        if (hero._tickTurbo > hero._timerTurbo)
                        {
                            hero._tickTurbo = 0;

                            if (hero.GetEnergy() < hero.GetMaxEnergy())
                            {
                                hero.AddEnergy(5);
                                new PopInfo("+5", Color.GreenYellow)
                                    .SetPosition(hero._x, hero._y - 32)
                                    .AppendTo(_parent);

                                Game1._sound_bubble.Play(.2f, .01f, 0);
                            }

                        }
                    }

                }

            }



            _listCollideZone = Collision2D.ListCollideZoneByNodeType
            (
                GetCollideZone(ZONE_AGRO),
                new int[]
                {
                    UID.Get<Enemy>(),
                    UID.Get<Hero>()
                },
                0
            );

            _barInfo.SetValue(_energy);

            UpdateChilds(gameTime);

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            if (InRectView())
            {
                //batch.DrawCircle(AbsVectorXY(), 80, 32, Color.LawnGreen, 1);

                //batch.Draw(Game1._tex_sokoban, Gfx.AddRect(AbsRect(),0,-12,0,12) , new Rectangle(640, 128-24, 64, 64+24), Color.White);
                batch.Draw(Game1._tex_itemAtlas, new Vector2(AbsX-48, AbsY-80), new Rectangle(320, 64, 96, 96), Color.White);

                if (_barInfo.IsWarning())
                {
                    batch.Draw(Game1._tex_itemAtlas, new Vector2(AbsX-48, AbsY-80), new Rectangle(416, 64, 96, 96), Color.White * _barInfo.GetAlphaWarning());

                    Draw.Rectangle(batch, new RectangleF(0, 0, Game1._screenW, Game1._screenH), Color.Red * _barInfo.GetAlphaWarning(), 2f);
                }

                //batch.DrawRectangle(AbsRect(), Color.YellowGreen *.4f, 2);

                //var colliderZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Hero>(), 0);
                //if (colliderZone != null)
                //batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua, 1);
                //batch.DrawRectangle(Gfx.TranslateRect(GetCollideZone(ZONE_AGRO)._rect, _parent.AbsVectorXY()), Color.Yellow, 1);


                // Draw object detected
                //if (_listCollideZone.Count > 0) // Hit only one time !
                //{
                //    for (int i = 0; i < _listCollideZone.Count; ++i)
                //    {
                //        Node collider = _listCollideZone[i]._node;
                    
                //        if (collider._type == UID.Get<Enemy>())
                //        {
                //            Enemy enemy = collider.This<Enemy>();

                //        }

                //        // if hero touch Portal then get energy recovery
                //        if (collider._type == UID.Get<Hero>())
                //        {
                //            Hero hero = collider.This<Hero>();

                //        }

                //    }

                //}


                Draw.TopCenterBorderedString(batch, Game1._font_Main, _energy.ToString(), AbsX, AbsY - 86, Color.MonoGameOrange, Color.Black);

                RenderChilds(batch);

            }


            return base.Render(batch);
        }
    }
}
