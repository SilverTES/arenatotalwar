﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    class MapBlock : MapObject
    {
        #region Attributes

        public const int ZONE_BODY = 2;

        #endregion 

        public MapBlock(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D)
        {
            _strength = 20;

            _type = UID.Get<MapBlock>();

            SetSize(Level.TileSizeW, Level.TileSizeH);
            SetPivot(Level.TileSizeW / 2, Level.TileSizeH - 4);
            SetCollideZone(0,_rect);
            SetCollideZone(ZONE_BODY, _rect);

            map2D.Get(mapX, mapY)._type = _type;
            map2D.Get(mapX, mapY)._isCollidable = false;
            map2D.Get(mapX, mapY).UpdatePassLevel(2); // need Strong passLevel for pathfinding !

            GoTo(mapX, mapY);
        }

        public static MapBlock Add(Map2D<Tile> map2D, Node parent, int mapX, int mapY)
        {
            if (null != map2D.Get(mapX, mapY))
            {
                if (Game1.IsNotMapObject(map2D.Get(mapX, mapY)._type))
                {
                    MapBlock mapBlock = new MapBlock(map2D, mapX, mapY);
                    map2D.Get(mapX, mapY)._id = mapBlock.AppendTo(parent)._index;

                    return mapBlock;
                }
            }
            return null;
        }
        public override Node Init()
        {

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y;

            UpdateRect();
            //UpdateCollideZone(0, _rect);
            UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(-2, -2, 4, 4)));
            UpdateCollideZone(ZONE_BODY, _rect);

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            if (InRectView())
            {
                //batch.Draw(Game1._tex_sokoban, Gfx.AddRect(AbsRect(),0,-12,0,12) , new Rectangle(640, 128-24, 64, 64+24), Color.White);
                batch.Draw(Game1._tex_sokoban, AbsRect , new Rectangle(640, 128, 64, 64), Color.White);

                //batch.DrawRectangle(AbsRect(), Color.YellowGreen *.4f, 2);

                //var colliderZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Hero>(), 0);
                //if (colliderZone != null)
                //    batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua * 0.2f, 1);

                //batch.DrawString(Game1._mainFont, ":" + _index, new Vector2(AbsX() - 16, AbsY() - 16), Color.Yellow);

            }

            return this;
        }
    }
}
