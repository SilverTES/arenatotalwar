﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    class ItemCoin : MapObject
    {
        #region Attributes

        //Addon.Draggable _drag;
        Collide.Zone _collideZone;

        bool _isHit = false;
        Node _collideNode = null;

        Sprite _sprite;

        #endregion 

        public ItemCoin(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D)
        {
            _type = UID.Get<ItemCoin>();

            SetSize(32, 32);
            SetPivot(16, 28);


            SetCollideZone(0, _rect);

            //_drag = CreateAddon<Addon.Draggable>();
            //_drag.SetDraggable(false);


            _sprite = new Sprite();

            _sprite.Add(Game1._animation_coin2);
            _sprite.Start("Idle", 1, Misc.Rng.Next(0,4));

            _animate.Add("popup", Easing.BackEaseInOut, new Tweening(0, 24, 16));

            //_drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH));
            //_drag.SetLimitRect(true);

            map2D.Get(mapX, mapY)._type = _type;
            GoTo(mapX, mapY);

        }

        public static bool Add(Map2D<Tile> map2D, Node parent, int mapX, int mapY)
        {
            if (null != map2D.Get(mapX, mapY))
            {
                if (Game1.IsNotMapObject(map2D.Get(mapX, mapY)._type))
                {
                    map2D.Get(mapX, mapY)._id = new ItemCoin(map2D, mapX, mapY).AppendTo(parent)._index;

                    return true;
                }
            }
            return false;
        }
        public void Remove()
        {
            //_map2D.Get(_mapPosition.X, _mapPosition.Y)._type = Const.NoIndex;
            //KillMe();
            Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y, false);
            KillMe();
        }

        public override Node Init()
        {

            return base.Init();
        }

        public void GetMe(Node collider)
        {
            _isHit = true;

            new PopInfo("+10", Color.Yellow)
                .SetPosition(_x, _y - 32)
                .AppendTo(_parent);

            Game1._sound_bubble.Play(0.2f, 1.0f, 0);

            Data msg = new Data()
            {
                _type = Data.Type.ADD_SCORE,
                _msg = "COUCOU TOUT LE MONDE !",
                _value = 10
            };

            Game1._messageQueue.Post((int)msg._type, msg, collider);

            _animate.Start("popup");
        }

        public override Node Update(GameTime gameTime)
        {


            _z = -(int)_y;

            //_drag.Update(Game1._mouse);
            _sprite.Update();

            UpdateRect();
            UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(10, 10, -20, -20)));

            _collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), new int[] { UID.Get<MapActor>(), UID.Get<Hero>() }, 0);

            if (null != _collideZone && !_isHit)
            {
                GetMe(_collideZone._node);
            }

            if (_isHit)
            {
                if (_animate.OnEnd("popup"))
                {
                    Remove();
                }
            }

            _animate.NextFrame();

            base.Update(gameTime);

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {

            batch.Draw(Game1._tex_shadow, new Rectangle((int)(AbsX - _oX) + 8, AbsY - 6, 32 - 16, 16 - 12), Color.Black * .2f); // Shadow

            //batch.Draw(Game1._sokoban, AbsRect(), new Rectangle(256, 192, 64, 64), Color.White);

            _sprite.Draw(batch, AbsX, AbsY, Color.White, 1, 1, 0, 0, - 4 -_animate.Value()); // Sprite

            //batch.DrawCircle(AbsX(), AbsY() - _animate.Value() - 12, 8, 16, Color.Red, 4);

            //batch.DrawRectangle(AbsRect(), Color.YellowGreen * .4f, 1);

            //_collideZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Hero>(), 0);
            //if (_collideZone != null)
            //batch.DrawRectangle(GetCollideZone(0)._rect, Color.Red * 0.8f, 1);

            //batch.DrawString(Game1._mainFont, ":" + _animate.Value(), new Vector2(AbsX() - 16, AbsY() - 16), Color.Yellow);

            //int tileW = _map2D._tileW;
            //int tileH = _map2D._tileH;

            //int X = _mapPosition.X;
            //int Y = _mapPosition.Y;
            //batch.DrawRectangle(new Rectangle(X * tileW, Y * tileH, tileW, tileH), Color.Red, 1); // Center

            return base.Render(batch);
        }
    }
}