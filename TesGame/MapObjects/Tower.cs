﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;
using System.Collections.Generic;

namespace ArenaTotalWar
{
    class Tower : MapObject
    {
        public const int ZONE_BODY = 0;

        public class Case
        {

        }

        public class AngleFrame
        {
            public Rectangle _rect;
            public float _angle;

            public AngleFrame(Rectangle rect, float angle)
            {
                _rect = rect;
                _angle = angle;
            }
        }

        #region Attributes

        static Dictionary<Position, AngleFrame> _frames = new Dictionary<Position, AngleFrame>();

        Position _position = Position.UP;

        int _targetIndex = -1;
        //int _scanTempo = 0;

        Vector2 _targetPos = new Vector2();

        int _tickFire = 0;
        int _tempoFire = 50;

        int _ammo = 80;
        int _maxAmmo = 240;

        float _canonAngle;
        float _absCanonAngle;

        BarInfo _barInfo;

        Shake _shake = new Shake();

        #endregion 

        public Tower(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D)
        {
            _type = UID.Get<Tower>();

            _strength = 20;

            //int sizeW = 64;
            //int sizeH = 64;


            _frames[Position.UP] = new AngleFrame(new Rectangle(128, 0, 64, 64), Geo.RAD_270);
            _frames[Position.DOWN] = new AngleFrame(new Rectangle(128, 128, 64, 64), Geo.RAD_90);
            _frames[Position.LEFT] = new AngleFrame(new Rectangle(64, 64, 64, 64), Geo.RAD_180);
            _frames[Position.RIGHT] = new AngleFrame(new Rectangle(192, 64, 64, 64), Geo.RAD_0);

            _frames[Position.NW] = new AngleFrame(new Rectangle(64, 0, 64, 64), Geo.RAD_225);
            _frames[Position.NE] = new AngleFrame(new Rectangle(192, 0, 64, 64), Geo.RAD_315);
            _frames[Position.SW] = new AngleFrame(new Rectangle(64, 128, 64, 64), Geo.RAD_135);
            _frames[Position.SE] = new AngleFrame(new Rectangle(192, 128, 64, 64), Geo.RAD_45);

            SetSize(32, 32);
            SetPivot(16, 32);
            SetCollideZone(0, _rect); // Zone for Body Collision

            SetCollideZone(1, _rect); // Zone for Enemy Detection 

            map2D.Get(mapX, mapY)._type = _type;
            map2D.Get(mapX, mapY)._isCollidable = true;
            map2D.Get(mapX, mapY).UpdatePassLevel(2); // need Strong passLevel for pathfinding !

            GoTo(mapX, mapY);

            _barInfo = new BarInfo(this, Color.MonoGameOrange, Color.Black, Color.GhostWhite * .4f, _maxAmmo, 24, 4, 1);
            _barInfo.SetY(24);
            _barInfo.SetPlayShow(false, true);
            _barInfo.AppendTo(this);
            _barInfo.AddWarning(new BarInfo.Warning(0, _maxAmmo / 12, 80, false , true));

        }

        public static bool Add(Map2D<Tile> map2D, Level level, int mapX, int mapY)
        {
            if (null != map2D.Get(mapX, mapY))
            {
                //if (map2D.Get(mapX, mapY)._subType == UID.Get<Tower.Case>() && // Can build only if type of tile is a Tower.Case
                //    map2D.Get(mapX, mapY)._type != UID.Get<Tower>())
                {

                    for (int i = 0; i < 3; i++)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            int x = mapX - 1 + i;
                            int y = mapY - 1 + j;

                            level.AddPathCase(x, y, false);

                        }
                    }

                    map2D.Get(mapX, mapY)._id = new Tower(map2D, mapX, mapY).AppendTo(level)._index;

                    new Smoke(
                        mapX* map2D._tileW + map2D._tileW/2,
                        mapY* map2D._tileH + map2D._tileH/ 2, 
                        64, false, true, 24, 24)
                        .AppendTo(level);

                    return true;
                }
            }
            return false;
        }
        public void Remove()
        {
            //_map2D.Get(_mapPosition.X, _mapPosition.Y)._type = 0;
            //KillMe();
            Remove(_map2D, _parent, _mapPosition.X, _mapPosition.Y, true);
        }

        public Tower Shake(float intensity = 1, float step = .05f)
        {
            _shake.SetIntensity(intensity, step);
            return this;
        }

        public override Node Init()
        {

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            _z = -(int)_y;

            //_scanTempo++;

            UpdateRect();
            UpdateCollideZone(0, Gfx.AddRect(_rect, new Rectangle(-5, -5, 10, 10)));
            UpdateCollideZone(1, Gfx.AddRect(_rect, new Rectangle(-100, -100, 200, 200)), _targetIndex == Const.NoIndex);

            //if (_scanTempo > 64) _scanTempo = 0;

            var collideZone = Collision2D.OnCollideZoneByNodeType
            (
                GetCollideZone(1),
                new int[]
                {
                    UID.Get<Enemy>(),
                    UID.Get<EnemyOmega>()
                },
                0
            );

            if (null != collideZone) // Hit only one time !
            {
                Node collider = collideZone._node;

                if (null != collider)
                {
                    //if (collider._type == UID.Get<Hero>())
                    //{
                    //    collider.This<Hero>().AddEnergy(-10);
                    //    new PopInfo("-10", Color.Red)
                    //        .SetPosition(_x, _y - 32)
                    //        .AppendTo(_parent);

                    //    KillMe();

                    //}

                    if (collider._type == UID.Get<Enemy>() || collider._type == UID.Get<EnemyOmega>())
                    {
                        //collider.This<Enemy>().AddEnergy(-40);
                        //collider.This<Enemy>().HitMe();

                        //new PopInfo("Enemy", Color.Red)
                        //    .SetPosition(_x, _y - 32)
                        //    .AppendTo(_parent);

                        //collider.This<Enemy>().KillMe();

                        //_parent.This<Level.Level0>().Shake(8, .2f);

                        if (_targetIndex == Const.NoIndex) // trigger : A target is enter in detection zone !
                        {

                            _targetIndex = collider._index;
                        }

                        if (_targetIndex != collider._index) // If detect another collider change target !
                            _targetIndex = collider._index;

                    }


                }

            }
            else
            {
                _targetIndex = Const.NoIndex;
            }

            if (_targetIndex != Const.NoIndex)
            {
                Node target = _parent.Index(_targetIndex);

                if (null != target)
                {
                    _targetPos.X = target._x;
                    _targetPos.Y = target._y;

                    _canonAngle = Geo.GetRadian(new Vector2(_x, _y - 16), new Vector2(target._x, target._y));

                    _absCanonAngle = _canonAngle;
                    if (_canonAngle < 0)
                        _absCanonAngle = Geo.RAD_360 + _canonAngle;


                    // Manage canon display angle

                    if ((_absCanonAngle >= Geo.RAD_315 + Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_360) ||
                        (_absCanonAngle >= Geo.RAD_0 && _absCanonAngle < Geo.RAD_45 - Geo.RAD_22_5))
                        _position = Position.RIGHT;

                    if (_absCanonAngle >= Geo.RAD_45 - Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_45 + Geo.RAD_22_5)
                        _position = Position.SE;

                    if (_absCanonAngle >= Geo.RAD_45 + Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_90 + Geo.RAD_22_5)
                        _position = Position.DOWN;

                    if (_absCanonAngle >= Geo.RAD_90 + Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_135 + Geo.RAD_22_5)
                        _position = Position.SW;

                    if (_absCanonAngle >= Geo.RAD_135 + Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_180 + Geo.RAD_22_5)
                        _position = Position.LEFT;

                    if (_absCanonAngle >= Geo.RAD_180 + Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_225 + Geo.RAD_22_5)
                        _position = Position.NW;

                    if (_absCanonAngle >= Geo.RAD_225 + Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_270 + Geo.RAD_22_5)
                        _position = Position.UP;

                    if (_absCanonAngle >= Geo.RAD_270 + Geo.RAD_22_5 && _absCanonAngle < Geo.RAD_315 + Geo.RAD_22_5)
                        _position = Position.NE;


                    _tickFire++;
                    if (_tickFire > _tempoFire && _ammo > 0)
                    {
                        _tickFire = 0;

                        Vector2 direction = Geo.GetVector(new Vector2(_x, _y - 16), new Vector2(target._x, target._y), 3);

                        Vector2 canonPos = XY + new Vector2(0, -32) + Geo.GetVector(_frames[_position]._angle) * 24;

                        new Bullet(canonPos.X, canonPos.Y, direction.X, direction.Y, Bullet.FIRE, 40)
                            .AppendTo(_parent);

                        new Smoke(canonPos.X, canonPos.Y, 8, false)
                            .AppendTo(_parent);

                        --_ammo;

                        Shake(3, .1f);

                        Game1._sound_laserBlast.Play(.005f, 1f, 0);
                    }

                }
                else
                {
                    _targetIndex = Const.NoIndex;
                }
            }




            //_position = Position.DOWN;

            _barInfo.SetValue(_ammo);

            UpdateChilds(gameTime);

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            //batch.Draw(Game1._tex_sokoban, Gfx.AddRect(AbsRect(),0,-12,0,12) , new Rectangle(640, 128-24, 64, 64+24), Color.White);
            batch.Draw(Game1._tex_sokoban, 
                new Rectangle(AbsX-32, AbsY-56 + (int)_shake.GetVector2().Y, 64,64), 
                _frames[_position]._rect, 
                Color.White);

            //Vector2 canonPos = AbsVectorXY() + new Vector2(0,-32);

            //batch.DrawLine(canonPos, canonPos + Move.GetVector(_canonAngle) * 24, Color.OrangeRed, 1);

            //batch.DrawRectangle(AbsRect(), Color.YellowGreen *.4f, 2);

            //var colliderZone = Collision2D.OnCollideZoneByNodeType(GetCollideZone(0), UID.Get<Hero>(), 0);
            //if (colliderZone != null)
            //batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua, 1);

            //batch.DrawRectangle(GetCollideZone(1)._rect, Color.Red, 1);

            // Draw Line to Target 
            //if (_targetIndex != Const.NoIndex)
            //{
            //    Vector2 center = AbsVectorXY() + new Vector2(0, -16);

            //    //batch.DrawLine(AbsVectorXY() + new Vector2(0,-16), _targetPos, Color.Red, 1);

            //    //Draw.Line(batch, center, _targetPos, Color.Red, 1);

            //    //batch.DrawString(
            //    //    Game1._font_Main, 
            //    //    "Radian :" + Move.GetAngle(center, _targetPos), 
            //    //    new Vector2(AbsX() - 16, AbsY() - 48), 
            //    //    Color.Yellow);
            //}

            //Color colorAmmo = Color.YellowGreen;

            //if (_ammo < 60) colorAmmo = Color.Yellow;
            //if (_ammo < 40) colorAmmo = Color.Orange;
            //if (_ammo < 20) colorAmmo = Color.Red;

            //string text = _ammo.ToString();

            //if (_ammo == _maxAmmo)
            //{
            //    colorAmmo = Color.White;
            //    text = "MAX";
            //}

            //Draw.CenterBorderedStringXY(batch,
            //    Game1._font_Main,
            //    text,
            //    AbsX(), AbsY(),
            //    colorAmmo,
            //    Color.Black);

            RenderChilds(batch);

            //batch.DrawString(Game1._font_Main, ":" + _targetIndex, new Vector2(AbsX() - 16, AbsY() - 16), Color.Yellow);
            //batch.DrawString(Game1._font_Main, ":" + _absCanonAngle, new Vector2(AbsX() - 16, AbsY() - 16), Color.Yellow);

            return this;
        }

        public bool IsWarning()
        {
            return _ammo < 50;
        }


        public bool AddAmmo(int ammo)
        {
            if (_ammo >= _maxAmmo) return false; // Do nothing if maxAmmo reach !

            _ammo += ammo;

            if (_ammo > _maxAmmo) _ammo = _maxAmmo;

            new PopInfo("-20", Color.Red)
                .SetPosition(_x, _y - 32)
                .AppendTo(_parent);

            Game1._sound_key.Play(.8f, .05f, 0);

            return true;

        }
    }
}
