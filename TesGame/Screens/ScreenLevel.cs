﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Retro2D;
using System.Collections.Generic;

namespace ArenaTotalWar
{
    namespace Screen
    {
        class SelectLevel : Node
        {
            FxFog _fxFog;
            FxFog _fxFogFG;

            public SelectLevel()
            {
                _naviGate = new NaviGate(this);

                SetSize(Game1._screenW, Game1._screenH);

                this["back"] = new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 480)
                    .This<Gui.Button>().SetLabel("BACK")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            Retro2D.Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                this["options"] = new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 440)
                    .This<Gui.Button>().SetLabel("OPTIONS")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Options"]), new Transition.FadeInOut().Init());
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                _fxFog = new FxFog(240, (Rectangle)_rect);
                _fxFogFG = new FxFog(80, (Rectangle)_rect);

                // Container
                this["level"] = new LevelContainer(Game1._mouse).SetStyle(Game1._style_main)
                    .AppendTo(this)
                    .SetSize(560, 296)
                    .SetX(Position.CENTER)
                    .SetY(240)
                    .SetPivot(Position.CENTER);

                this["back"].SetNaviNode(Position.UP, this["options"]);
                this["options"].SetNaviNode(Position.DOWN, this["back"]);

                //this["options"].SetNaviNode(Position.UP, _levels[_lastBottomLevelPosition]);
                //this["options"].SetNaviNode(Position.UP, this["level"]);

                SetNaviNodeVertical(this["level"], this["options"]);
                SetNaviNodeVertical(this["back"], this["level"]);

                new ButtonInfo(Color.Yellow, "OK", "BACK").SetBlink()
                    .SetPosition(Game1._screenW - 80, Game1._screenH - 80)
                    .AppendTo(this);


                this["levelBox"] = new MessageBox(Game1._mouse)
                    //.AppendTo(this)
                    .SetPosition(Game1._screenW / 2 + 240, Game1._screenH / 2 + 90)
                    .This<Gui.Base>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false)
                    .SetSize(480, 192)
                    .SetPivot(Position.CENTER)
                    //.SetX(Game1._screenW / 2)
                    //.SetY(720)
                    .Init()
                    .This<MessageBox>().OnMessage((m) =>
                    {


                    });



            }

            public override Node Init()
            {
                InitChilds();

                _naviGate.SetNaviGate(true);
                _naviGate.SetNaviNodeFocusAt(2);

                return this;
            }

            public override Node Update(GameTime gameTime)
            {
                if (_naviGate.IsNaviGate)
                {
                    Game1.UpdateNaviGateButton(_naviGate, true);

                    if (Input.Button.OnPress("back")) this["back"].This<Gui.Button>().RunMessageProc(new Gui.Message { _node = this, _message = "ON_PRESS" });

                    if (_naviGate.OnSubmit() && !Retro2D.Screen.IsTransition())
                    {
                        _naviGate.MoveNaviGateToFocusedChild(this);
                        Game1._sound_click.Play(.5f, .05f, 0);
                    }

                    if (_naviGate.OnChangeFocus && !Retro2D.Screen.IsTransition())
                    {
                        Game1._sound_click.Play(.5f, .05f, 0);
                    }

                }



                UpdateChilds(gameTime);

                this["levelBox"].Update(gameTime);

                _fxFog.Update();
                _fxFogFG.Update();

                return this;
            }

            public override Node Render(SpriteBatch batch)
            {

                //float relMouseX = GetGame<Game1>()._relMouseX;
                //float relMouseY = GetGame<Game1>()._relMouseY;

                batch.GraphicsDevice.Clear(new Color(10, 20, 30));

                //batch.Draw(Game1._titleScreen, new Vector2(0, 0), Color.White);

                //Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, 32, 32, Color.Moccasin * 0.2f);

                _fxFog.Render(batch, Color.Black * .4f);

                RenderChilds(batch);


                _fxFogFG.Render(batch, Color.Red * .2f);

                //Draw.Grid(batch, 0, 0, screenW, screenH, 24, 24, Color.Moccasin * 0.6f);
                //batch.DrawRectangle(selectRect, Color.MonoGameOrange, 1);
                //Draw.Sight(batch, relMouseX, relMouseY, screenW, screenH, Color.RoyalBlue, 1);

                this["levelBox"].Render(batch);

                //Draw.CenterStringX(batch, Game1._bigFont, "LEVEL", (int)_rect.Width / 2, 2, Color.MonoGameOrange);

                return this;
            }
        }

    }
}
