﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;


namespace ArenaTotalWar
{
    namespace Screen
    {
        class Title : Node
        {
            FxFog _fxFog;
            FxFog _fxFogFG;

            ViewZone _viewZone;

            bool _isStart;

            public Title()
            {

                _viewZone = new ViewZone(Game1._window, Game1._window.ScreenRect, Game1._window.ScreenRect);
                //_viewZone = new ViewZone(Game1._window);

                _naviGate = new NaviGate(this);

                SetSize(Game1._screenW, Game1._screenH);

                #region Title Menu

                //new Gui.Window(Game1._nodeGui)
                //    .This<Gui.Window>().SetFocusable(false)
                //    .AppendTo(this)
                //    .SetSize(240, 240)
                //    .SetPivot(Position.CENTER)
                //    .SetPosition(Game1._screenW / 2, 400)
                //    .This<Gui.Window>().SetSkinGui(Game1._skinGUI01);

                var btn = new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 340)
                    .This<Gui.Button>().SetLabel("START")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            this["box"].This<MessageBox>().ToggleShow();
                            //Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Level"]), new Transition.FadeInOut().Init());
                        }

                            
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                System.Console.WriteLine("------ Start Button : " + btn.This<Gui.Button>().Style);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 380)
                    .This<Gui.Button>().SetLabel("OPTIONS")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Options"]), new Transition.FadeInOut().Init());
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 420)
                    .This<Gui.Button>().SetLabel("CREDITS")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            System.Console.WriteLine("ON_PRESS CREDITS");
                            //Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Options"]), new Transition.FadeInOut().Init());
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 460)
                    .This<Gui.Button>().SetLabel("QUIT GAME")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            Game1._isQuit = true;
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                #endregion

                // Box Creation !

                #region Box Number Player

                Style style = Game1._style_main;//.Clone();

                this["box"] = new MessageBox(Game1._mouse, 0, 40, 16)
                    .AppendTo(this)
                    .This<Gui.Base>().SetStyle(style)
                    .Get<Addon.Draggable>().SetDraggable(false)
                    .SetSize(240, 280)
                    .SetX(Position.CENTER)
                    .SetY(Position.CENTER)
                    .Init()
                    .This<MessageBox>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            System.Console.WriteLine("dialbox NAVIGATE ON SUBMIT");
                    });

                new Gui.Button(Game1._mouse).SetName("1 Player")
                    .AppendTo(this["box"])
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(100)
                    .This<Gui.Button>().SetLabel("1 Player")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            PlayerATW.SetNbPlayers(NbPlayers._1);
                            System.Console.WriteLine("ON_PRESS dialbox :" + m._node._name);
                            Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this);
                        }
                    })
                    .This<Gui.Button>().SetStyle(style)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse).SetName("2 Players")
                    .AppendTo(this["box"])
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(140)
                    .This<Gui.Button>().SetLabel("2 Players")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            PlayerATW.SetNbPlayers(NbPlayers._2);
                            System.Console.WriteLine("ON_PRESS dialbox :" + m._node._name);
                            Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this);
                        }
                    })
                    .This<Gui.Button>().SetStyle(style)
                    .Get<Addon.Draggable>().SetDraggable(false);


                new Gui.Button(Game1._mouse).SetName("3 Players")
                    .AppendTo(this["box"])
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(180)
                    .This<Gui.Button>().SetLabel("3 Players")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            PlayerATW.SetNbPlayers(NbPlayers._3);
                            System.Console.WriteLine("ON_PRESS dialbox :" + m._node._name);
                            Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this);
                        }
                    })
                    .This<Gui.Button>().SetStyle(style)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse).SetName("4 Players")
                    .AppendTo(this["box"])
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(220)
                    .This<Gui.Button>().SetLabel("4 Players")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            PlayerATW.SetNbPlayers(NbPlayers._4);
                            System.Console.WriteLine("ON_PRESS dialbox :" + m._node._name);
                            Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this);
                        }
                    })
                    .This<Gui.Button>().SetStyle(style)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["box"])
                    .SetSize(48, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(260)
                    .This<Gui.Button>().SetLabel("Back")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            Game1._messageQueue.Post(0, new Data {  _msg = "OK" }, this["box"]);
                    })
                    .This<Gui.Button>().SetStyle(style)
                    .Get<Addon.Draggable>().SetDraggable(false);

                #endregion

                _fxFog = new FxFog(240, (Rectangle)_rect);
                _fxFogFG = new FxFog(80, (Rectangle)_rect);

                new ButtonInfo(Color.Yellow, "OK", "BACK", "XXX", "YYY")
                    .SetBlink()
                    .SetPosition(Game1._screenW - 80, Game1._screenH - 80)
                    .AppendTo(this);

            }

            public override Node Init()
            {
                InitChilds();

                _naviGate.SetNaviGate(true);
                _naviGate.SetNaviNodeFocusAt(0);

                return this;
            }

            public override Node Update(GameTime gameTime)
            {
                //_viewZone.UpdateRectSrc(new Rectangle(0, 0, Game1._relMouseX, Game1._relMouseY));
                //_viewZone.UpdateRectView(new Rectangle(0, 0, Game1._relMouseX, Game1._relMouseY));

                if (Input.Button.OnePress("showBox", Game1.Player1.GetButton((int)SNES.BUTTONS.START) != 0))
                {
                    this["box"].This<MessageBox>().ToggleShow();
                }

                if (_naviGate.IsNaviGate)
                {

                    Game1.UpdateNaviGateButton(_naviGate);

                    if (_naviGate.OnSubmit())
                    {
                        _naviGate.MoveNaviGateToFocusedChild(this);
                        Game1._sound_click.Play(.5f, .05f, 0);
                    }

                    if (_naviGate.OnChangeFocus && !Retro2D.Screen.IsTransition())
                    {
                        Game1._sound_click.Play(.5f, .05f, 0);
                    }

                }

                // Get Message from box
                if (HasMessage())
                {
                    Data data = (Data)_message._data;

                    if (data._msg == "OK")
                    {
                        this["box"].This<MessageBox>().ToggleShow();
                        _isStart = true;
                        System.Console.WriteLine("Select {0} Player", (int)PlayerATW.GetNbPlayers());
                    }

                    EndMessage();
                }

                // If Nb Player is selected and box is closed then go to next screen
                if (_isStart && !this["box"]._isVisible)
                {
                    _isStart = false;
                    Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Level"]), new Transition.FadeInOut().Init());
                }


                UpdateChilds(gameTime);

                _fxFog.Update();
                _fxFogFG.Update();

                return this;
            }

            public override Node Render(SpriteBatch batch)
            {
                // PreRender !
                //batch = _viewZone.Batch();
                //Game1._window._graphics.GraphicsDevice.SetRenderTarget(Game1.fogTarget);
                //batch.GraphicsDevice.Clear(Color.White * 0.1f);
                //batch.Begin(SpriteSortMode.Deferred, BlendState.Additive);
                //batch.Draw
                //(
                //    Game1._tex_fow,
                //    new Vector2(Game1._relMouseX, Game1._relMouseY),
                //    Game1._tex_fow.Bounds,
                //    Color.White,
                //    0,
                //    new Vector2(Game1._tex_fow.Width / 2, Game1._tex_fow.Height / 2),
                //    new Vector2(16, 16),
                //    SpriteEffects.None,
                //    0

                //);
                //batch.End();


                //_viewZone.BeginRender();

                //batch.GraphicsDevice.Clear(new Color(10, 20, 30));

                batch.Draw(Game1._tex_TitleScreen, new Rectangle(0,0, Game1._screenW, Game1._screenH), Color.White);

                //Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, 32, 32, Color.Moccasin * 0.2f);

                _fxFog.Render(batch, Color.Black * .4f);

                RenderChilds(batch);

                _fxFogFG.Render(batch, Color.Red * .2f);

                //Draw.Grid(batch, 0, 0, screenW, screenH, 24, 24, Color.Moccasin * 0.6f);
                //batch.DrawRectangle(selectRect, Color.MonoGameOrange, 1);
                //Draw.Sight(batch, relMouseX, relMouseY, screenW, screenH, Color.RoyalBlue, 1);

                //batch.Draw(Game1._tex_basicButton, new Vector2(Game1._screenW - 80, Game1._screenH - 80), Color.White);

                //Draw.CenterStringX(batch, Game1._bigFont, "TITLE", (int)_rect.Width / 2, 2, Color.MonoGameOrange);



                // PostRender !
                //_viewZone.EndRender();


                //Game1._window._graphics.GraphicsDevice.SetRenderTarget(_viewZone._renderTarget2D);
                //BlendState blendState = new BlendState();
                //blendState.AlphaDestinationBlend = Blend.SourceColor;
                //blendState.ColorDestinationBlend = Blend.SourceColor;
                //blendState.AlphaSourceBlend = Blend.Zero;
                //blendState.ColorSourceBlend = Blend.Zero;

                //batch.Begin(SpriteSortMode.Deferred, blendState, null, null, null);
                //batch.Draw(Game1.fogTarget, new Rectangle(0, 0, Game1._screenW, Game1._screenH), Color.White);
                //batch.End();
                //_viewZone.Render();

                return this;
            }
        }

    }
}
