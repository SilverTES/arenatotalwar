﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using Retro2D;


namespace ArenaTotalWar
{
    namespace Screen
    {
        class Play : Node
        {
            #region Attributes

            Node _nodeLevel;

            bool _isPause = false; // ScreenPlay Pause !

            bool _onGameOver = false;
            bool _isGameOver = false;

            bool _onGameWin = false;
            bool _isGameWin = false;

            bool _isQuitLevel = false;
            bool _isRestartLevel = false;

            //bool _isStartUp = true; // is first start of the level !
            bool _onStartUp = true; // on first start of the level !

            // MiniMap
            MiniMap _miniMap;

            MessageBox _levelStartupBox; // Message Box at each level startup !

            #endregion 

            public Node GetLevel()
            {
                return _nodeLevel;
            }
            public Node SetLevel(Node nodeLevel)
            {
                _nodeLevel = nodeLevel;
                _levelStartupBox = (MessageBox)_nodeLevel["box"];

                _levelStartupBox.SetPosition(Game1._screenW/2 + _levelStartupBox._rect.Width/2, Game1._screenH / 2 + _levelStartupBox._rect.Height / 2);

                _levelStartupBox.Init();

                return this;
            }
            public void GameOver()
            {
                _isGameOver = true;
            }
            public void GameWin()
            {
                _isGameWin = true;
            }

            public Play()
            {

                int px = 640 / 2 - 80;
                new PlayerHud(0, "Player 1").SetPosition(0 + px, 300).AppendTo(this);
                new PlayerHud(1, "Player 2").SetPosition(640 + px, 300).AppendTo(this);
                new PlayerHud(2, "Player 3").SetPosition(0 + px, 660).AppendTo(this);
                new PlayerHud(3, "Player 4").SetPosition(640 + px, 660).AppendTo(this);

                #region DialBox PauseMenu
                this["pauseMenu"] = new DialBox(Game1._mouse, Game1._screenH/2, Game1._screenH + 320)
                    //.AppendTo(this)
                    .This<Gui.Base>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false)
                    .SetSize(240, 320)
                    .SetPivot(Position.CENTER)
                    .SetX(Game1._screenW/2)
                    .SetY(Game1._screenH + 320)
                    .Init()
                    .This<DialBox>().OnMessage((m) =>
                    {
                        if (m._message == "ON_SHOW")
                        {
                            MediaPlayer.Volume = 0.05f;

                            //System.Console.WriteLine("_isPause = true");
                            _isPause = true;

                        }

                        if (m._message == "OFF_HIDE")
                        {
                            MediaPlayer.Volume = 0.2f;

                            //System.Console.WriteLine("_isPause = false");
                            _isPause = false;

                            if (_isQuitLevel) // Quit current level !!
                            {
                                _isQuitLevel = false;
                                _nodeLevel = null;

                                MediaPlayer.Play(Game1._song_intro); // restart menu music !
                                MediaPlayer.Volume = 0.2f;
                                MediaPlayer.IsRepeating = true;

                                Retro2D.Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                            }

                            if (_isRestartLevel) // Restart level !
                            {
                                _onStartUp = true;

                                _nodeLevel.Init();
                                
                                _isRestartLevel = false;
                                System.Console.WriteLine(" Restart Level");
                            }
                        }

                    });

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["pauseMenu"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(180)
                    .This<Gui.Button>().SetLabel("Mission")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            _levelStartupBox?.ToggleShow();
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["pauseMenu"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(210)
                    .This<Gui.Button>().SetLabel("Options")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                            Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Options"]), new Transition.FadeInOut().Init());

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["pauseMenu"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(240)
                    .This<Gui.Button>().SetLabel("Quit")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            _isQuitLevel = true;
                            this["pauseMenu"].This<DialBox>().ToggleShow();
                        }
                        
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["pauseMenu"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(270)
                    .This<Gui.Button>().SetLabel("Restart")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            _isRestartLevel = true;
                            this["pauseMenu"].This<DialBox>().ToggleShow();
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["pauseMenu"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(300)
                    .This<Gui.Button>().SetLabel("Resume")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            //System.Console.WriteLine("dialbox OK");
                            Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this["pauseMenu"]);

                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .This<Gui.Button>().SetAsNaviNodeFocus() // Get the focus here !!
                    .Get<Addon.Draggable>().SetDraggable(false);
                #endregion

                #region DialBox GameOver
                this["gameOver"] = new MessageBox(Game1._mouse)
                    //.AppendTo(this)
                    .SetPosition(Game1._screenW / 2 + 240, Game1._screenH / 2 + 90)
                    .This<Gui.Base>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false)
                    .SetSize(480, 192)
                    .SetPivot(Position.CENTER)
                    //.SetX(Game1._screenW / 2)
                    //.SetY(720)
                    .Init()
                    .This<MessageBox>().OnMessage((m) =>
                    {
                        if (m._message == "ON_SHOW")
                        {
                            //System.Console.WriteLine("_isPause = true");
                            _isPause = true;
                        }

                        if (m._message == "OFF_HIDE")
                        {
                            //System.Console.WriteLine("_isPause = false");
                            _isPause = false;

                            if (_isQuitLevel) // Quit current level !!
                            {
                                _isQuitLevel = false;
                                _nodeLevel = null;

                                MediaPlayer.Play(Game1._song_intro); // restart menu music !
                                MediaPlayer.Volume = 0.2f;
                                MediaPlayer.IsRepeating = true;

                                Retro2D.Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                            }

                            if (_isRestartLevel) // Restart level !
                            {
                                _onStartUp = true;

                                _nodeLevel.Init();

                                _isRestartLevel = false;
                                System.Console.WriteLine(" Restart Level");
                            }
                        }

                    });

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["gameOver"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(130)
                    .This<Gui.Button>().SetLabel("Quit")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            _isQuitLevel = true;
                            this["gameOver"].This<MessageBox>().ToggleShow();
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["gameOver"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(160)
                    .This<Gui.Button>().SetLabel("Restart")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            _isRestartLevel = true;
                            this["gameOver"].This<MessageBox>().ToggleShow();
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                //new Gui.Label(Game1._nodeGui, true)
                //    .AppendTo(this["gameOver"])
                //    .SetSize(80, 8)
                //    .SetPivot(Position.CENTER)
                //    .SetX(Position.CENTER)
                //    .SetY(40)
                //    .This<Gui.Label>().SetStyle(Game1._style_main)
                //    .This<Gui.Label>().SetLabel("GAME OVER");

                new Gui.Image(Game1._mouse, Game1._tex_gameOver, Color.White, new Rectangle(0, 0, 440, 80))
                    .AppendTo(this["gameOver"])
                    .SetSize(440, 80)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(64)
                    .This<Gui.Base>().SetFocusable(false);

                #endregion

                #region DialBox GameWin
                this["gameWin"] = new MessageBox(Game1._mouse)
                    //.AppendTo(this)
                    .SetPosition(Game1._screenW / 2 + 240, Game1._screenH / 2 + 90)
                    .This<Gui.Base>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false)
                    .SetSize(480, 192)
                    .SetPivot(Position.CENTER)
                    //.SetX(Game1._screenW / 2)
                    //.SetY(720)
                    .Init()
                    .This<MessageBox>().OnMessage((m) =>
                    {
                        if (m._message == "ON_SHOW")
                        {
                            //System.Console.WriteLine("_isPause = true");
                            _isPause = true;
                        }

                        if (m._message == "OFF_HIDE")
                        {
                            //System.Console.WriteLine("_isPause = false");
                            _isPause = false;

                            if (_isQuitLevel) // Quit current level !!
                            {
                                _isQuitLevel = false;
                                _nodeLevel = null;

                                MediaPlayer.Play(Game1._song_intro); // restart menu music !
                                MediaPlayer.Volume = 0.2f;
                                MediaPlayer.IsRepeating = true;

                                Retro2D.Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                            }

                            if (_isRestartLevel) // Restart level !
                            {
                                _onStartUp = true;

                                _nodeLevel.Init();

                                _isRestartLevel = false;
                                System.Console.WriteLine(" Restart Level");
                            }
                        }

                    });

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["gameWin"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(130)
                    .This<Gui.Button>().SetLabel("Quit")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            _isQuitLevel = true;
                            this["gameWin"].This<MessageBox>().ToggleShow();
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Button(Game1._mouse)
                    .AppendTo(this["gameWin"])
                    .SetSize(80, 8)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(160)
                    .This<Gui.Button>().SetLabel("Restart")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            _isRestartLevel = true;
                            this["gameWin"].This<MessageBox>().ToggleShow();
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                new Gui.Image(Game1._mouse, Game1._tex_gameOver, Color.White, new Rectangle(0,80,440,80))
                    .AppendTo(this["gameWin"])
                    .SetSize(440, 80)
                    .SetPivot(Position.CENTER)
                    .SetX(Position.CENTER)
                    .SetY(64)
                    .This<Gui.Base>().SetFocusable(false);

                //new Gui.Label(Game1._nodeGui, true)
                //    .AppendTo(this["gameWin"])
                //    .SetSize(80, 8)
                //    .SetPivot(Position.CENTER)
                //    .SetX(Position.CENTER)
                //    .SetY(64)
                //    .This<Gui.Window>().SetFocusable(false)
                //    .This<Gui.Label>().SetStyle(Game1._style_main)
                //    .This<Gui.Label>().SetLabel("MISSION COMPLETE");

                #endregion
            }

            public override Node Init()
            {
                _nodeLevel?.Init();

                if (null != _nodeLevel)
                {

                    int mapW = _nodeLevel.This<Level>().GetArenaSizeW();
                    int mapH = _nodeLevel.This<Level>().GetArenaSizeH();
                    _miniMap = new MiniMap
                    (
                        _nodeLevel.This<Level>(),
                        new Rectangle(0, 0, mapW / 12, mapH /12),
                        mapW,
                        mapH
                        
                    );

                    // RePosition _miniMap
                    if ((int)PlayerATW.GetNbPlayers() > 1)
                    {
                        _miniMap.SetPosition
                        (
                            Game1._screenW / 2 - _miniMap.GetRect().Width / 2,
                            Game1._screenH / 2 - _miniMap.GetRect().Height / 2
                        );

                    }
                    else
                    {
                        _miniMap.SetPosition(Game1._screenW - _miniMap.GetRect().Width - 4, 4);
                    }


                }


                MediaPlayer.Play(Game1._song_start);
                MediaPlayer.Volume = 0.2f;
                MediaPlayer.IsRepeating = true;

                _onStartUp = true;

                return this;
            }
            public override Node Update(GameTime gameTime)
            {
                // Manage : GameWin & GameOver
                if (null != _nodeLevel)
                {
                    if (_nodeLevel.This<Level0>().IsWin) GameWin();
                    if (_nodeLevel.This<Level0>().IsLose) GameOver();
                }

                // Game Over Trigger !
                if (!_isGameOver) _onGameOver = false;
                if (_isGameOver && !_onGameOver)
                {
                    _onGameOver = true;

                    System.Console.WriteLine("On Game Over !!");
                    this["gameOver"].This<MessageBox>().ToggleShow();

                    Game1._sound_lose.Play(.8f, .1f, 0);
                }

                // Game Win Trigger !
                if (!_isGameWin) _onGameWin = false;
                if (_isGameWin && !_onGameWin)
                {
                    _onGameWin = true;

                    System.Console.WriteLine("On Game Win !!");
                    this["gameWin"].This<MessageBox>().ToggleShow();

                    Game1._sound_win.Play(.8f, .001f, 0);
                }


                if (_onStartUp)// || Input.Button.OnePress("box", Keyboard.GetState().IsKeyDown(Keys.F8)) )
                {
                    _isPause = true;

                    if (!Retro2D.Screen.IsTransition())
                    {
                        System.Console.WriteLine("Level StartUp");
                        _onStartUp = false;

                        _isGameOver = false; // Reset GameOver !
                        _isGameWin = false; // Reset GameWin !

                        _levelStartupBox?.ToggleShow();
                    }
                }

                if (_levelStartupBox._animate.OnEnd("hide") && !this["pauseMenu"].This<DialBox>().IsShow())
                    _isPause = false;

                _levelStartupBox?.Update(gameTime);

                UpdateRect();

                // Pause Menu Game !
                if (Input.Button.OnePress("Pause", Game1.Player1.GetButton((int)SNES.BUTTONS.START)!=0) && 
                    !_levelStartupBox.IsShow() &&
                    !this["gameOver"].This<MessageBox>().IsShow() &&
                    !this["gameWin"].This<MessageBox>().IsShow())
                    this["pauseMenu"].This<DialBox>().ToggleShow();


                // Update all DialBox !
                this["pauseMenu"].Update(gameTime);
                this["gameOver"].Update(gameTime);
                this["gameWin"].Update(gameTime);

                #region Debug Control
                //_selectRect.X = GetPosInMap(Game1._relMouseX, _tileW);
                //_selectRect.Y = GetPosInMap(Game1._relMouseY, _tileH);
                //_mouseMapX = (int)(_selectRect.X / _tileW); 
                //_mouseMapY = (int)(_selectRect.Y / _tileH);

                //if (Game1._mouseState.LeftButton == ButtonState.Pressed)
                //{
                //    if (!Keyboard.GetState().IsKeyDown(Keys.LeftControl))
                //    {
                //        if (ItemCoin.Add(_mapObject, this, _mouseMapX, _mouseMapY))
                //        {
                //            new PopInfo("+10 Points", Color.Coral)
                //                .SetPosition(Game1._relMouseX, Game1._relMouseY)
                //                .AppendTo(this);

                //            Game1._sound_coin.Play(0.1f, 0.1f, 0);
                //        }
                //    }
                //    else
                //    {
                //        Explosion.Add(_mapObject, this, _mouseMapX, _mouseMapY);
                //    }

                //}
                //if (Game1._mouseState.MiddleButton == ButtonState.Pressed)
                //{
                //    //_map2D.SetTile(_layer1, _mouseMapX, _mouseMapY, _tileA);

                //    //MapBlock.Add(_mapObject, this, _mouseMapX, _mouseMapY);
                //    Block.Add(_mapObject, this, _mouseMapX, _mouseMapY);
                //}
                //if (Game1._mouseState.RightButton == ButtonState.Pressed)
                //{
                //    //Console.WriteLine("Delete Tile !");
                //    //_map2D.SetTile(_layer1, _mouseMapX, _mouseMapY, new TileMap());

                //    MapObject.Remove(_mapObject, this, _mouseMapX, _mouseMapY);

                //    if (null != _mapObject.Get(_mouseMapX, _mouseMapY))
                //    {
                //            if (null != Index(_mapObject.Get(_mouseMapX, _mouseMapY)._id))
                //                _childs.At(_mapObject.Get(_mouseMapX, _mouseMapY)._id).KillMe();
                //    }
                //}
                //if (Input.Button.OnePress("WaveExplosion", Keyboard.GetState().IsKeyDown(Keys.LeftAlt)))
                //{
                //    new ExplosionWave(_mapObject, Position.RIGHT, 4, 4)
                //        .AppendTo(this)
                //        .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);

                //    new ExplosionWave(_mapObject, Position.LEFT, 4, 4)
                //        .AppendTo(this)
                //        .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);

                //    new ExplosionWave(_mapObject, Position.UP, 4, 4)
                //        .AppendTo(this)
                //        .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);

                //    new ExplosionWave(_mapObject, Position.DOWN, 4, 4)
                //        .AppendTo(this)
                //        .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);
                //}
                //if (Input.Button.OnePress("PoseBomb", Keyboard.GetState().IsKeyDown(Keys.LeftShift)))
                //{
                //    //Bomb.Add(_mapObject, this, _mouseMapX, _mouseMapY);
                    
                //    Enemy.Add(_mapObject, _mouseMapX, _mouseMapY);
                //}
                #endregion

                if (!_isPause)
                {
                    _nodeLevel?.Update(gameTime);
                }

                UpdateChilds(gameTime);

                // Update Player HUD !
                //for (int i = 0; i < _playerHuds.Length; i++)
                //{
                //    _playerHuds[i].Update();
                //}

                _miniMap.Update();

                return this;
            }
            public override Node Render(SpriteBatch batch)
            {

                batch.GraphicsDevice.Clear(new Color(10, 20, 30));

                _nodeLevel?.Render(batch);

                //batch.DrawLine(new Vector2(10,10), new Vector2(NbNode()/10,10), Color.Green, 4);
                //batch.DrawLine(new Vector2(10,10), new Vector2(NbActive()/10,10), Color.Red, 4);
                //batch.DrawString(Game1._font_Main, NbActive()+"/"+NbNode(), new Vector2(NbNode() / 10 + 2, 10), Color.LightYellow);

                // Draw SplitScreen Separator
                if ((int)PlayerATW.GetNbPlayers() > 2)
                {
                    Draw.Line(batch, new Vector2(Game1._screenW / 2, 0), Game1._screenH, Geo.RAD_90, Color.Black, 1);
                    Draw.Line(batch, new Vector2(0, Game1._screenH / 2), Game1._screenW, Geo.RAD_0, Color.Black, 1);
                }

                RenderChilds(batch);
                //for (int i=0; i<_playerHuds.Length; i++)
                //{
                //    _playerHuds[i].Render(batch);
                //}

                if (_isPause)
                {
                    Draw.FillRectangle(batch, new Rectangle(0, 0, Game1._screenW, Game1._screenH), Color.DarkBlue * .2f);
                    //Draw.CenterStringXY(batch, Game1._font_Big, "- P A U S E -", Game1._screenW / 2, Game1._screenH / 2, Color.MonoGameOrange);
                }

                _miniMap.Render(batch);

                // Render all DialBox
                this["pauseMenu"].Render(batch);
                this["gameOver"].Render(batch);
                this["gameWin"].Render(batch);


                _levelStartupBox?.Render(batch);

                // Draw Bank
                Draw.CenterBorderedStringXY(batch, Game1._font_Main, " NbPlayers:" + (int)PlayerATW.GetNbPlayers() + " Wave:" + 0 + " Bank:" + PlayerATW.GetBankPoint(), Game1._screenW / 2, 12, Color.GreenYellow, Color.Black);


                // Debug : Show nb Node in level
                if (null != _nodeLevel)
                {
                    Level level = _nodeLevel.This<Level>();

                    Draw.Line(batch, new Vector2(2, 4), new Vector2(level.NbNode() / 2, 4), Color.Green, 2);
                    Draw.Line(batch, new Vector2(2, 4), new Vector2(level.NbActive() / 2, 4), Color.Red, 2);
                    batch.DrawString
                    (
                        Game1._font_Main, 
                        level.NbActive() + "/" + 
                        level.NbNode() + " : "+ 
                        Game1._frameCounter.Fps() + " FPS", 
                        new Vector2(level.NbNode() / 2 + 4, 4), 
                        Color.LightYellow
                    );
                }

                return this;
            }

        }

    }
}
