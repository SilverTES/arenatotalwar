﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System;

namespace ArenaTotalWar
{
    namespace Screen
    {
        class Options : Node
        {

            string _pathGamePadSetup;

            public Options SetPathGamePadSetup(string pathGamePadSetup)
            {
                _pathGamePadSetup = pathGamePadSetup;
                return this;
            }

            public Options(string pathGamePadSetup = "default.xml")
            {
                _pathGamePadSetup = pathGamePadSetup;

                _naviGate = new NaviGate(this);

                this["back"] = new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 480)
                    .This<Gui.Button>().SetLabel("BACK")
                    .This<Gui.Button>().OnMessage((m) => 
                    {
                        if (m._message == "ON_PRESS")
                            Retro2D.Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                this["save"] = new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 60)
                    .This<Gui.Button>().SetLabel("SAVE")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            Game1._sound_itemLife.Play(.5f, .05f, 0);
                            Console.WriteLine("Save GamePad Setup");

                            new PopInfo("Save GamePad Setup", Color.Orange)
                                .SetPosition(m._node._x, m._node._y - 32)
                                .AppendTo(this);

                            // Serialize All GamePad Setup in File
                            Game1.SaveGamePadSetupToFile(_pathGamePadSetup);
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                this["load"] = new Gui.Button(Game1._mouse)
                    .AppendTo(this)
                    .SetSize(120, 16)
                    .SetPivot(Position.CENTER)
                    .SetPosition(Game1._screenW / 2, 100)
                    .This<Gui.Button>().SetLabel("LOAD")
                    .This<Gui.Button>().OnMessage((m) =>
                    {
                        if (m._message == "ON_PRESS")
                        {
                            Game1._sound_itemLife.Play(.5f, .05f, 0);
                            Console.WriteLine("Load GamePad Setup");

                            new PopInfo("Load GamePad Setup", Color.Orange)
                                .SetPosition(m._node._x, m._node._y - 32)
                                .AppendTo(this);

                            // Load XML Files contains GamePad Setup
                            Game1.LoadGamePadSetupFromFile(_pathGamePadSetup);
                        }

                    })
                    .This<Gui.Button>().SetStyle(Game1._style_main)
                    .Get<Addon.Draggable>().SetDraggable(false);

                // CallBack when Gamepad button Rec OK
                //Action recOkCallBack = () => {  };
                //Action onChangeFocus = () => {  };

                Style setupStyle = new Style
                {
                    _useSkin = false,
                    _backgroundColor = Style.ColorValue.MakeColor(Color.Transparent),
                    _focusBackgroundFillColor = Style.ColorValue.MakeColor(Color.Black * .8f),
                    _focusBorderColor = Style.ColorValue.MakeColor(Color.Red),
                    _borderWidth = 1,
                    _border = new Style.Space(8)
                };

                Style buttonStyle = new Style
                {
                    _skinGui = Game1._skinGui_01,
                    _color = Style.ColorValue.MakeColor(Color.OrangeRed),
                    _focusBackgroundFillColor = Style.ColorValue.MakeColor(Color.Red * .4f),
                    _focusBorderColor = Style.ColorValue.MakeColor(Color.Red),
                    _border = new Style.Space(4)
                };

                Action<Gui.Message> gamePadMessage = (m) =>
                {
                    if (m._message == "ON_OK" || m._message == "ON_SETUP" || m._message == "ON_TEST" || m._message == "ON_FOCUS_CHANGE")
                        Game1._sound_click.Play(.2f, .8f, 0);

                    if (m._message == "ON_REC_OK")
                        Game1._sound_itemLife.Play(.2f, .8f, 0);

                    if (m._message == "ON_PRESS")
                        Game1._sound_itemLife.Play(.2f, .8f, 0);

                    if (m._message == "ON_STOP_TEST")
                        Game1._sound_itemLife.Play(.2f, .2f, 0);

                    if (m._message == "ON_END_REC")
                        Game1._sound_itemLife.Play(.2f, .2f, 0);

                };

                int center = Game1._screenW / 2;

                int left = center - 180;
                int right = center + 180;
                int up = 180;
                int down = 380;


                this["gamepad1"] = new SetupGamePad(Game1.Player1, buttonStyle, Game1._mouse) // Configurator GamePAD 1
                    .SetPlayer(Game1._players[Game1.PLAYER1])
                    .This<SetupGamePad>().OnMessage(gamePadMessage)
                    .This<SetupGamePad>().SetInfo("Player 1", Color.Red, 0, 50)
                    .AppendTo(this)
                    .SetX(left).SetY(up)
                    .Init()
                    .This<SetupGamePad>().SetStyle(setupStyle)
                    .This<SetupGamePad>()._drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH))
                    .This<SetupGamePad>()._drag.SetDragRectNode(true)
                    .This<SetupGamePad>()._drag.SetDraggable(false);

                this["gamepad2"] = new SetupGamePad(Game1.Player1, buttonStyle, Game1._mouse) // Configurator GamePAD 2
                    .SetPlayer(Game1._players[Game1.PLAYER2])
                    .This<SetupGamePad>().OnMessage(gamePadMessage)
                    .This<SetupGamePad>().SetInfo("Player 2", Color.Red, 0, 50)
                    .AppendTo(this)
                    .SetX(left).SetY(down)
                    .Init()
                    .This<SetupGamePad>().SetStyle(setupStyle)
                    .This<SetupGamePad>()._drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH))
                    .This<SetupGamePad>()._drag.SetDragRectNode(true)
                    .This<SetupGamePad>()._drag.SetDraggable(false);

                this["gamepad3"] = new SetupGamePad(Game1.Player1, buttonStyle, Game1._mouse) // Configurator GamePAD 3
                    .SetPlayer(Game1._players[Game1.PLAYER3])
                    .This<SetupGamePad>().OnMessage(gamePadMessage)
                    .This<SetupGamePad>().SetInfo("Player 3", Color.Red, 0, 50)
                    .AppendTo(this)
                    .SetX(right).SetY(up)
                    .Init()
                    .This<SetupGamePad>().SetStyle(setupStyle)
                    .This<SetupGamePad>()._drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH))
                    .This<SetupGamePad>()._drag.SetDragRectNode(true)
                    .This<SetupGamePad>()._drag.SetDraggable(false);

                this["gamepad4"] = new SetupGamePad(Game1.Player1, buttonStyle, Game1._mouse) // Configurator GamePAD 4
                    .SetPlayer(Game1._players[Game1.PLAYER4])
                    .This<SetupGamePad>().OnMessage(gamePadMessage)
                    .This<SetupGamePad>().SetInfo("Player 4", Color.Red, 0, 50)
                    .AppendTo(this)
                    .SetX(right).SetY(down)
                    .Init()
                    .This<SetupGamePad>().SetStyle(setupStyle)
                    .This<SetupGamePad>()._drag.SetLimitRect(new RectangleF(0, 0, Game1._screenW, Game1._screenH))
                    .This<SetupGamePad>()._drag.SetDragRectNode(true)
                    .This<SetupGamePad>()._drag.SetDraggable(false);


                // NaviNode
                this["back"]
                    .SetNaviNode(Position.LEFT, this["gamepad2"])
                    .SetNaviNode(Position.RIGHT, this["gamepad4"])
                    .SetNaviNode(Position.DOWN, this["save"])
                    .SetNaviNode(Position.UP, this["gamepad2"]);

                this["save"]
                    .SetNaviNode(Position.LEFT, this["gamepad1"])
                    .SetNaviNode(Position.RIGHT, this["gamepad4"])
                    .SetNaviNode(Position.DOWN, this["load"])
                    .SetNaviNode(Position.UP, this["back"]);

                this["load"]
                    .SetNaviNode(Position.LEFT, this["gamepad1"])
                    .SetNaviNode(Position.RIGHT, this["gamepad3"])
                    .SetNaviNode(Position.DOWN, this["gamepad1"])
                    .SetNaviNode(Position.UP, this["save"]);

                this["gamepad1"]
                    .SetNaviNode(Position.UP, this["load"])
                    .SetNaviNode(Position.RIGHT, this["gamepad3"])
                    .SetNaviNode(Position.DOWN, this["gamepad2"]);
                this["gamepad2"]
                    .SetNaviNode(Position.UP, this["gamepad1"])
                    .SetNaviNode(Position.RIGHT, this["gamepad4"])
                    .SetNaviNode(Position.DOWN, this["back"]);
                this["gamepad3"]
                    .SetNaviNode(Position.UP, this["load"])
                    .SetNaviNode(Position.LEFT, this["gamepad1"])
                    .SetNaviNode(Position.DOWN, this["gamepad4"]);
                this["gamepad4"]
                    .SetNaviNode(Position.UP, this["gamepad3"])
                    .SetNaviNode(Position.LEFT, this["gamepad2"])
                    .SetNaviNode(Position.DOWN, this["back"]);

                new ButtonInfo(Color.Yellow, "OK", "BACK").SetBlink()
                    .SetPosition(Game1._screenW - 80, Game1._screenH - 80)
                    .AppendTo(this);

            }

            public override Node Init()
            {
                InitChilds();

                _naviGate.SetNaviGate(true);
                _naviGate.SetNaviNodeFocusAt(0);

                return this;
            }

            public override Node Update(GameTime gameTime)
            {
                if (_naviGate.IsNaviGate)
                {
                    Game1.UpdateNaviGateButton(_naviGate, true);

                    if (Input.Button.OnPress("back")) this["back"].This<Gui.Button>().RunMessageProc(new Gui.Message { _node = this, _message = "ON_PRESS" });

                    if (_naviGate.OnSubmit())
                    {
                        _naviGate.MoveNaviGateToFocusedChild();
                        Game1._sound_click.Play(.5f, .05f, 0);
                    }

                    if (_naviGate.OnChangeFocus && !Retro2D.Screen.IsTransition())
                    {
                        Game1._sound_click.Play(.5f, .05f, 0);
                    }

                }


                UpdateChilds(gameTime);

                return this;
            }

            public override Node Render(SpriteBatch batch)
            {

                //float relMouseX = GetGame<Game1>()._relMouseX;
                //float relMouseY = GetGame<Game1>()._relMouseY;

                batch.GraphicsDevice.Clear(new Color(10, 20, 30));

                Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, 16, 16, Color.CadetBlue * 0.4f);

                RenderChilds(batch);

                

                //Draw.Grid(batch, 0, 0, screenW, screenH, 24, 24, Color.Moccasin * 0.6f);
                //batch.DrawRectangle(selectRect, Color.MonoGameOrange, 1);
                //Draw.Sight(batch, relMouseX, relMouseY, screenW, screenH, Color.RoyalBlue, 1);

                Draw.TopCenterString(batch, Game1._font_Big, "Options", (int)_rect.Width / 2, 2, Color.MonoGameOrange);

                return this;
            }
        }

    }
}
