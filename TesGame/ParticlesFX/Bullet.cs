﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System;

namespace ArenaTotalWar
{
    class Bullet : Node
    {
        public const int FIRE = 0;
        public const int FIRE2 = 1;
        public const int BLUE = 2;

        int _mode = BLUE;
        int _power = 40;

        float _ax;
        float _ay;

        float _aax;
        float _aay;

        float _vx;
        float _vy;

        float _angle;

        int _tempoLife = 60;

        Sprite _sprite;

        public Bullet(float x, float y, float vx, float vy, int mode = BLUE, int power = 40) 
        {

            _type = UID.Get<Bullet>();

            SetPosition(x,y);

            _mode = mode;
            _power = power;

            _vx = vx;
            _vy = vy;

            _angle = Geo.GetRadian(new Vector2(), new Vector2(_vx, _vy));

            _aax = _vx / 20;
            _aay = _vy / 20;
            
            SetSize(4, 4);
            SetPivot(Position.CENTER);

            SetCollideZone(0, _rect);

            _sprite = new Sprite();
            _sprite.Add(Game1._animation_bulletFire);
            _sprite.Add(Game1._animation_bulletFire2);
            _sprite.Add(Game1._animation_bulletBlue);

            if (_mode == FIRE) _sprite.Start("ShootFire", 1, 0);
            if (_mode == FIRE2) _sprite.Start("ShootFire2", 1, 0);
            if (_mode == BLUE) _sprite.Start("ShootBlue", 1, 0);

        }

        public override Node Update(GameTime gameTime)
        {
            _sprite.Update();

            UpdateRect();
            UpdateCollideZone(0, _rect);

            _z = -(int)_y - 32; // Explosion is over everything on the same line !

            var collideZone = Collision2D.OnCollideZoneByNodeType
            (
                GetCollideZone(0),
                new int[]
                {
                    UID.Get<MapDecor>(),
                    UID.Get<MapBlock>(),
                    UID.Get<Enemy>(),
                    UID.Get<EnemyOmega>(),
                    UID.Get<Bomb>(),
                    UID.Get<Block>()
                },
                new int[]
                {
                    MapDecor.ZONE_BODY,
                    MapBlock.ZONE_BODY,
                    Enemy.ZONE_BODY,
                    EnemyOmega.ZONE_BODY,
                    1,
                    Block.ZONE_BODY
                }

            );

            if (null != collideZone) // Hit only one time !
            {
                Node collider = collideZone._node;

                if (null != collider)
                {
                    float strength = collider.This<MapObject>()._strength;
                    //if (collider._type == UID.Get<Hero>())
                    //{
                    //    collider.This<Hero>().AddEnergy(-10);
                    //    new PopInfo("-10", Color.Red)
                    //        .SetPosition(_x, _y - 32)
                    //        .AppendTo(_parent);

                    //    KillMe();

                    //}
                    if (collider._type == UID.Get<Bomb>()) collider.This<Bomb>().Explose();
                    if (collider._type == UID.Get<Block>())
                    {
                        collider.This<Block>().AddEnergy(-_power);
                        collider.This<Block>().Shake(2, .1f);
                    }
                    //if (collider._type == UID.Get<ItemCoin>()) collider.This<ItemCoin>().Remove();
                    //if (collider._type == UID.Get<ItemPower>()) collider.This<ItemPower>().Remove();
                    //if (collider._type == UID.Get<ItemLife>()) collider.This<ItemLife>().Remove();

                    if (collider._type == UID.Get<MapDecor>())
                    {
                        //hitWood = true;
                        //hitIronWood = true;
                        collider.This<MapDecor>().Shake(strength / 10, .1f);
                        collider.This<MapDecor>().HitDamage(-_power);

                        //new Smoke(impactPos.X, impactPos.Y, 16, false, 32, 16)
                        //    .AppendTo(_parent);
                    }

                    if (collider._type == UID.Get<Enemy>())
                    {
                        collider.This<Enemy>().AddEnergy(-_power);
                        collider.This<Enemy>().HitMe(new Vector2(_vx, _vy) , .2f, 16);

                        new PopInfo("-40", Color.Red)
                            .SetPosition(_x, _y - 32)
                            .AppendTo(_parent);

                        new Smoke(_x, _y, 16)
                            .AppendTo(_parent);

                        Game1._sound_slime.Play(.2f, 1f, 0);

                        KillMe();
                    }

                    if (collider._type == UID.Get<EnemyOmega>())
                    {
                        collider.This<EnemyOmega>().AddEnergy(-_power);
                        collider.This<EnemyOmega>().HitMe(new Vector2(_vx, _vy), .2f, 16);

                        new PopInfo("-40", Color.Red)
                            .SetPosition(_x, _y - 32)
                            .AppendTo(_parent);

                        new Smoke(_x, _y, 16)
                            .AppendTo(_parent);

                        Game1._sound_slime.Play(.2f, 1f, 0);

                        KillMe();
                    }


                    new Smoke(_x, _y, 16)
                        .AppendTo(_parent);

                    Game1._sound_killBlock.Play(.2f, .5f, 0);

                    KillMe();

                }

            }

            _ax += _aax;
            _ay += _aay;

            if (Math.Abs(_ax) > Math.Abs(_vx)) _ax = _vx;
            if (Math.Abs(_ay) > Math.Abs(_vy)) _ay = _vy;

            _x += _ax;
            _y += _ay;

            // destroy bullet if out of arena
            //if (_x < 0 || _x > _parent._rect.Width || 
            //    _y < 0 || _y > _parent._rect.Height)
            //    KillMe();

            --_tempoLife;
            if (_tempoLife <= 0)
                KillMe();


            return this;
        }

        public override Node Render(SpriteBatch batch)
        {

            //batch.DrawCircle(AbsX(), AbsY(), 6, 4, Color.Red, 1);
            //batch.DrawPoint(new Vector2(AbsX(), AbsY()), Color.Yellow, 3);

            batch.Draw(Game1._tex_shadow, new Rectangle((int)(AbsX - _oX), AbsY + 8, 16 - 8, 4), Color.Black * .2f); // Shadow

            _sprite.Draw(batch, AbsX, AbsY, Color.White, 1 , 1, _angle);

            //batch.Draw(Game1._tex_heroItems,
            //    new Vector2(AbsX()-4, AbsY()-4),
            //    new Rectangle(32, 0, 8, 8), Color.White);

            //batch.DrawRectangle(AbsRect(), Color.Red, 2);

            //batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua, 1);

            return this;
        }

    }
}
