﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    class ExplosionWave : MapObject
    {
        Position _direction = Position.M;

        //Point _impact = new Point();
        Point _waveDirection = new Point();

        int _tick = 0;
        int _timer = 20; // timer for next explosion

        int _wave = 0; 
        int _wavePower = 3; // power of the wave

        public ExplosionWave(Map2D<Tile> map2D, Position direction, int power = 1, int timer = 8) : base(map2D)
        {
            _direction = direction;
            _wavePower = power;
            _timer = timer;
        }
        void SetWaveDirection(int x, int y)
        {
            _waveDirection.X = x;
            _waveDirection.Y = y;
        }

        public override Node Update(GameTime gameTime)
        {
            if (_direction == Position.UP) SetWaveDirection(0, -1);
            if (_direction == Position.DOWN) SetWaveDirection(0, 1);
            if (_direction == Position.LEFT) SetWaveDirection(-1, 0);
            if (_direction == Position.RIGHT) SetWaveDirection(1, 0);

            _tick++;

            if (_tick > _timer)
            {
                _tick = 0;

                // create new explosion to new position
                Explosion.Add(_map2D, _parent, _mapPosition.X + _waveDirection.X * _wave, _mapPosition.Y + _waveDirection.Y * _wave);

                // check what explosion touch 
                Tile tile;

                tile = _map2D.Get(_mapPosition.X + _waveDirection.X * (_wave + 1), _mapPosition.Y + _waveDirection.Y * (_wave + 1));

                if (null != tile)
                {
                    if (tile._type == UID.Get<MapBlock>()) // check if block on the way
                        KillMe();

                }
                // check what explosion touch 
                tile = _map2D.Get(_mapPosition.X + _waveDirection.X * _wave , _mapPosition.Y + _waveDirection.Y * _wave);

                if (null != tile)
                {
                    if (tile._type == UID.Get<Bomb>() ||
                        tile._type == UID.Get<Block>() ||
                        tile._type == UID.Get<ItemCoin>() ||
                        tile._type == UID.Get<ItemPower>() ||
                        tile._type == UID.Get<ItemLife>() )
                    {
                        KillMe(); // Stop the wave here !
                    }

                }

                _wave++;
                if (_wave > _wavePower) // end of wave explosion
                {
                    KillMe();
                }

            }

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {

            return this;
        }
    }
}
