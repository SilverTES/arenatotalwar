﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    public class PopInfo : Node
    {
        string _label;
        Color _color;

        public PopInfo(string label, Color color, float start = 0, float end = 16, float duration = 32)
        {
            _label = label;
            _color = color;

            _animate.Add("popup", Easing.BackEaseInOut, new Tweening(start, end, duration));
            _animate.Start("popup");

            _z = -1000; // Over all Node Childs
        }

        public override Node Update(GameTime gameTime)
        {
            UpdateRect();

            if (_animate.OnEnd("popup"))
            {
                KillMe();
            }

            _animate.NextFrame();


            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {

            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX()-1, AbsY() - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX()+1, AbsY() - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX(), AbsY()-1 - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX(), AbsY()+1 - _animate.Value(), Color.Black);

            Draw.TopCenterBorderedString(batch, Game1._font_Main, _label, AbsX, AbsY - _animate.Value(), _color, Color.Black);

            return base.Render(batch);
        }
    }
}
