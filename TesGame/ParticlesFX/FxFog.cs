﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;


namespace ArenaTotalWar
{
    class FxFog
    {
        class Fog
        {
            float _width = 128;
            float _height = 128;

            Vector2 _pos = new Vector2();
            Vector2 _dir = new Vector2();

            Rectangle _viewRect;

            public Fog(float x, float y, float dx, float dy, Rectangle viewRect)
            {
                _pos.X = x;
                _pos.Y = y;
                _dir.X = dx*(float)(0.1+ Misc.Rng.NextDouble());
                _dir.Y = dy*(float)(0.1+ Misc.Rng.NextDouble());
                _viewRect = viewRect;

                _width = Misc.Rng.Next(64, 256);
                _height = Misc.Rng.Next(64, 128);
            }

            public void Update()
            {
                _pos.X += _dir.X;
                _pos.Y += _dir.Y;

                if (_pos.X > _viewRect.X + _viewRect.Width && _dir.X > 0) _pos.X = -_viewRect.Width;
                if (_pos.X < -_viewRect.X -_viewRect.Width && _dir.X < 0) _pos.X = _viewRect.X + _viewRect.Width;
                
            }

            public void Render(SpriteBatch batch, Color color)
            {
                batch.Draw(Game1._tex_fog, new Rectangle((int)_pos.X, (int)_pos.Y, (int)_width, (int)_height), color);
            }

        }

        Rectangle _viewRect;

        List<Fog> _fogs = new List<Fog>();

        public FxFog(int nbFog, Rectangle viewRect)
        {
            _viewRect = viewRect;

            for (int i=0; i<nbFog;i++)
            {
                float x = Misc.Rng.Next(_viewRect.X - _viewRect.Width, _viewRect.X + _viewRect.Width);
                float y = Misc.Rng.Next(_viewRect.Y - _viewRect.Width, _viewRect.Y + _viewRect.Height);

                Fog fog = new Fog(x, y, 1, 0, _viewRect);

                _fogs.Add(fog);
            }

        }

        public void Update()
        {
            for (int i=0; i<_fogs.Count; i++)
            {
                _fogs[i].Update();
            }
        }

        public void Render(SpriteBatch batch, Color color)
        {
            for (int i = 0; i < _fogs.Count; i++)
            {
                _fogs[i].Render(batch, color);
            }
            
        }

    }
}
