﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace ArenaTotalWar
{
    class Explosion : MapObject
    {

        Sprite _sprite = new Sprite();

        public Explosion(Map2D<Tile> map2D, int mapX, int mapY) : base(map2D)
        {
            SetSize(32, 32);
            SetPivot(16, 24);

            _sprite.Add(Game1._animation_explosion);

            _sprite.Start("Explosion", 1, 0);

            //map2D.Get(mapX, mapY)._type = _type;
            GoTo(mapX, mapY);

            SetCollideZone(0, _rect);
        }

        public static bool Add(Map2D<Tile> map2D, Node parent, int mapX, int mapY)
        {
            if (null != map2D.Get(mapX, mapY))
            {
                //if (map2D.Get(mapX, mapY)._type != UID.Get<MapBlock>()) // if Tile contain Nlock do nothing !
                //    return false;

                new Explosion(map2D, mapX, mapY).AppendTo(parent);

                return true;
            }
            return false;
        }

        public override Node Update(GameTime gameTime)
        {
            UpdateRect();
            UpdateCollideZone(0, Gfx.AddRect(_rect,new Rectangle(5,10,-10,-20)));

            _z = -(int)_y - 10; // Explosion is over everything on the same line !

            _sprite.Update();

            if (_sprite.OffPlay)
                KillMe();

            var listCollideZone = Collision2D.ListCollideZoneByNodeType
            (   
                GetCollideZone(0), 
                new int[] 
                {
                    UID.Get<MapBlock>(),
                    UID.Get<MapDecor>(),
                    UID.Get<Hero>(),
                    UID.Get<Enemy>(),
                    UID.Get<Bomb>(),
                    UID.Get<ItemCoin>(),
                    UID.Get<ItemLife>(),
                    UID.Get<ItemPower>(),
                    UID.Get<Block>()
                }, 
                0
            );

            if (listCollideZone.Count > 0 && _sprite.CurFrame == 1 && _sprite.CurDuration == 0) // Hit only one time !
            {

                //bool hitMetal = false;
                //bool hitOrganic = false;
                //bool hitWood = false;
                //bool hitIronWood = false;
                //bool hitGlass = false;

                for (int index = 0; index < listCollideZone.Count; index++)
                {
                    Node collider = listCollideZone[index]._node;

                    if (null != collider)
                    {
                        if (collider._type == UID.Get<Hero>())
                        {
                            collider.This<Hero>().AddEnergy(-200);
                            new PopInfo("-100", Color.Red)
                                .SetPosition(_x, _y - 32)
                                .AppendTo(_parent);
                        }
                        if (collider._type == UID.Get<Enemy>())
                        {
                            collider.This<Enemy>().AddEnergy(-200);
                            new PopInfo("-100", Color.Red)
                                .SetPosition(_x, _y - 32)
                                .AppendTo(_parent);
                        }

                        if (collider._type == UID.Get<Block>())
                        {
                            collider.This<Block>().AddEnergy(-200);
                        }

                        if (collider._type == UID.Get<MapDecor>())
                        {
                            collider.This<MapDecor>().HitDamage(-200);
                            collider.This<MapDecor>().Shake(4,.1f);
                        }

                        //if (collider._type == UID.Get<Block>()) collider.This<Block>().Remove();

                        if (collider._type == UID.Get<Bomb>())  collider.This<Bomb>().Explose();
                        if (collider._type == UID.Get<ItemCoin>()) collider.This<ItemCoin>().Remove();
                        if (collider._type == UID.Get<ItemPower>()) collider.This<ItemPower>().Remove();
                        if (collider._type == UID.Get<ItemLife>()) collider.This<ItemLife>().Remove();


                    }
                }

                //if (hitMetal) Game1._sound_swordHit.Play(.1f, .01f, 0);
                //if (hitOrganic) Game1._sound_punch.Play(.1f, .01f, 0);
                //if (hitWood) Game1._sound_hitWood.Play(.4f, 1f, 0);
                //if (hitIronWood) Game1._sound_hitIronWood.Play(.4f, 1f, 0);
                //if (hitGlass) Game1._sound_hitGlass.Play(.1f, .1f, 0);

            }
            return this;
        }

        public override Node Render(SpriteBatch batch)
        {

            _sprite.Draw(batch, AbsX, AbsY, Color.White);
            //batch.DrawCircle(AbsX(), AbsY() - 12, 8, 16, Color.Red, 4);

            //batch.DrawRectangle(AbsRect(), Color.Red, 2);

            //batch.DrawRectangle(GetCollideZone(0)._rect, Color.Aqua, 1);

            return this;
        }

    }
}
