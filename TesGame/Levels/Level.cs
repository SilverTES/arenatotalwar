﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;

using Retro2D;

namespace ArenaTotalWar
{

    public class Level : Node
    {
        #region Attributes

        public TileMap2D _map2D;
        protected TileMapLayer _layer0;
        protected TileMapLayer _layer1;
        protected Map2D<Tile> _mapObject;

        protected static TileMap _tileA = new TileMap(0, 44, new Rectangle(512, 64, 64, 64), true);
        protected static TileMap _tilePath = new TileMap(1, 88, new Rectangle(768, 384, 64, 64), false);
        protected static TileMap _tileC = new TileMap(2, UID.Get<Tower.Case>(), new Rectangle(704, 384, 32, 32), false); // Gray Floor 
        protected static TileMap _tileB = new TileMap(1, 88, new Rectangle(640, 384, 64, 64), false); // Green Floor

        public const int TileSizeW = 32;
        public const int TileSizeH = 32;

        // Size of the player view, use for splitscreen !
        public int ViewW { get; private set; }
        public int ViewH { get; private set; }

        protected int _arenaSizeW;
        protected int _arenaSizeH;

        protected int _arenaMapW;
        protected int _arenaMapH;

        public bool IsStartWave { get; protected set; }

        public bool IsWin { get; protected set; }
        public bool IsLose { get; protected set; }


        // View for each player
        protected static Rectangle _rectViewSrc;
        protected static Rectangle _rectViewDest;

        // View of the player in game Camera & ViewZone 
        protected Camera[] _camera = new Camera[Game1.MAX_PLAYER];
        protected ViewZone[] _viewZone = new ViewZone[Game1.MAX_PLAYER];

        protected Vector2 _middlePlayer = new Vector2();

        // Extreme position of players !
        protected float
            _minX = float.MaxValue,
            _minY = float.MaxValue,
            _maxX = float.MinValue,
            _maxY = float.MinValue;

        // Song List !
        public List<Song> _listSong = new List<Song>();
        protected int _currentSongIndex = 0;

        public Song NextSong()
        {
            _currentSongIndex++;
            if (_currentSongIndex > _listSong.Count - 1) _currentSongIndex = 0;

            return _listSong[_currentSongIndex];
        }
        public void PlaySong(bool repeat = false)
        {
            MediaPlayer.Play(_listSong[_currentSongIndex]);
            MediaPlayer.IsRepeating = repeat;
        }

        #endregion

        public void AddPathCase(int mapX, int mapY, bool killObject = true)
        {
            _layer0._map2D.Put(mapX, mapY, _tilePath);
            MapObject.Remove(_mapObject, this, mapX, mapY, killObject);
            _mapObject.Put(mapX, mapY, new Tile() { _subType = UID.Get<Portal>() });

        }

        public void AddTowerCase(int mapX, int mapY)
        {
            _layer0._map2D.Put(mapX, mapY, _tileC);
            MapObject.Remove(_mapObject, this, mapX, mapY, true);
            _mapObject.Put(mapX, mapY, new Tile() { _subType = UID.Get<Tower.Case>() });

        }

        public int GetArenaSizeW() { return _arenaSizeW; }
        public int GetArenaSizeH() { return _arenaSizeH; }

        public int GetArenaMapW() { return _arenaMapW; }
        public int GetArenaMapH() { return _arenaMapH; }

        public void UpdateRectView(RectangleF rectView) // Update the level rectView, hide object out of level view !
        {
            SetRectView(Gfx.AddRect(rectView, new Rectangle(-128, -128, 256, 256))); // Set Camera RectView
        }

        public Level()
        {
            // Set Default View
            ViewW = Game1._screenW / 2;
            ViewH = Game1._screenH / 2;

            // Camera : Create & Setting !
            for (int i = 0; i < Game1.MAX_PLAYER; ++i)
            {
                _camera[i] = new Camera();
            }

            // 1 Player View Frame
            if ((int)PlayerATW.GetNbPlayers() < 2)
            {
                // Set Rect view Source & Destination
                _rectViewSrc = new Rectangle(0, 0, ViewW + 384, ViewH + 216);

                _camera[Game1.PLAYER1].SetView(_rectViewSrc);
                _camera[Game1.PLAYER1].SetZone(new RectangleF(0, 0, _rectViewSrc.Width / 6, _rectViewSrc.Height / 6));
                _viewZone[Game1.PLAYER1] = new ViewZone(Game1._window, _rectViewSrc, new Rectangle(0, 0, ViewW * 2, ViewH * 2));

                UpdateRectView(_rectViewSrc);
            }
            // 2 Players View Frame
            else if ((int)PlayerATW.GetNbPlayers() == 2)
            {
                int spaceX = 80;
                int spaceY = 48;

                // Set Rect view Source & Destination
                _rectViewSrc = new Rectangle(0, 0, ViewW + spaceX, ViewH + spaceY);

                _camera[Game1.PLAYER1].SetView(_rectViewSrc);
                _camera[Game1.PLAYER2].SetView(_rectViewSrc);

                _camera[Game1.PLAYER1].SetZone(new RectangleF(0, 0, _rectViewSrc.Width / 6, _rectViewSrc.Height / 6));
                _camera[Game1.PLAYER2].SetZone(new RectangleF(0, 0, _rectViewSrc.Width / 6, _rectViewSrc.Height / 6));

                _viewZone[Game1.PLAYER1] = new ViewZone(Game1._window, _rectViewSrc, new Rectangle(0, 0, ViewW + spaceX, ViewH + spaceY));
                _viewZone[Game1.PLAYER2] = new ViewZone(Game1._window, _rectViewSrc, new Rectangle(ViewW - spaceX, ViewH - spaceY, ViewW + spaceX, ViewH + spaceY));

                UpdateRectView(_rectViewSrc);
            }
            // 3 Players View Frame
            else if ((int)PlayerATW.GetNbPlayers() == 3)
            {

                // Set Rect view Source & Destination
                _rectViewSrc = new Rectangle(0, 0, ViewW, ViewH);
                _rectViewDest = new Rectangle(0, 0, ViewW, ViewH);

                // Camera : Create & Setting !
                for (int i = 0; i < 3; ++i)
                {
                    _camera[i].SetView(_rectViewSrc);
                    _camera[i].SetZone(new RectangleF(0, 0, _rectViewSrc.Width / 6, _rectViewSrc.Height / 6));
                }

                _viewZone[Game1.PLAYER1] = new ViewZone(Game1._window, _rectViewSrc, Gfx.TranslateRect(_rectViewDest, new Point(0, 0)));
                _viewZone[Game1.PLAYER2] = new ViewZone(Game1._window, _rectViewSrc, Gfx.TranslateRect(_rectViewDest, new Point(ViewW, 0)));
                _viewZone[Game1.PLAYER3] = new ViewZone(Game1._window, _rectViewSrc, Gfx.TranslateRect(_rectViewDest, new Point(0, ViewH)));

                UpdateRectView(_rectViewSrc);
            }
            // 4 Players View Frame
            else
            {
                // Set Rect view Source & Destination
                _rectViewSrc = new Rectangle(0, 0, ViewW, ViewH);
                _rectViewDest = new Rectangle(0, 0, ViewW, ViewH);

                // Camera : Create & Setting !
                for (int i = 0; i < 4; ++i)
                {
                    _camera[i].SetView(_rectViewSrc);
                    _camera[i].SetZone(new RectangleF(0, 0, _rectViewSrc.Width / 6, _rectViewSrc.Height / 6));
                }

                // ViewZone for the each player view
                _viewZone[Game1.PLAYER1] = new ViewZone(Game1._window, _rectViewSrc, _rectViewDest);
                _viewZone[Game1.PLAYER2] = new ViewZone(Game1._window, _rectViewSrc, Gfx.TranslateRect(_rectViewDest, new Point(ViewW, 0)));
                _viewZone[Game1.PLAYER3] = new ViewZone(Game1._window, _rectViewSrc, Gfx.TranslateRect(_rectViewDest, new Point(0, ViewH)));
                _viewZone[Game1.PLAYER4] = new ViewZone(Game1._window, _rectViewSrc, Gfx.TranslateRect(_rectViewDest, new Point(ViewW, ViewH)));

                UpdateRectView(_rectViewSrc); // Update the level rectView, hide object out of level view !
            }

        }

        public override Node Init()
        {

            KillAll();

            IsStartWave = false;

            IsWin = false;
            IsLose = false;

            // Reset Bank !
            PlayerATW.SetBankPoint(2400);

            return this;
        }

        public override Node Update(GameTime gameTime)
        {

            // Get the extreme players position for set the camera !
            //_minX = float.MaxValue;
            //_minY = float.MaxValue;
            //_maxX = float.MinValue;
            //_maxY = float.MinValue;

            //for (int i = 0; i < Game1.MAX_PLAYER; i++)
            //{
            //    if (null != Game1._heros[i])
            //    {
            //        if (!Game1._heros[i].This<Hero>().IsDead())
            //        {
            //            if (Game1._heros[i]._x < _minX) _minX = Game1._heros[i]._x;
            //            if (Game1._heros[i]._y < _minY) _minY = Game1._heros[i]._y;
            //            if (Game1._heros[i]._x > _maxX) _maxX = Game1._heros[i]._x;
            //            if (Game1._heros[i]._y > _maxY) _maxY = Game1._heros[i]._y;
            //        }
            //    }
            //}

            //_middlePlayer.X = _minX + (_maxX - _minX) / 2;
            //_middlePlayer.Y = _minY + (_maxY - _minY) / 2;

            //_camera.Update(_middlePlayer.X, _middlePlayer.Y);
            //SetPosition(_camera.X, _camera.Y);

            //UpdateChilds();

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {

            SortZD();

            for (int i = 0; i < (int)PlayerATW.GetNbPlayers(); ++i)
                if (null != Game1._heros[i])
                    RenderPlayerView(_viewZone[i], _camera[i], Game1._heros[i]._x, Game1._heros[i]._y);

            // Debug : Draw Equidistant Point Between Players

            // Draw Camera Zone
            //batch.DrawRectangle(Gfx.TranslateRect((Rectangle)_camera.Zone, AbsVectorXY()), Color.Red, 1);
            //batch.DrawPoint(_middlePlayer + AbsVectorXY(), Color.Red, 4);

            // Debug : Draw Extreme Player Pos
            //batch.DrawLine(_minX + AbsX(), 0, _minX + AbsX(), Game1._screenH, Color.BlueViolet * .4f, 1);
            //batch.DrawLine(_maxX + AbsX(), 0, _maxX + AbsX(), Game1._screenH, Color.BlueViolet * .4f, 1);
            //batch.DrawLine(0, _minY + AbsY(), Game1._screenW, _minY + AbsY(), Color.BlueViolet * .4f, 1);
            //batch.DrawLine(0, _maxY + AbsY(), Game1._screenW, _maxY + AbsY(), Color.BlueViolet * .4f, 1);

            //Draw.BorderedString(batch, Game1._font_Main, "_rectView = " + _rectView + " Camera : "+ AbsX() + "," + AbsY(), new Vector2(10, 28), Color.Crimson, Color.Black);

            //for (int i = 0; i < Game1.MAX_PLAYER; i++)
            //{
            //    if (null != Game1._heros[i])
            //    {
            //        if (!Game1._heros[i].This<Hero>().IsDead())
            //        {
            //            batch.DrawString(
            //                Game1._font_Main, 
            //                "Player "+ i + " : " + Game1._heros[i].AbsX() + "," + Game1._heros[i].AbsY()+ " InRectView : "+ Game1._heros[i].InRectView(), 
            //                new Vector2(64, 40 + i * 12), Color.GreenYellow);
            //        }
            //    }
            //}

            // Draw Movement Camera
            //batch.DrawPoint(_camera, Color.Red, 8);
            //Draw.BorderedString(batch, Game1._font_Main, " Camera : " + (int)_camera.X + "," + (int)_camera.Y, new Vector2(10, 28), Color.Crimson, Color.Black);
            //Draw.BorderedString(batch, Game1._font_Main, " CameraZone : " + (int)_camera.Zone.X + "," + (int)_camera.Zone.Y, new Vector2(10, 40), Color.Crimson, Color.Black);
            //Draw.BorderedString(batch, Game1._font_Main,
            //    " MiddlePlayer : " + (int)_middlePlayer.X + "," + (int)_middlePlayer.Y, new Vector2(10, 52), Color.Yellow, Color.Black);


            // Draw Level rectView 

            //batch.DrawRectangle(_rectView, Color.Blue, 1);

            return this;
        }

        void RenderPlayerView(ViewZone viewZone, Camera camera, float x, float y)
        {

            camera.Update(x, y);

            SetPosition(camera.X, camera.Y);

            SpriteBatch batch = viewZone.Batch();

            viewZone.BeginRender();

            _map2D.Render(batch, _layer0, Color.White, (int)camera.X, (int)camera.Y);
            _map2D.Render(batch, _layer1, Color.White, (int)camera.X, (int)camera.Y);

            //Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, _tileW, _tileH, Color.Moccasin * 0.05f);

            RenderChilds(batch);

            //batch.DrawRectangle(_selectRect, Color.LightBlue, 1);

            // Debug 
            //if (null != _map2D.GetTile(_layer1, _mouseMapX, _mouseMapY))
            //if (null != _mapObject.Get(_mouseMapX, _mouseMapY))
            //{
            //    Draw.TopCenterString
            //    (
            //        batch,
            //        Game1._font_Main, _mouseMapX + "," + _mouseMapY + ":" + _mapObject.Get(_mouseMapX, _mouseMapY)._type + 
            //        "\n" + _mapObject.Get(_mouseMapX, _mouseMapY)._id +
            //        "\n" + _mapObject.Get(_mouseMapX, _mouseMapY)._subType,
            //        _selectRect.X + _selectRect.Width / 2, _selectRect.Y + 4,
            //        Color.Gold
            //    );
            //}


            //Draw.CenterStringX(batch, Game1._bigFont, "F2 : Play", Game1._screenW / 2, 2, Color.MonoGameOrange);

            // Debug show _layerMapBlock collidable !
            //List<List<Tile>> map = _layerMapBlock.GetMap();
            //for (int i = 0; i < map.Count; i++)
            //{
            //    for (int j = 0; j < map[i].Count; j++)
            //    {
            //        if (map[i][j]._isCollidable)
            //            batch.DrawPoint(i * 32 + 16, j * 32 + 16, Color.BlueViolet, 4);
            //    }
            //    Console.WriteLine();
            //}

            // Debug :
            //Draw.CenterBorderedStringXY(batch,
            //    Game1._font_Main,
            //    " _nbEnemyPop = " + _nbEnemyPop, //+ " : IsWin = " + IsWin + " : IsLose = " + IsLose,
            //    Game1._screenW / 2, 16,
            //    Color.MonoGameOrange,
            //    Color.Black);



            
            //Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, _tileW, _tileH, Color.Moccasin * 0.2f);

            RenderView(batch);

            viewZone.EndRender();

            viewZone.Render();
        }

        public virtual void RenderView(SpriteBatch batch)
        {

        }

        public void StartWave()
        {
            IsStartWave = true;
        }

        public static int GetPosInMap(float x, int tileSize)
        {
            return (int)(x / tileSize) * tileSize;
        }

    }

}