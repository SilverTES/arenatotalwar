﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace ArenaTotalWar
{
    public class LevelSelector : Gui.Base
    {
        int _level;
        Color _color;

        bool _isUnLock = false;
        //float _alpha = 1;

        Node _toNode;

        public LevelSelector(Input.Mouse mouse, Node toNode, int level, Color color) : base(mouse)
        {
            _type = UID.Get<Gui.Base>();
            _class.Add(UID.Get<LevelSelector>());
            _level = level;
            _color = color;

            _toNode = toNode;

            //_animate.Add("popup", Easing.BackEaseInOut, new Tweening(0, 16, 32));
            //_animate.Start("popup");

            _z = -1000; // Over all Node Childs

            SetSize(64, 48);
            SetPivot(32, 8);

            _navi._isFocusable = true;
        }
        public int GetLevel() { return _level; }
        public bool IsUnlock() { return _isUnLock; }
        public LevelSelector SetUnLock(bool isUnLock)
        {
            _isUnLock = isUnLock;
            return this;
        }
        public override Node Update(GameTime gameTime)
        {
            //if (_animate.OnEnd("popup"))
            //{
            //    KillMe();
            //}
            //_animate.NextFrame();

            if (_navi._onPress)
            {
                Data msg = new Data()
                {
                    _type = Data.Type.SELECT_LEVEL,
                    _msg = "Select Level",
                    _value = _level
                };

                Game1._messageQueue.Post((int)msg._type, msg, _toNode, this);
            }

            if (_navi._isFocus)
                _alpha = 1;
            else
            {
                //if (_isUnLock)
                //    _alpha = .8f;
                //else
                    _alpha = .4f;
            }

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            
            batch.Draw(Game1._tex_squareLock, new Vector2(AbsX-_oX+16, AbsY-_oY-16), _isUnLock ?new Rectangle(32,0,32,64): new Rectangle(0, 0, 32, 64), Color.White * _alpha);

            Draw.TopCenterBorderedString(batch, Game1._font_Main, _level.ToString(), AbsX, AbsY, _color, Color.Black);

            return this;
        }
    }
}
