﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ArenaTotalWar
{
    class LevelContainer : Gui.Base
    {

        List<Node> _levels = new List<Node>();
        int _currentLevel = 0;
        //int _lastBottomLevelPosition = 17; // Position when we quit level Selector by press down 

        public LevelContainer(Input.Mouse mouse) : base(mouse)
        {
            _naviGate = new NaviGate(this);

            Style._focusBorderColor._value = Color.Yellow;
            SetFocusable(true);


            int level = 0;
            for (int j = 0; j < 3; j++)
            {
                for (int i = 0; i < 7; i++)
                {
                    Node node = new LevelSelector(Game1._mouse, this, level, Color.Yellow).SetUnLock(false)
                        .AppendTo(this)
                        .SetPosition(40 + i * 80, 32 + j * 96);
                    level++;

                    _levels.Add(node);
                }
            }

            for (int i = 0; i < 6; i++)
            {
                // Left <--> Right
                //_levels[i].SetNaviNode(Position.RIGHT, _levels[i + 1]); _levels[i + 1].SetNaviNode(Position.LEFT, _levels[i]);
                //_levels[i + 7].SetNaviNode(Position.RIGHT, _levels[i + 8]); _levels[i + 8].SetNaviNode(Position.LEFT, _levels[i + 7]);
                //_levels[i + 14].SetNaviNode(Position.RIGHT, _levels[i + 15]); _levels[i + 15].SetNaviNode(Position.LEFT, _levels[i + 14]);

                SetNaviNodeHorizontal(_levels[i], _levels[i + 1]);
                SetNaviNodeHorizontal(_levels[i + 7], _levels[i + 8]);
                SetNaviNodeHorizontal(_levels[i + 14], _levels[i + 15]);


            }

            for (int i = 0; i < 7; i++)
            {
                // Up <--> Down
                //_levels[i].SetNaviNode(Position.DOWN, _levels[i + 7]); _levels[i + 7].SetNaviNode(Position.UP, _levels[i]);
                //_levels[i + 7].SetNaviNode(Position.DOWN, _levels[i + 14]); _levels[i + 14].SetNaviNode(Position.UP, _levels[i + 7]);

                SetNaviNodeVertical(_levels[i], _levels[i + 7]);
                SetNaviNodeVertical(_levels[i + 7], _levels[i + 14]);

                //_levels[i + 14].SetNaviNode(Position.DOWN, this["options"]); // Quit levelSelector if at bottom !
            }


            _levels[0].This<LevelSelector>().SetUnLock(true);


            _naviGate.SetNaviGate(false);
            _naviGate.SetNaviNodeFocusAt(0);
        }

        public override Node Update(GameTime gameTime)
        {
            if (_naviGate.IsNaviGate)
            {
                Game1.UpdateNaviGateButton(_naviGate, true);

                
                if (Input.Button.OnPress("back")) _naviGate.BackNaviGate();

                if (_naviGate.OnSubmit())
                {
                    _naviGate.MoveNaviGateToFocusedChild();
                    Game1._sound_click.Play(.5f, .05f, 0);
                }

                if (_naviGate.OnChangeFocus && !Retro2D.Screen.IsTransition())
                {
                    Game1._sound_click.Play(.5f, .05f, 0);

                    if (_naviGate.FocusNaviNode()._class.Contains(UID.Get<LevelSelector>()))
                        _currentLevel = _naviGate.FocusNaviNode().This<LevelSelector>().GetLevel();
                }

                if (_naviGate.OnBackNaviGate)
                {
                    Game1._sound_click.Play(.5f, .5f, 0);
                }

            }


            UpdateChilds(gameTime);

            if (HasMessage())
            {
                Data data = (Data)_message._data;

                if (data._type == Data.Type.SELECT_LEVEL) // Get Message from levelSelector
                {
                    Console.WriteLine("Select Level :" + data._value);

                    if (_message._from.This<LevelSelector>().IsUnlock()) // if Level is unlocked then Go
                    {
                        //Game1._sound_itemLife.Play(.5f, .05f, 0);

                        // Quit Set NaviGate false;

                        _naviGate.SetNaviGate(false);

                        //Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Play"]).This<Screen.Play>().SetLevel(Game1._nodeLevels[0]).Init(), new Transition.FadeInOut().Init());
                        Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Play"]).This<Screen.Play>().SetLevel(new Level0()).Init(), new Transition.FadeInOut().Init());
                    }
                    else
                    {

                        //Game1._sound_click.Play(.5f, .05f, 0);
                    }
                }

                EndMessage();
            }

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            base.Render(batch);

            RenderChilds(batch);

            if (_naviGate.IsNaviGate)
                Draw.TopCenterBorderedString(batch, Game1._font_Main, "Select Level " + _currentLevel, AbsX, AbsY-_rect.Height/2 - 24, Color.MonoGameOrange, Color.Black);

            return this;
        }

    }
}
