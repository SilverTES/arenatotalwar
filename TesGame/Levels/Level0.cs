﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

using Retro2D;


namespace ArenaTotalWar
{
    class Level0 : Level
    {
        #region Attributes

        const float CAMERA_SPEED = 1f;

        FxFog _fxFog;

        // Collision Grid
        Collision2DGrid _collision2DGrid;

        List<Point> _enemyPopPoint = new List<Point>();

        int _tickEnemyPop = 0;
        int _tempoEnemyPop = 80;

        public int _nbEnemyPop = 0;

        int _maxEnemy = 640;

        RectangleF _selectRect = new RectangleF(0, 0, TileSizeW + 1, TileSizeH + 1);

        int _mouseMapX;
        int _mouseMapY;

        Portal _portal;

        #endregion 

        public Level0() : base()
        {
            
            // Level Playlist 
            _listSong.Add(Game1._song_epic);
            _listSong.Add(Game1._song_wave);

            // Create NaviGate 
            _naviGate = new NaviGate(this);

            // Set Arena Size 
            _arenaSizeW = 64 * TileSizeW;
            _arenaSizeH = 36 * TileSizeH;
            _arenaMapW = _arenaSizeW / TileSizeW;
            _arenaMapH = _arenaSizeH / TileSizeH;

            // Create TileMap2D
            _map2D = new TileMap2D();
            _map2D.Setup(_rectViewSrc, _arenaMapW, _arenaMapH, TileSizeW, TileSizeH, 0, 0);
            _map2D.SetMapSize(_arenaMapW, _arenaMapH);

            // Create TileMap2D Layers
            _layer0 = _map2D.CreateLayer(Game1._tex_sokoban);
            _layer1 = _map2D.CreateLayer(Game1._tex_sokoban);

            // Create Object Layer
            _mapObject = new Map2D<Tile>(_arenaMapW, _arenaMapH, TileSizeW, TileSizeH);

            // Camera Setting !
            for (int i = 0; i < Game1.MAX_PLAYER; ++i)
            {
                _camera[i].SetLimit(new RectangleF(0, 0, _arenaSizeW, _arenaSizeH));
                _camera[i].SetPosition(new Vector2(_arenaSizeW / 2 + TileSizeW / 2, _arenaSizeH / 2 + TileSizeH * 2));
            }

            // Create CollisionGrid
            _collision2DGrid = new Collision2DGrid(_arenaSizeW / 64, _arenaSizeH / 64, 64); // --- VERY IMPORTANT FOR PRECISE COLLISION !!

            // Create Fog 
            _fxFog = new FxFog(16, _rectViewSrc);

            // Box Creation !
            #region Box Mission Start
            this["box"] = new MessageBox(Game1._mouse, 0, 60, 12)
                //.AppendTo(this)
                .This<Gui.Base>().SetStyle(Game1._style_main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(320, 320)
                .This<MessageBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS")
                        System.Console.WriteLine("dialbox NAVIGATE ON SUBMIT");
                });

            new Gui.Button(Game1._mouse)
                .AppendTo(this["box"])
                .SetSize(128, 16)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(280)
                .This<Gui.Button>().SetLabel("Accept Mission")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS")
                        Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this["box"]);
                })
                .This<Gui.Button>().SetStyle(Game1._style_main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Label(Game1._mouse)
                .AppendTo(this["box"])
                .SetSize(80, 8)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(80)
                .This<Gui.Base>().SetFocusable(false)
                .This<Gui.Label>().SetStyle(Game1._style_main)
                .This<Gui.Label>().SetLabel
                (
                    "---= MISSION OBJECTIVE =---\n" +
                    "\n" +
                    "\n" +
                    "\n- Defend the portal."+
                    "\n" +
                    "\n- Kill all enemies."+
                    "\n" +
                    "\n- Survive the 12 enemy waves."
                );
            #endregion
        }

        public override Node Init()
        {
            base.Init();

            SetSize(_arenaSizeW, _arenaSizeH);

            _layer0._map2D.FillObject2D(_tileB);

            // Reset MapObject
            _mapObject.FillObject2D<Tile>();

            //for (int i = 0; i < _gameMapW; i++)
            //{
            //    //_map2D.SetTile(_layer1, i, 0, _tileA);
            //    //_map2D.SetTile(_layer1, i, _gameMapH - 1, _tileA);

            //    MapBlock.Add(_mapObject, this, i, 0);
            //    MapBlock.Add(_mapObject, this, i, _gameMapH - 1);

            //}

            //for (int j = 0; j < _gameMapH; j++)
            //{
            //    //_map2D.SetTile(_layer1, 0, j, _tileA);
            //    //_map2D.SetTile(_layer1, _gameMapW - 1, j, _tileA);

            //    MapBlock.Add(_mapObject, this, 0, j);
            //    //MapBlock.Add(_mapObject, this, _gameMapW - 1, j);
            //}

            for (int i = 2; i < _arenaMapW - 4; i += 4)
            {
                for (int j = 2; j < _arenaMapH - 2; j += 4)
                {
                    //_map2D.SetTile(_layer1, i, j, _tileA);
                    if (Misc.Rng.Next(0, 10) > 5)
                        MapBlock.Add(_mapObject, this, i, j);
                    else
                        AddTowerCase(i, j);
                }
            }

            for (int b = 0; b < 240; b++)
            {
                bool find = false;

                while (!find)
                {
                    int bx = Misc.Rng.Next(2, _arenaMapW - 2);
                    int by = Misc.Rng.Next(2, _arenaMapH - 2);

                    find = Block.Add(_mapObject, this, bx, by);
                }
            }
            //for (int b = 0; b < 80; b++)
            //{
            //    bool find = false;

            //    while (!find)
            //    {
            //        int bx = Misc.Rng.Next(2, _arenaMapW - 2);
            //        int by = Misc.Rng.Next(2, _arenaMapH - 2);

            //        find = ItemCoin.Add(_mapObject, this, bx, by);
            //    }
            //}

            //MapDecor.Add(_mapObject, this, 0, 0);
            //MapDecor.Add(_mapObject, this, _arenaMapW - 1 , 0);
            //MapDecor.Add(_mapObject, this, _arenaMapW - 1, _arenaMapH - 1);
            //MapDecor.Add(_mapObject, this, 0, 14);

            //MapDecor.Add(_mapObject, this, _arenaMapW/2, 0);
            //MapDecor.Add(_mapObject, this, 8, 6);
            //MapDecor.Add(_mapObject, this, 20, 6);
            //MapDecor.Add(_mapObject, this, _arenaMapW/2, _arenaMapH - 1);

            for (int b = 0; b < 20; b++)
            {
                MapDecor find = null;

                while (null == find)
                {
                    int bx = Misc.Rng.Next(2, _arenaMapW - 2);
                    int by = Misc.Rng.Next(2, _arenaMapH - 2);

                    find = MapDecor.Add(_mapObject, this, bx, by);
                }
            }

            // Portal Position
            int px = _arenaMapW / 2;
            int py = _arenaMapH / 2;


            _portal = Portal.Add(_mapObject, this, px, py);
            //System.Console.WriteLine(" _portal : {0}, {1}", _portal._mapPosition.X, _portal._mapPosition.Y);

            int minY = 0;
            int centerY = _arenaMapH / 2;
            int maxY = _arenaMapH - 1;
            int minX = 0;
            int centerX = _arenaMapW / 2;
            int maxX = _arenaMapW - 1;


            _enemyPopPoint.Add(new Point(minX, minY));
            _enemyPopPoint.Add(new Point(centerX, minY));
            _enemyPopPoint.Add(new Point(maxX, minY));

            _enemyPopPoint.Add(new Point(minX, maxY));
            _enemyPopPoint.Add(new Point(centerX, maxY));
            _enemyPopPoint.Add(new Point(maxX, maxY));

            _enemyPopPoint.Add(new Point(minX, centerY));
            _enemyPopPoint.Add(new Point(maxX, centerY));

            SortZD();
            UpdateChilds(new GameTime());

            MediaPlayer.Play(Game1._song_start); // start level music !
            MediaPlayer.Volume = 0.2f;
            MediaPlayer.IsRepeating = true;


            bool player1 = (int)PlayerATW.GetNbPlayers() > 0;
            bool player2 = (int)PlayerATW.GetNbPlayers() > 1;
            bool player3 = (int)PlayerATW.GetNbPlayers() > 2;
            bool player4 = (int)PlayerATW.GetNbPlayers() > 3;

            //System.Console.WriteLine(" Pop players at level0 !");
            Game1.PopPlayers(this, _mapObject, _portal._mapPosition.X, _portal._mapPosition.Y+1, player1, player2, player3, player4);
            //Shake(0, 0);




            return this;
        }

        public Map2D<Tile> GetLayerMapObject()
        {
            return _mapObject;
        }

        public override Node Update(GameTime gameTime)
        {

            //base.Update();

            // All Inputs !

            if (Input.Button.OnePress("Shake", Keyboard.GetState().IsKeyDown(Keys.RightControl) || Game1.Player1.GetButton((int)SNES.BUTTONS.SELECT)!=0))
            {
                Game1.Shake(16, .2f);

                StartWave();
                System.Console.WriteLine("Start Wave !");
                PlaySong(); // start wave music !
                //Debug : 
                _nbEnemyPop = 0;

            }

            if (MediaPlayer.State != MediaState.Playing && !MediaPlayer.IsRepeating) // if Combat Phase Play List<Song> !
            {
                //start playing new song
                System.Console.WriteLine("Start PLay New Song !");
                MediaPlayer.Play(NextSong());
            }




            // When Level Win
            if (_nbEnemyPop > _maxEnemy && !IsWin)
            {
                _nbEnemyPop = 0;
                IsWin = true;

                MediaPlayer.Stop();
            }
            // When Level Lose
            if (_portal.IsDead() && !IsLose)
            {
                _nbEnemyPop = 0;
                IsLose = true;

                MediaPlayer.Stop();
            }

            UpdateRect();

            #region Debug Control

            _selectRect.X = GetPosInMap(Game1._relMouseX, TileSizeW);
            _selectRect.Y = GetPosInMap(Game1._relMouseY, TileSizeH);

            _mouseMapX = (int)(_selectRect.X / TileSizeW);
            _mouseMapY = (int)(_selectRect.Y / TileSizeH);

            if (Game1._mouseState.LeftButton == ButtonState.Pressed)
            {
                if (!Keyboard.GetState().IsKeyDown(Keys.LeftControl))
                {
                    if (ItemCoin.Add(_mapObject, this, _mouseMapX, _mouseMapY))
                    {
                        new PopInfo("+10 Points", Color.Coral)
                            .SetPosition(Game1._relMouseX, Game1._relMouseY)
                            .AppendTo(this);

                        Game1._sound_coin.Play(0.1f, 0.1f, 0);
                    }
                }
                else
                {
                    Explosion.Add(_mapObject, this, _mouseMapX, _mouseMapY);
                }

            }
            if (Game1._mouseState.MiddleButton == ButtonState.Pressed)
            {
                //_map2D.SetTile(_layer1, _mouseMapX, _mouseMapY, _tileA);

                MapBlock.Add(_mapObject, this, _mouseMapX, _mouseMapY);
                //Block.Add(_mapObject, this, _mouseMapX, _mouseMapY);
            }
            if (Game1._mouseState.RightButton == ButtonState.Pressed)
            {
                //Console.WriteLine("Delete Tile !");
                //_map2D.SetTile(_layer1, _mouseMapX, _mouseMapY, new TileMap());

                MapObject.Remove(_mapObject, this, _mouseMapX, _mouseMapY, true);
            }
            if (Input.Button.OnePress("WaveExplosion", Keyboard.GetState().IsKeyDown(Keys.LeftAlt)))
            {
                new ExplosionWave(_mapObject, Position.RIGHT, 4, 4)
                    .AppendTo(this)
                    .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);

                new ExplosionWave(_mapObject, Position.LEFT, 4, 4)
                    .AppendTo(this)
                    .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);

                new ExplosionWave(_mapObject, Position.UP, 4, 4)
                    .AppendTo(this)
                    .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);

                new ExplosionWave(_mapObject, Position.DOWN, 4, 4)
                    .AppendTo(this)
                    .This<ExplosionWave>().GoTo(_mouseMapX, _mouseMapY);
            }
            if (Input.Button.OnePress("PoseBomb", Keyboard.GetState().IsKeyDown(Keys.LeftShift)))
            {
                //Bomb.Add(_mapObject, this, _mouseMapX, _mouseMapY);

                //Enemy.Add(this, _mapObject, _mouseMapX, _mouseMapY, _portal.GetMapPosition().X, _portal.GetMapPosition().Y);
            }
            #endregion


            // AutoPop Ennemy Debug **************************
            if (IsStartWave)
            {
                _tickEnemyPop++; 

                if (_tickEnemyPop > _tempoEnemyPop)
                {
                    _tickEnemyPop = 0;
                    ++_nbEnemyPop;

                    if (_nbEnemyPop < _maxEnemy)
                    {
                        int i = Misc.Rng.Next(0, _enemyPopPoint.Count - 1);
                        Point popPoint = _enemyPopPoint[i];

                        if (null != popPoint)
                        {
                            int j = Misc.Rng.Next(0, 10);

                            if (j > 5)
                                EnemyOmega.Add(this, _mapObject, popPoint.X, popPoint.Y, _portal);
                            else
                                Enemy.Add(this, _mapObject, popPoint.X, popPoint.Y, _portal);
                        }

                        //Enemy.Add(this, _mapObject, popPoint.X, popPoint.Y, Game1._heros[0]._mapTarget);
                    }
                }
            }


            // Collision2D System Run !
            //_collision2DGrid.SetPosition((int)_x, (int)_y);
            _collision2DGrid.SetPosition(0, 0);
            Collision2D.ResetAllZone(this);
            Collision2D.GridSystemZone(this, _collision2DGrid);

            // Update Map2D
            //_map2D.Update();

            // Update Childs
            UpdateChilds(gameTime);

            // Update Player HUD !
            //for (int i = 0; i < _playerHuds.Length; i++)
            //{
            //    _playerHuds[i].Update();
            //}

            // Update FX
            //_fxFog.Update();

            return this;
        }

        //public override Node Render(SpriteBatch batch)
        //{
        //    //SortZD();

        //    //for (int i=0; i<Game1.MAX_PLAYER; ++i)
        //    //    if (null != Game1._heros[i])
        //    //        RenderLevel(_viewZone[i], _camera[i], Game1._heros[i]._x, Game1._heros[i]._y);

        //    return base.Render(batch);
        //}

        public override void RenderView(SpriteBatch batch)
        {
            //_fxFog.Render(batch, Color.White * .5f);

            //Draw.RectFill(batch, new Rectangle(0, 0, ViewW, ViewH), Color.DarkBlue * .06f); // little blueish effect

            //base.RenderView(batch);
        }

    }
}
