﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System.Collections.Generic;

namespace ArenaTotalWar
{
    public class BarInfo : Node
    {
        public class Warning
        {
            public float _mini;
            public float _maxi;

            public int _tempoWarning;

            public bool _playSound;
            public bool _showWarning;

            public Warning(float mini, float maxi, int tempoWarning = 80, bool playSound = false, bool showWarning = false)
            {
                _mini = mini;
                _maxi = maxi;

                _tempoWarning = tempoWarning;

                _playSound = playSound;
                _showWarning = showWarning;
            }
        }

        Addon.Loop _loop;

        Color _color;
        Color _bgColor;
        Color _borderColor;
        float _borderWidth;

        float _value;
        float _showValue;
        float _maxValue;
        int _width;
        int _height;

        int _tickWarning = 0;
        int _tempoWarning = 80;
        bool _isWarning;
        float _alphaWarning = 0;

        bool _playSound;
        bool _showWarning;

        bool _drawWarning;

        Node _owner;
        Rectangle _rectValue = new Rectangle();

        List<Warning> _warnings = new List<Warning>(); 

        public BarInfo(Node owner, Color color, Color bgColor, Color borderColor, float maxValue, int width = 24, int height = 4, float borderWidth = 0)
        {
            _loop = CreateAddon<Addon.Loop>();
            _loop.SetLoop(0, 0, 1, .05f, Loops.PINGPONG);
            _loop.Start();

            _owner = owner;
            _maxValue = maxValue;
            _width = width;
            _height = height;
            _color = color;
            _bgColor = bgColor;
            _borderColor = borderColor;
            _borderWidth = borderWidth;

            SetSize(_width, _height);
            SetPivot(Position.CENTER);

            SetX(_owner._rect.Width / 2);
            SetY(_owner._rect.Y - 4);

            _z = -100000; // Over all Node Childs

            _rectValue.X = (int)_rect.X;
            _rectValue.Y = (int)_rect.Y;
            _rectValue.Width = (int)_rect.Width;
            _rectValue.Height = (int)_rect.Height;

        }
        public void SetValue(float value)
        {
            _value = value;
        }
        public BarInfo SetPlayShow(bool playSound = false, bool showWarning = false)
        {
            _playSound = playSound;
            _showWarning = showWarning;
            return this;
        }
        public BarInfo SetWarning(bool isWarning, int tempoWarning = 80, bool playSound = false, bool showWarning = false)
        {
            _isWarning = isWarning;
            _tempoWarning = tempoWarning;

            _playSound = playSound;
            _showWarning = showWarning;

            return this;
        }
        public bool IsWarning() { return _isWarning; }

        public BarInfo AddWarning(Warning warning)
        {
            _warnings.Add(warning);
            return this;
        }

        public float GetAlphaWarning() { return _alphaWarning; }

        public override Node Update(GameTime gameTime)
        {
            _loop.Update();

            _showValue = (_width * _value) / _maxValue;

            UpdateRect();

            if (!_isWarning)
            {
                for (int i=0; i<_warnings.Count; i++)
                {
                    if (_value >= _warnings[i]._mini && _value < _warnings[i]._maxi)
                    {
                        SetWarning(true, _warnings[i]._tempoWarning, _warnings[i]._playSound, _warnings[i]._showWarning);
                        break;
                    }
                }
            }


            if (_isWarning)
            {
                _tickWarning++;
                if (_tickWarning > _tempoWarning)
                {
                    _tickWarning = 0;
                    if (_playSound)
                        Game1._sound_alarm.Play(.05f, .2f, 0);
                }

                _alphaWarning = _loop._current;

                _drawWarning = _showWarning;

                _isWarning = false;
            }
            else
                _drawWarning = false;


            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            _rectValue.X = AbsRect.X;
            _rectValue.Y = AbsRect.Y;
            _rectValue.Width = (int)_showValue;

            //Draw.CenterStringX(batch, Game1._mainFont, "BarInfo", AbsX(), AbsY() - 32, _color);

            Draw.FillRectangle(batch, AbsRect, _bgColor);
            Draw.FillRectangle(batch, _rectValue, _color);
            Draw.Rectangle(batch, AbsRect, _borderColor, _borderWidth);

            if (_drawWarning) // Warning
                batch.Draw(Game1._tex_warning, new Vector2(AbsX - Game1._tex_warning.Width/2, AbsY - _oY - 18), Color.White * _alphaWarning);

            return this;
        }

        public bool IsPercent(int percent)
        {
            return _value <= (_maxValue / (100/percent));
        }
    }
}
