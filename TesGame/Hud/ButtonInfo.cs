﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;

namespace ArenaTotalWar
{
    public class ButtonInfo : Node
    {
        Addon.Loop _loop;

        string _infoA;
        string _infoB;
        string _infoX;
        string _infoY;

        //Color _colorA = Color.Yellow;
        //Color _colorB = Color.Yellow;
        //Color _colorC = Color.Yellow;
        //Color _colorD = Color.Yellow;

        Color _colorBG = Color.Black;
        Color _colorFG;

        bool _blink;

        public ButtonInfo(Color colorFG, string infoA = "", string infoB = "", string infoX = "", string infoY = "", bool blink = false)
        {
            _colorFG = colorFG;

            _infoA = infoA;
            _infoB = infoB;
            _infoX = infoX;
            _infoY = infoY;

            _blink = blink;
            //_animate.Add("popup", Easing.BackEaseInOut, new Tweening(start, end, duration));
            //_animate.Start("popup");

            _z = -1000; // Over all Node Childs

            SetSize(40, 40);
            SetPivot(Position.CENTER);

            _loop = CreateAddon<Addon.Loop>();

            _loop.SetLoop(1f, .8f, 1f, .01f, Loops.PINGPONG);

            if (_blink)
                _loop.Start();
        }
        public ButtonInfo SetBlink(bool blink = true)
        {
            _blink = blink;
            if (_blink) _loop.Start();
            return this;
        }
        public ButtonInfo SetInfoA(string infoA) { _infoA = infoA; return this; }
        public ButtonInfo SetInfoB(string infoB) { _infoB = infoB; return this; }
        public ButtonInfo SetInfoX(string infoX) { _infoX = infoX; return this; }
        public ButtonInfo SetInfoY(string infoY) { _infoY = infoY; return this; }

        public override Node Update(GameTime gameTime)
        {
            //if (_animate.OnEnd("popup"))
            //{
            //    KillMe();
            //}

            //_animate.NextFrame();
            _loop.Update();

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {

            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX()-1, AbsY() - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX()+1, AbsY() - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX(), AbsY()-1 - _animate.Value(), Color.Black);
            //Draw.CenterStringX(batch, Game1._font_Main, _label, AbsX(), AbsY()+1 - _animate.Value(), Color.Black);

            //Draw.CenterBorderedStringX(batch, Game1._font_Main, _label, AbsX(), AbsY() - _animate.Value(), _color, Color.Black);

            batch.Draw(Game1._tex_basicButton, new Vector2(AbsX-_oX, AbsY-_oY), Color.White);

            Draw.LeftMiddleBorderedString(batch, Game1._font_Main, _infoA, AbsX + 21, AbsY+1, _colorFG * _loop._current, _colorBG);
            Draw.TopCenterBorderedString(batch, Game1._font_Main, _infoB, AbsX, AbsY + 21, _colorFG * _loop._current, _colorBG);

            Draw.BottomCenterBorderedString(batch, Game1._font_Main, _infoX, AbsX, AbsY - 18, _colorFG * _loop._current, _colorBG);
            Draw.RightMiddleBorderedString(batch, Game1._font_Main, _infoY, AbsX - 20, AbsY+1, _colorFG * _loop._current, _colorBG);

            return this;
        }
    }
}
