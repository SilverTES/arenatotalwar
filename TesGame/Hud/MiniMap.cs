﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System.Collections.Generic;

namespace ArenaTotalWar
{
    class MiniMap
    {
        Node _container;
        List<Node> _group;
        Rectangle _rect = new Rectangle();
        //float _zoom;

        float _scaleX;
        float _scaleY;

        Addon.Loop _loop;

        //public MiniMap(Node container, int x, int y, int mapW, int mapH, float zoom = .25f)
        public MiniMap(Node container, Rectangle rect , int mapW, int mapH)
        {
            _container = container;

            _rect = rect;
            //_rect.X = x;
            //_rect.Y = y;
            // Calculate aspect ratio

            //_rect.Width = (int)(mapW * _zoom)+1;
            //_rect.Height = (int)(mapH * _zoom)+2;

            _scaleX = (float)_rect.Width / (float)mapW;
            _scaleY = (float)_rect.Height / (float)mapH;

            //_scaleX = mapW /_rect.Width ;
            //_scaleY = mapH /_rect.Height;

            //_zoom = zoom;

            _loop = new Addon.Loop();
            _loop.SetLoop(0, 0, 1, .05f, Loops.PINGPONG);
            _loop.Start();
        }

        public MiniMap SetPosition(int x , int y)
        {
            _rect.X = x;
            _rect.Y = y;
            return this;
        }

        public Rectangle GetRect() { return _rect; }

        public void Update()
        {
            _loop.Update();
        }

        public void Render(SpriteBatch batch)
        {
            Draw.FillRectangle(batch, _rect, Color.Black);

            //Draw.Grid(batch, _rect.X, _rect.Y, _rect.Width, _rect.Height, _rect.Width/10, _rect.Height/10, Color.DeepSkyBlue * .4f);

            _group = _container.GroupOf(UID.Get<MapDecor>());
            for (int i = 0; i < _group.Count; i++)
            {
                Color color = Color.GreenYellow * .6f;
                DrawPoint(batch, _group[i]._x * _scaleX, _group[i]._y * _scaleY, color, 1);
            }

            _group = _container.GroupOf(UID.Get<MapBlock>());
            for (int i = 0; i < _group.Count; i++)
            {
                Color color = Color.Firebrick * .6f;
                DrawPoint(batch, _group[i]._x * _scaleX, _group[i]._y * _scaleY, color, 1);
            }

            _group = _container.GroupOf(UID.Get<Block>());
            for (int i = 0; i < _group.Count; i++)
            {
                Color color = Color.LightGray * .6f;
                DrawPoint(batch, _group[i]._x * _scaleX, _group[i]._y * _scaleY, color, 1);
            }

            // Draw All Node in container !
            _group = _container.GroupOf(UID.Get<Enemy>());
            for (int i = 0; i < _group.Count; i++)
            {
                DrawPoint(batch, _group[i]._x * _scaleX, _group[i]._y * _scaleY, Color.Red, 1);
            }

            _group = _container.GroupOf(UID.Get<Tower>());
            for (int i = 0; i < _group.Count; i++)
            {
                Color color = Color.Blue;

                if (_group[i].This<Tower>().IsWarning())
                    color = Color.LightBlue * _loop._current;

                DrawPoint(batch, _group[i]._x * _scaleX, _group[i]._y * _scaleY, color, 1);
            }

            _group = _container.GroupOf(UID.Get<Hero>());
            for (int i=0; i< _group.Count; i++)
            {
                Vector2 pos = new Vector2(_rect.X, _rect.Y) + new Vector2(_group[i]._x * _scaleX, _group[i]._y * _scaleY);
                DrawCross(batch, _group[i]._x * _scaleX, _group[i]._y * _scaleY, Color.DarkOrange);
                DrawPoint(batch, _group[i]._x * _scaleX, _group[i]._y * _scaleY, Color.Yellow, 1);
            }

            Draw.Rectangle(batch, _rect, Color.White * .4f);
        }

        void DrawPoint(SpriteBatch batch, float x, float y, Color color, float size = 1)
        {
            Draw.FillSquare(batch, new Vector2(_rect.X + x, _rect.Y + y), size, color);
        }

        void DrawCross(SpriteBatch batch, float x, float y, Color color, float size = 1)
        {
            Draw.FillSquare(batch, new Vector2(_rect.X + x, _rect.Y + y-1), size, color);
            Draw.FillSquare(batch, new Vector2(_rect.X + x, _rect.Y + y+1), size, color);
            Draw.FillSquare(batch, new Vector2(_rect.X + x-1, _rect.Y + y), size, color);
            Draw.FillSquare(batch, new Vector2(_rect.X + x+1, _rect.Y + y), size, color);
        }

    }
}
