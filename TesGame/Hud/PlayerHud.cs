﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Retro2D;
using System.Text;

namespace ArenaTotalWar
{
    class PlayerHud : Node
    {
        int _indexPlayer;
        string _label;

        Hero _hero;

        ButtonInfo _buttonInfo;

        bool _buttonIsDocked;

        StringBuilder _sbEnergy = new StringBuilder();
        StringBuilder _sbPower = new StringBuilder();
        StringBuilder _sbMaxActiveBomb = new StringBuilder();

        public PlayerHud(int indexPlayer, string label)
        {
            _indexPlayer = indexPlayer;
            _label = label;

            int show = Game1._tex_playerHud.Width / 2;
            int hide = 80;

            _buttonInfo = (ButtonInfo)new ButtonInfo(Color.Gray, "OK", "RUN")
                .AppendTo(this)
                .SetPosition(Game1._tex_playerHud.Width / 2, 24);


            SetZ(-10000);

            _animate.Add("show", Easing.BackEaseIn, new Tweening(hide, show, 10));
            _animate.Add("hide", Easing.BackEaseIn, new Tweening(show, hide, 10));

            _animate.Start("show");

            //SetVisible(false); // Debug
        }

        public override Node Update(GameTime gameTime)
        {
            _buttonInfo.SetX(_animate.Value());

            UpdateRect();
            UpdateChilds(gameTime);
            

            _hero = Game1._heros[_indexPlayer];

            if (null != _hero)
            {
                _sbEnergy.Clear().AppendFormat("{0}/{1}", _hero.GetEnergy(), _hero.GetMaxEnergy());
                _sbPower.Clear().AppendFormat("{0}", _hero.GetBombPower());
                _sbMaxActiveBomb.Clear().AppendFormat("{0}/{1}", _hero.GetActiveBomb(), _hero.GetMaxActiveBomb());

                _buttonInfo.SetInfoA(_hero._heroUX.GetCurrentItem()._type.ToString());

                if (_hero.ON_B_L)
                {
                    _animate.Start("hide"); Game1._sound_key.Play(.2f, 1f, 0);
                }
                if (!_hero.IS_B_L && _buttonIsDocked)
                {
                    _buttonIsDocked = false;
                    _animate.Start("show"); Game1._sound_key.Play(.2f, .4f, 0);
                }

                if (_animate.OnEnd("hide")) _buttonIsDocked = true;

            }

            _animate.NextFrame();

            return this;
        }

        public override Node Render(SpriteBatch batch)
        {


            // Draw selected item
            if (null != _hero)
            {
                //// Draw Skin Player HUD
                //batch.Draw(Game1._tex_playerHud, new Vector2(AbsX(), AbsY()-20), new Rectangle(0,0,240,80), Color.White);

                //// Draw Player Name
                //Draw.LeftCenterBorderedString(batch, Game1._font_Mini, _label, AbsX+12, AbsY-4, Color.DarkGray, Color.Black);

                int posX = -48;
                int posY = 10;

                batch.Draw(Game1._tex_playerHud, new Vector2(AbsX + Game1._tex_playerHud.Width + posX, AbsY + posY - 2), new Rectangle(0, 80, 32, 32), Color.White);

                float selectX = _hero._heroUX._selectX * 11;
                float selectY = _hero._heroUX._selectY * 11;

                batch.Draw(Game1._tex_playerHud, 
                    new Vector2(AbsX + Game1._tex_playerHud.Width + posX + selectX + 2, AbsY + posY + selectY), 
                    new Rectangle(32, 80, 6, 6), Color.White);

                // Draw Player States 
                Draw.LeftMiddleBorderedString(batch, Game1._font_Main, _sbEnergy.ToString(), AbsX + 12, AbsY + 14, Color.Cyan, Color.Black);
                Draw.LeftMiddleBorderedString(batch, Game1._font_Main, _sbPower.ToString(), AbsX + 12, AbsY + 24, Color.Red, Color.Black);
                Draw.LeftMiddleBorderedString(batch, Game1._font_Main, _sbMaxActiveBomb.ToString(), AbsX + 12, AbsY + 34, Color.GreenYellow, Color.Black);

                RenderChilds(batch);
            }


            return this;
        }
    }
}
